package com.qulix.mdtlib.persistence;

import android.content.Context;
import android.content.SharedPreferences;

abstract class PersistentValue<ValueType> {
   public PersistentValue(Context context,
                          String keyName,
                          ValueType defaultValue) {

      _context = context.getApplicationContext();
      _keyName = keyName;
      _value = defaultValue;

   }

   public ValueType get() {
      if (_queried) return _value;

      SharedPreferences prefs
         = _context.getSharedPreferences(Persistence.PERSISTENCE_PREFERENCES,
                                         Context.MODE_PRIVATE);

      _value = get(prefs, _keyName, _value);

      _queried = true;

      return _value;
   }

   public void set(ValueType value) {
      _queried = true;
      _value = value;

      SharedPreferences prefs
         = _context.getSharedPreferences(Persistence.PERSISTENCE_PREFERENCES,
                                         Context.MODE_PRIVATE);

      SharedPreferences.Editor edit = prefs.edit();

      try {
         set(edit, _keyName, value);
      } finally {
         edit.commit();
      }
   }

   protected abstract void set(SharedPreferences.Editor editor, String key, ValueType value);
   protected abstract ValueType get(SharedPreferences prefs, String key, ValueType defaultValue);

   private Context _context;
   private String _keyName;
   private ValueType _value;
   private boolean _queried;
}