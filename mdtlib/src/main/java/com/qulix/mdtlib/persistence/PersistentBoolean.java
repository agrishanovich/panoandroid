package com.qulix.mdtlib.persistence;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistentBoolean extends PersistentValue<Boolean> {

   public PersistentBoolean(Context context,
                            String keyName,
                            Boolean defaultValue) {
      super(context, keyName, defaultValue);
   }

   @Override protected Boolean get(SharedPreferences prefs, String key, Boolean defValue) {
      return prefs.getBoolean(key, defValue);
   }

   @Override protected void set(SharedPreferences.Editor editor, String key, Boolean value) {
      editor.putBoolean(key, value);
   }

   public void toggle() {
      set(!get());
   }
}