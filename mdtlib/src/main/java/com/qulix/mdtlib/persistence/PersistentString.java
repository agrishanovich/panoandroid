package com.qulix.mdtlib.persistence;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistentString extends PersistentValue<String> {

   public PersistentString(Context context,
                            String keyName,
                            String defaultValue) {
      super(context, keyName, defaultValue);
   }

   @Override protected String get(SharedPreferences prefs, String key, String defValue) {
      return prefs.getString(key, defValue);
   }

   @Override protected void set(SharedPreferences.Editor editor, String key, String value) {
      editor.putString(key, value);
   }
}