package com.qulix.mdtlib.persistence;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistentFloat extends PersistentValue<Float> {

   public PersistentFloat(Context context,
                            String keyName,
                            Float defaultValue) {
      super(context, keyName, defaultValue);
   }

   @Override protected Float get(SharedPreferences prefs, String key, Float defValue) {
      return prefs.getFloat(key, defValue);
   }

   @Override protected void set(SharedPreferences.Editor editor, String key, Float value) {
      editor.putFloat(key, value);
   }
}