package com.qulix.mdtlib.persistence;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistentLong extends PersistentValue<Long> {

   public PersistentLong(Context context,
                            String keyName,
                            Long defaultValue) {
      super(context, keyName, defaultValue);
   }

   @Override protected Long get(SharedPreferences prefs, String key, Long defValue) {
      return prefs.getLong(key, defValue);
   }

   @Override protected void set(SharedPreferences.Editor editor, String key, Long value) {
      editor.putLong(key, value);
   }
}