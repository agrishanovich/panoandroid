package com.qulix.mdtlib.persistence;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistentInt extends PersistentValue<Integer> {

   public PersistentInt(Context context,
                            String keyName,
                            Integer defaultValue) {
      super(context, keyName, defaultValue);
   }

   @Override protected Integer get(SharedPreferences prefs, String key, Integer defValue) {
      return prefs.getInt(key, defValue);
   }

   @Override protected void set(SharedPreferences.Editor editor, String key, Integer value) {
      editor.putInt(key, value);
   }
}