package com.qulix.mdtlib.xml.sax;
import java.util.List;

import com.qulix.mdtlib.pair.Pair;

public final class AttributeValue implements Element {

   public String value() {
      if (_value != null) {
         return _value.trim();
      } else {
         return null;
      }
   }

   public AttributeValue(String name) {
      _name = name;
   }

   @Override public Element onElement(String name,
                                      List<Pair<String, String>> attributes) {
      for (Pair<String, String> attr : attributes) {
         if (attr.first.equals(_name)) {
            _value = attr.second;
            break;
         }
      }

      return null;
   }


   @Override public boolean isComplete() { return _value != null; }

   @Override public void onEnd() {}
   @Override public void characters(String characters) {}

   @Override public void reset() {
      _value = null;
   }

   private String _value;
   private String _name;
}