package com.qulix.mdtlib.xml.sax;
import java.util.List;

import com.qulix.mdtlib.pair.Pair;


public final class ElementText implements Element {

   public String value() {
      if (_text != null) {
         return _text.trim();
      } else {
         return null;
      }
   }

   @Override public Element onElement(String name,
                                      List<Pair<String, String>> attributes) {
      return new Element() {
         /**
          * Return element to push or null
          */
         @Override public Element onElement(String name, List<Pair<String, String>> attributes) {
            return this;
         }

         @Override public void characters(String string) {
            ElementText.this.characters(string);
         }

         @Override public void onEnd() {}

         /**
          * Returns status of completion. If element returns "complete", it will
          * not collect any data until reset, so it can safely be removed from
          * list of tries and no callback methods can be called on it.
          */
         @Override public boolean isComplete() { return false; }

         @Override public void reset() {}
      };
   }

   @Override public boolean isComplete() { return _text != null; }

   @Override public void characters(String characters) {
      if (_text == null) _text = "";

      _text += characters;
   }

   @Override public void onEnd() { }

   @Override public void reset() {
      _text = null;
   }

   private String _text;
}