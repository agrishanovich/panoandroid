package com.qulix.mdtlib.xml.sax;
import java.util.List;

import com.qulix.mdtlib.pair.Pair;

/**
 * Dummy element can be pushed to stack of elements in case if some part
 * of hierarchy needs to be ignored.
 */
public final class DummyElement implements Element {

   // as this state-free, we can return self to avoid duplicating objects
   @Override public Element onElement(String name, List<Pair<String, String>> attributes) { return this; }

   @Override public boolean isComplete() { return true; }

   @Override public void onEnd() {}
   @Override public void characters(String characters) {}

   @Override public void reset() {}
}