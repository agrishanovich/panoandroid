package com.qulix.mdtlib.xml.sax;

import java.util.List;

import com.qulix.mdtlib.pair.Pair;

public final class EndGuard implements Element {

   public EndGuard(Element slave,
                   Runnable guard) {
      _slave = slave;
      _guard = guard;
   }

   @Override public Element onElement(String name, List<Pair<String, String>> attributes) {
      final Element toPush = _slave.onElement(name, attributes);

      if (toPush == null) return null;

      return new Element() {

         @Override public Element onElement(String name, List<Pair<String, String>> attributes) {
            return toPush.onElement(name, attributes);
         }

         @Override public void onEnd() {
            toPush.onEnd();
            _guard.run();
         }

         @Override public void characters(String characters) {
            toPush.characters(characters);
         }

         @Override public boolean isComplete() {
            return toPush.isComplete();
         }

         @Override public void reset() {
            toPush.reset();
         }

      };
   }

   @Override public void onEnd() {
      _slave.onEnd();
   }

   @Override public void characters(String characters) {
      _slave.characters(characters);
   }

   /**
    * Returns status of completion. If element returns "complete", it will
    * not collect any data until reset, so it can safely be removed from
    * list of tries and no callback methods can be called on it.
    */
   @Override public boolean isComplete() {
      return _slave.isComplete();
   }


   @Override public void reset() {
      _slave.reset();
   }

   private Element _slave;
   private Runnable _guard;
}