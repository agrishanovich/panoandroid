package com.qulix.mdtlib.xml.sax;

import java.util.List;

import com.qulix.mdtlib.pair.Pair;

public interface Element {

   /**
    * Return element to push or null
    */
   Element onElement(String name, List<Pair<String, String>> attributes);

   void characters(String string);

   void onEnd();

   /**
    * Returns status of completion. If element returns "complete", it will
    * not collect any data until reset, so it can safely be removed from
    * list of tries and no callback methods can be called on it.
    */
   boolean isComplete();

   void reset();
}