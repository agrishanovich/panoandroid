package com.qulix.mdtlib.xml.sax;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.qulix.mdtlib.pair.Pair;

/**
 * This class implements the idea of description-based SAX xml parser.
 *
 * To parse data with SAX state machine required which will control in
 * which element currently parser is and which to do in this case with this
 * element.
 *
 * The idea of this parser is to separate state machine from description of
 * the data format, so state machine can be reusable.
 *
 * This parser relies on the "data description" which actually is
 * state-machine tree nodes, which can be used to store and obtain data.
 *
 * This is how description looks like:
 *
 * <pre>
 *    private Element _description
 *      = inElement("zoneplaces",
 *
 *                  _zoneReader
 *                  = inElement("zone",
 *                              // on end
 *                              new Runnable() {
 *                                 @Override public void run() {
 *                                    completeZone();
 *                                    _zoneReader.reset();
 *                                 }
 *                              },
 *                              // contents
 *                              _zoneId = new AttributeValue("id"),
 *
 *                              inElement("owner",
 *                                        _owner = new ElementText()),
 *
 *                              _placeReader
 *                              = inElement("place",
 *                                          new Runnable() {
 *                                             @Override public void run() {
 *                                                completePlace();
 *                                                _placeReader.reset();
 *                                             }
 *                                          },
 *                                          _placeId = new AttributeValue("id"),
 *                                          inElement("coordinates",
 *                                                    _placeCenter = new AttributeValue("centre"),
 *                                                    _placeCoordinates = new ElementText()),
 *                                          inElement("properties",
 *
 *                                                    inProperty("Additional", _propertyAdditional = new ElementText()),
 *                                                    inProperty("Address", _propertyAddress = new ElementText()),
 *                                                    inProperty("Contact", _propertyContact = new ElementText()),
 *                                                    inProperty("Currency", _propertyCurrency = new ElementText()),
 *                                                    inProperty("Description", _propertyDescription = new ElementText()),
 *                                                    inProperty("GIPLayer", _propertyGipLayer = new ElementText()),
 *                                                    inProperty("Hours", _propertyHours = new ElementText()),
 *                                                    inProperty("IconURL", _propertyIconURL = new ElementText()),
 *                                                    inProperty("Metro", _propertyMetro = new ElementText()),
 *                                                    inProperty("Period", _propertyPeriod = new ElementText()),
 *                                                    inProperty("Price", _propertyPrice = new ElementText()),
 *                                                    inProperty("Test", _propertyTest = new ElementText()),
 *                                                    inProperty("countSpaces", _propertyCountSpaces = new ElementText()),
 *                                                    inProperty("freeSpaces", _propertyFreeSpaces = new ElementText()),
 *                                                    inProperty("hasPopup", _propertyHasPopup = new ElementText()),
 *                                                    inProperty("hasRoute", _propertyHasRoute = new ElementText()),
 *                                                    inProperty("isListed", _propertyIsListed = new ElementText()),
 *                                                    inProperty("isOperational", _propertyIsOperational = new ElementText()),
 *                                                    inProperty("isPaid", _propertyIsPaid = new ElementText()),
 *                                                    inProperty("isParking", _propertyIsParking = new ElementText()),
 *                                                    inProperty("isVisibleLight", _propertyIsVisibleLight = new ElementText()),
 *                                                    inProperty("lineColor", _propertyLineColor = new ElementText()),
 *                                                    inProperty("name_ru", _propertyNameRu = new ElementText())))))
 * </pre>
 *
 * for XML like:
 *
 * <pre>
 *
 * <zoneplaces>
 *	<zone id="ROUTES">
 *		<owner>CODD</owner>
 *		<place id="CAO_006" uniqueness="owner" type="G">
 *			<coordinates centre="55.7589336593698,37.5855787081453">55.765949,37.590469|55.764919,37.589069|55.763439,37.587582|55.761189,37.585899|55.759209,37.584572|55.75721,37.584011|55.755619,37.58371|55.755249,37.583389|55.752869,37.583031|55.752869,37.583752|55.755421,37.58416|55.755619,37.58408|55.758881,37.584801|55.760105,37.585533|55.761307,37.586372|55.76334,37.58799|55.76487,37.589531|55.765759,37.590721|55.765949,37.590469</coordinates>
 *			<properties>
 *				<property name="Additional">Ул. Большая Садовая</property>
 *				<property name="Address">Центральный АО</property>
 *				<property name="name_ru">Маршрут №6</property>
 *				<property name="timeCreated">1343138842</property>
 *			</properties>
 *		</place>
 *		<place id="LAYER_ROUTE" uniqueness="owner" type="S">
 *			<properties>
 *				<property name="Description">Маршруты МКФ</property>
 *				<property name="GIPLayer">48</property>
 *				<property name="canPark">No</property>
 *				<property name="hasPopup">No</property>
 *				<property name="hasRoute">No</property>
 *				<property name="isListed">No</property>
 *				<property name="isParking">No</property>
 *				<property name="isVisible">Yes</property>
 *				<property name="isVisibleFull">Yes</property>
 *				<property name="isVisibleLight">Yes</property>
 *				<property name="lineColor">#FF3030</property>
 *				<property name="name_ru">Маршруты МКФ</property>
 *				<property name="timeCreated">1342079335</property>
 *			</properties>
 * </pre>
 *
 *
 */

public class SaxXmlParser {


   @SuppressWarnings("serial")
   public static class ParseError extends Exception {
      ParseError(String message) {
         super(message);
      }
   }

   private class Handler extends DefaultHandler {

      @Override
      public void startElement(String uri,
                               String localName,
                               String qName,
                               Attributes attributes) throws SAXException {

         Element top = _stack.peek();

         List<Pair<String, String>> attrs = new ArrayList<Pair<String, String>>();

         for (int a = 0; a < attributes.getLength(); ++a) {
            attrs.add(Pair.create(attributes.getLocalName(a),
                                  attributes.getValue(a)));
         }

         Element toPush = top.onElement(localName, attrs);

         if (toPush == null) {
            toPush = new DummyElement();
         }

         _stack.push(toPush);
      }

      @Override
      public void endElement(String uri,
                             String localName,
                             String qName) throws SAXException {
         _stack.peek().onEnd();
         _stack.pop();
      }

      @Override
      public void characters(char[] ch,
                             int start,
                             int length) throws SAXException {
         _stack.peek().characters(new String(ch, start, length));
      }

   }



   public SaxXmlParser(String string,
                       Element description) throws ParseError {
      _description = description;

      _stack.push(_description);

      try {
         SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(new StringReader(string)),
                                                             new Handler());

      } catch (IOException e) {
         throw new ParseError("Failed to parse, no error handling currently: " + e);
      } catch (SAXException e) {
         throw new ParseError("Failed to parse, no error handling currently: " + e);
      } catch (ParserConfigurationException e) {
         throw new ParseError("Failed to parse, no error handling currently: " + e);
      }
   }


   private Element _description;
   private Stack<Element> _stack = new Stack<Element>();
}