package com.qulix.mdtlib.xml.sax;
import java.util.List;

import com.qulix.mdtlib.pair.Pair;

/**
 * Dummy element can be pushed to stack of elements in case if some part
 * of hierarchy needs to be ignored.
 */
public final class Matched implements Element {

   public interface Condition {
      boolean isMatches(String name,
                        List<Pair<String, String>> attributes);
   }

   public static class ByName implements Condition {
      public ByName(String name) {
         _name = name;
      }

      @Override public boolean isMatches(String name,
                                         List<Pair<String, String>> attributes) {
         return _name.equals(name);
      }

      private String _name;
   }

   public static class ByAttributeValue implements Condition {
      public ByAttributeValue(String name, String value) {
         _name = name;
         _value = value;
      }

      @Override public boolean isMatches(String name,
                                         List<Pair<String, String>> attributes) {
         for (Pair<String, String> attr : attributes) {
            if (attr.first.equals(_name)
                && attr.second.equals(_value)) return true;
         }

         return false;
      }

      private String _name;
      private String _value;
   }

   public static class And implements Condition {
      public And(Condition one,
                 Condition two) {
         _one = one;
         _two = two;
      }

      @Override public boolean isMatches(String name,
                                         List<Pair<String, String>> attributes) {
         return _one.isMatches(name, attributes)
            && _two.isMatches(name, attributes);
      }

      private Condition _one;
      private Condition _two;
   }

   public Matched(Condition condition,
                  Element match) {
      _condition = condition;
      _match = match;
   }

   @Override public Element onElement(String name,
                                      List<Pair<String, String>> attributes) {
      if (isMatches(name, attributes)) {

         Element toPush =  _match.onElement(name, attributes);

         if (toPush != null) return toPush;
         else return _match;
      } else {
         return null;
      }
   }


   private boolean isMatches(String name,
                             List<Pair<String, String>> attributes) {
      return attributes != null && name != null && _condition.isMatches(name, attributes);
   }

   @Override public boolean isComplete() {
      return _match.isComplete();
   }

   @Override public final void onEnd() {
      // do nothing
   }

   @Override public void characters(String characters) {}

   @Override public void reset() {
      _match.reset();
   }

   private Condition _condition;
   private Element _match;
}