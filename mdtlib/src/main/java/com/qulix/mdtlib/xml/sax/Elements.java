package com.qulix.mdtlib.xml.sax;
import java.util.List;

import com.qulix.mdtlib.pair.Pair;

/**
 * Dummy element can be pushed to stack of elements in case if some part
 * of hierarchy needs to be ignored.
 */
public final class Elements implements Element {

   public Elements(Element... slaves) {
      _slaves = slaves;
   }

   // as this state-free, we can return self to avoid duplicating objects
   @Override public Element onElement(String name,
                                      List<Pair<String, String>> attributes) {

      for (Element slave : _slaves) {
         boolean active = ! slave.isComplete();

         if (active) {
            Element toPush = slave.onElement(name, attributes);
            if (toPush != null) {
               return toPush;
            }
         }
      }

      return null;
   }


   @Override public boolean isComplete() {
      for (Element slave : _slaves) {
         if ( ! slave.isComplete() ) return false;
      }

      return true;
   }

   @Override public void onEnd() { /* nothing to do */ }
   @Override public void characters(String characters) { /* nothing to do */ }

   @Override public void reset() {
      for (Element slave : _slaves) {
         slave.reset();
      }
   }

   private Element[] _slaves;
}