package com.qulix.mdtlib.service;

import android.os.IBinder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.operation.SimpleOperation;

public class ConnectServiceOperation <InterfaceType> extends SimpleOperation {

   public ConnectServiceOperation(Context context,
                                  Class<?> serviceClass,
                                  final Receiver<InterfaceType> receiver) {

      _context = context.getApplicationContext();

      endedEvent().subscribe(new Runnable() {
            @Override public void run() {

               if (_connection != null) {
                  _context.unbindService(_connection);
               }

               _connection = null;
               _api = null;
            }
         });


      _connection = new ServiceConnection() {

            @SuppressWarnings("unchecked")
            @Override public void onServiceConnected(ComponentName name,
                                                     IBinder service) {
               try {
                  _api = (InterfaceType)service;
               } catch (ClassCastException e) {
                  throw new Error("Wrong interface type");
               }

               if (receiver != null && ! isEnded ()) {
                  receiver.receive(_api);
               }
            }

            @Override public void onServiceDisconnected(ComponentName name) {
               _api = null;
               _connection = null;
            }
         };

      Intent serviceIntent = new Intent(_context,
                                        serviceClass);

      _context.bindService(serviceIntent,
                           _connection,
                           Context.BIND_AUTO_CREATE);
   }

   public InterfaceType api() {
      return _api;
   }

   public boolean isConnected() {
      return _api != null;
   }

   private Context _context;
   private InterfaceType _api;
   private ServiceConnection _connection;
}