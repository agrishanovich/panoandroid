package com.qulix.mdtlib.cookie;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;

import java.util.*;

public class PersistentCookieStore extends BasicCookieStore {

   private static final String COOKIE_PREFS = "CookiePrefsFile";
   private static final String COOKIE_NAME_STORE = "names";
   private static final String COOKIE_NAME_PREFIX = "cookie_";
   private static final String COOKIE_NAMES_DIVIDER = ",";

   public interface JsonParserProvider {
      String toJson(Cookie dataType);
      Cookie fromJson(String json);
   }

   public PersistentCookieStore() {
      loadCookies();
   }

   public static void init(Context context, JsonParserProvider jsonParserProvider) {
      _sharedPreferences = context.getSharedPreferences(COOKIE_PREFS, 0);
      _jsonParserProvider = jsonParserProvider;
   }

   @Override
   public synchronized void addCookies(Cookie[] cookies) {
      super.addCookies(cookies);
      storeCookies();
   }

   @Override
   public synchronized void addCookie(Cookie cookie) {
      super.addCookie(cookie);
      storeCookies();
   }

   @Override
   public List<Cookie> getCookies() {
      return super.getCookies();
   }

   @Override
   public synchronized boolean clearExpired(Date date) {
      boolean clearExpiredResult = super.clearExpired(date);
      storeCookies();
      return clearExpiredResult;
   }

   @Override
   public synchronized void clear() {
      super.clear();
      storeCookies();
   }

   private void storeCookies() {
      List<Cookie> cookies = Collections.synchronizedList(getCookies());
      SharedPreferences.Editor preferencesEditor = _sharedPreferences.edit();

      StringBuilder storedCookieNames = new StringBuilder();

      List list = Collections.synchronizedList(cookies);
      synchronized(list) {
         Iterator<Cookie> iterator = list.iterator();
         while (iterator.hasNext()) {
            storeCookie(iterator.next(), storedCookieNames, preferencesEditor);
         }
      }

      preferencesEditor.putString(COOKIE_NAME_STORE, storedCookieNames.toString());
      preferencesEditor.commit();
   }

   private void storeCookie(Cookie cookie, StringBuilder storedCookieNames, SharedPreferences.Editor preferencesEditor) {
      if (!TextUtils.isEmpty(cookie.getName())) {
         if (storedCookieNames.length() > 0) {
            storedCookieNames.append(COOKIE_NAMES_DIVIDER);
         }
         storedCookieNames.append(cookie.getName());

         String cookieAsString = _jsonParserProvider.toJson(cookie);
         preferencesEditor.putString(COOKIE_NAME_PREFIX + cookie.getName(), cookieAsString);
      }
   }

   private void loadCookies() {
      Cookie[] cookies = null;
      String storedCookieNames = _sharedPreferences.getString(COOKIE_NAME_STORE, null);
      if (storedCookieNames != null) {
         String[] cookieNames = TextUtils.split(storedCookieNames, COOKIE_NAMES_DIVIDER);

         cookies = new Cookie[cookieNames.length];

         for (int i = 0; i < cookieNames.length; i++) {

            String cookieAsString = _sharedPreferences.getString(COOKIE_NAME_PREFIX + cookieNames[i], null);
            Cookie serializableCookie = _jsonParserProvider.fromJson(cookieAsString);

            if (serializableCookie != null ) {
               cookies[i] = serializableCookie;
            }
         }
      }

      if (cookies != null && cookies.length > 0) {
         super.addCookies(cookies);
      }
   }

   private static JsonParserProvider _jsonParserProvider;
   private static SharedPreferences _sharedPreferences;
}
