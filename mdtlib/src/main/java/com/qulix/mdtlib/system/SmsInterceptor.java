package com.qulix.mdtlib.system;

import android.os.Bundle;
import android.telephony.SmsMessage;
import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;

/**
 * To utilize intercepting of sms, add this to android manifest:
 *
 * <receiver android:name="path.to.SmsInterceptor.subclass">
 *   <intent-filter android:priority="100">
 *   <action android:name="android.provider.Telephony.SMS_RECEIVED" />
 *   </intent-filter>
 * </receiver>
 *
 * and
 *
 * <uses-permission android:name="android.permission.RECEIVE_SMS"/>
 *
 */

public abstract class SmsInterceptor extends BroadcastReceiver {

   @Override
   public void onReceive(Context context, Intent intent) {
      //---get the SMS message passed in---
      Bundle bundle = intent.getExtras();

      boolean abort = false;

      if (bundle != null) {
            //---retrieve the SMS message received---
            Object[] pdus = (Object[]) bundle.get("pdus");

            for (int i=0; i < pdus.length; i++){
               SmsMessage sms = SmsMessage.createFromPdu((byte[])pdus[i]);

               String message = sms.getMessageBody().toString();

               abort |= handleMessage(context,
                                      sms.getOriginatingAddress(),
                                      message);
            }
         }

      if (abort) {
         this.abortBroadcast();
      }
   }


   protected abstract boolean handleMessage(Context context, String phone, String message);
}