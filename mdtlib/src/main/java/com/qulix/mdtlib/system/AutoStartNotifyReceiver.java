package com.qulix.mdtlib.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * To use autostart receiver, subclass it, add a reaction in
 * onBootCompleted and then add receiver to the AndroidManifest.xml:
 *
 * <receiver android:name="subclass.package.SubclassName" android:label="@string/app_name">
 *   <intent-filter>
 *     <action android:name="android.intent.action.BOOT_COMPLETED" />
 *     <category android:name="android.intent.category.LAUNCHER" />
 *   </intent-filter>
 * </receiver>
 */
public abstract class AutoStartNotifyReceiver extends BroadcastReceiver {

   private static final String BOOT_COMPLETED_ACTION = "android.intent.action.BOOT_COMPLETED";

   @Override
   public void onReceive(Context context, Intent intent) {
      if(intent.getAction().equals(BOOT_COMPLETED_ACTION)){
         onBootCompleted(context);
      }
   }


   protected abstract void onBootCompleted(Context context);

}