package com.qulix.mdtlib.pair;

public class Pair <A, B> {
   public A first;
   public B second;

   public Pair(A first, B second) {
      this.first = first;
      this.second = second;
   }

   public static <A, B> Pair<A, B> create(A a, B b) {
      return new Pair<A, B>(a, b);
   }

   public String toString() {
      return "(" + first + ", " + second + ")";
   }
}