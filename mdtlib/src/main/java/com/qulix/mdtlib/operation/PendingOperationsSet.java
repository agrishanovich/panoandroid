package com.qulix.mdtlib.operation;

import java.util.ArrayList;
import java.util.List;

public final class PendingOperationsSet implements OperationTracker {

   @Override
   public <OperationType extends Operation> OperationType registerForTermination(final OperationType op) {
      op.endedEvent().subscribe(new Runnable() {
            @Override public void run() {
               _operations.remove(op);
            }
         });
      _operations.add(op);

      return op;
   }

   public PendingOperationsSet registerForTermination(final PendingOperationsSet opSet) {
      for (Operation operation: opSet._operations) {
         registerForTermination(operation);
      }

      return opSet;
   }

   public void terminateAll() {

      List<Operation> operationsToTerminate = new ArrayList<Operation>(_operations);

      _operations.clear();

      for (Operation op : operationsToTerminate) {
         op.terminate();
      }
   }

   public boolean isEmpty() {
      return _operations.size() == 0;
   }

   public int count() {
      return _operations.size();
   }

   @Override public String toString() {
      return "(" + count() + ") pending operations: " + _operations;
   }

   private List<Operation> _operations = new ArrayList<Operation>();
}