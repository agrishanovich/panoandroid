package com.qulix.mdtlib.operation;

public interface OperationTracker {
   <OperationType extends Operation> OperationType registerForTermination(OperationType op);
}