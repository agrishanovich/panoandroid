package com.qulix.mdtlib.operation;

import com.qulix.mdtlib.subscription.RunnableSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;

public class SimpleOperation implements Operation {

   @Override public final Subscription<Runnable> endedEvent() {
      return selfEndedEvent();
   }

   @Override public final void terminate() {
      if (_ended) return;
      _ended = true;

      selfEndedEvent().fireImmediately();
   }

   @Override public final boolean isEnded() {
      return _ended;
   }

   protected final RunnableSubscription selfEndedEvent() {
      if (_endedEvent == null) {
         _endedEvent = new RunnableSubscription();
      }
      return _endedEvent;
   }

   private RunnableSubscription _endedEvent;

   private boolean _ended;
}