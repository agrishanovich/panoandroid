package com.qulix.mdtlib.operation;
import com.qulix.mdtlib.concurrency.CTHandler;

public class TimerOperation extends SimpleOperation {
   public TimerOperation(final long timeout,
                         final Runnable action) {
      CTHandler.postDelayed(new Runnable() {
            @Override public void run() {
               if (!isEnded()) {
                  action.run();
               }
               terminate();
            }
         },
         timeout);
   }
}