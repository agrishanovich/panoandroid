package com.qulix.mdtlib.operation;

import com.qulix.mdtlib.subscription.interfaces.Subscription;

public interface Operation {

   Subscription<Runnable> endedEvent();

   boolean isEnded();

   void terminate();

}