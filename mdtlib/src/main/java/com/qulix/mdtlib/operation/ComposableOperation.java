package com.qulix.mdtlib.operation;

import com.qulix.mdtlib.functional.Factory;

import java.util.ArrayList;
import java.util.List;

public class ComposableOperation extends SimpleOperation {

   public ComposableOperation() {
      selfEndedEvent().subscribe(new Runnable() {
         @Override
         public void run() {
            _pendingSlaves.clear();
            if (_currentSlave != null) {
               _currentSlave.terminate();
               _currentSlave = null;
            }
         }
      });
   }

   public void addSlave(Factory<Operation> slaveCreator) {

      _pendingSlaves.add(slaveCreator);

      if (_currentSlave == null) {
         executeNext();
      }
   }

   private void executeNext() {
      if (_pendingSlaves.size() > 0) {
         _currentSlave = _pendingSlaves.remove(0).create();
         _currentSlave.endedEvent().subscribe(new Runnable() {
            @Override
            public void run() {
               executeNext();
            }
         });
      } else {
         _currentSlave = null;
         terminate();
      }
   }

   private Operation _currentSlave;
   private final List<Factory<Operation>> _pendingSlaves = new ArrayList<>();
}
