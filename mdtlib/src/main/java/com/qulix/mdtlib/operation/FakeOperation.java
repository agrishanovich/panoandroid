package com.qulix.mdtlib.operation;
import android.os.Handler;

public final class FakeOperation extends SimpleOperation {
   public FakeOperation(long timeout,
                        final Runnable action) {
      new Handler().postDelayed(new Runnable() {
            @Override public void run() {
               if (isEnded()) return ;

               action.run();

               terminate();
            }
         },
         timeout);
   }

}