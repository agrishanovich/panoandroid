package com.qulix.mdtlib.operation;

import com.qulix.mdtlib.functional.Maybe;


public class CompositeOperation extends SimpleOperation {

   public CompositeOperation() {
      selfEndedEvent().subscribe(new Runnable() {
            @Override public void run() {
               if (haveSlave()) {
                  _slave.terminate();
               }
            }
         });
   }

   protected Operation slave() {
      return _slave;
   }

   public void setSlave(Operation slave) {
      if (slave != null && slave.isEnded()) throw new IllegalArgumentException("can not set ended slave.");

      if (haveSlave()) {
         _slave.endedEvent().unSubscribe(_slaveEndedRunnable);
         _slave.terminate();
      }

      _slave = slave;

      if (haveSlave()) {
         _slave.endedEvent().subscribe(_slaveEndedRunnable);
      }
   }

   public void setSlave(Maybe<Operation> maybeSlave) {
      if (maybeSlave.isValue()) {
         setSlave(maybeSlave.value());
      }
   }

   public void resetSlave() {
      setSlave((Operation)null);
   }

   public boolean haveSlave() {
      return _slave != null;
   }

   private Runnable _slaveEndedRunnable = new Runnable() {
         @Override public void run() {
            _slave = null;
            terminate();
         }
      };

   private Operation _slave;
}