package com.qulix.mdtlib.exchange;

import com.qulix.mdtlib.exchange.InputData;

public class NamedElementReader<DataType> implements Reader<DataType> {

   public static <DataType> NamedElementReader<DataType> create(String name,
                                                                Reader<DataType> reader) {
      return new NamedElementReader<DataType>(name, reader);
   }

   private NamedElementReader(String name, Reader<DataType> slave) {
      _name = name;
      _slave = slave;
   }

   @Override public DataType read(InputData data,
                                  String name) throws InputData.ElementException {
      return _slave.read(data,
                         _name);
   }

   private String _name;
   private Reader<DataType> _slave;
}