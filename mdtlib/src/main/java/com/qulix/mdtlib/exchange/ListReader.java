package com.qulix.mdtlib.exchange;

import java.util.List;


public class ListReader<DataType> implements Reader<List<DataType>> {

   public static <DataType> ListReader<DataType> create(Reader<DataType> slave) {
      return new ListReader<DataType>(slave);
   }

   public ListReader(Reader<DataType> slave) {
      _slave = slave;
   }

   @Override public List<DataType> read(InputData data,
                                        String name) throws InputData.ElementException {
      return data.readList(name, _slave);
   }

   private Reader<DataType> _slave;
}