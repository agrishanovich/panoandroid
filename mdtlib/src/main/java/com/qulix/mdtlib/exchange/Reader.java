package com.qulix.mdtlib.exchange;

public interface Reader<DataType> {
   DataType read(InputData data,
                 String name) throws InputData.ElementException;
}