package com.qulix.mdtlib.exchange;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

class JsonArrayOfPrimitivesInputData implements InputData {

   JsonArrayOfPrimitivesInputData(JSONArray array, int index) throws ElementException {
      _array = array;
      _index = index;
   }

   @Override public long readLong(String name) throws ElementException {
      try {
         return _array.getLong(_index);
      } catch (JSONException e) {
         throw new ElementException(name,
                                    e);
      }
   }

   @Override public String readString(String name) throws ElementException {
      try {
         return _array.getString(_index);
      } catch (JSONException e) {
         throw new ElementException(name,
                                    e);
      }
   }


   @Override public <T> List<T> readList(String name, Reader<T> typeReader) throws ElementException {
      throw new ElementException(name, "Impossible to read list from element of array of primitives.");
   }

   @Override public InputData dive(String name) throws ElementException {
      throw new ElementException(name, "Impossible to perform dive to element of array of primitives.");
   }


   private JSONArray _array;
   private int _index;

}

public class JsonInputData implements InputData {

   @SuppressWarnings("serial")
   public static class ParsingError extends Exception {

      ParsingError(JSONException error) {
         _error = error;
      }

      public String toString() {
         return "Parsing error: " + _error;
      }

      private JSONException _error;
   }

   public JsonInputData(String data) throws ParsingError {
      try {
         _jsonObject = new JSONObject(data);
      } catch (JSONException e) {
         throw new ParsingError(e);
      }
   }

   private JsonInputData(JSONObject object) throws ElementException {
      _jsonObject = object;
   }


   @Override public InputData dive(String name) throws ElementException {
      if (name != TOPLEVEL) {
         try {
            return new JsonInputData(_jsonObject.getJSONObject(name));
         } catch (JSONException e) {
            throw new ElementException(name);
         }
      } else {
         return this;
      }
   }

   @Override public long readLong(String name) throws ElementException {
      try {
         return _jsonObject.getLong(name);
      } catch (JSONException e) {
         throw new ElementException(name, e);
      }
   }

   @Override public String readString(String name) throws ElementException {
      try {
         return _jsonObject.getString(name);
      } catch (JSONException e) {
         throw new ElementException(name, e);
      }
   }

   @Override public <T> List<T> readList(String name, Reader<T> elementReader) throws ElementException {
      try {
         JSONArray array = _jsonObject.getJSONArray(name);
         List<T> list = new ArrayList<T>();

         for (int a = 0; a < array.length(); ++a) {

            InputData innerData;

            if (array.optJSONObject(a) != null) {
               innerData = new JsonInputData(array.optJSONObject(a));
            } else {
               innerData = new JsonArrayOfPrimitivesInputData(array, a);
            }


            list.add(elementReader.read(innerData, TOPLEVEL));
         }

         return list;
      } catch (JSONException e) {
         throw new ElementException(name, e);
      }
   }

   private JSONObject _jsonObject;
}