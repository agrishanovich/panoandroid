package com.qulix.mdtlib.exchange;

import java.util.List;

public interface InputData {

   public static final String TOPLEVEL = null;

   @SuppressWarnings("serial")
   public static class ElementException extends Exception {

      public ElementException(String name) {
         _name = name;
      }

      public ElementException(String name, String problem) {
         _name = name;
         _problem = problem;
      }

      public ElementException(String name, Exception exception) {
         _name = name;
         _exception = exception;
      }

      public String toString() {
         if (_exception != null) {
            return "Element: " + _name + ", problem: " + _exception;
         }

         if (_problem != null) {
            return "Element: " + _name + ", problem: " + _problem;
         }

         return "No such element: " + _name;
      }

      private String _name;
      private String _problem;
      private Exception _exception;
   }

   InputData dive(String name) throws ElementException;

   long readLong(String name) throws ElementException;
   String readString(String name) throws ElementException;
   <T> List<T> readList(String name, Reader<T> elementReader) throws ElementException;
}