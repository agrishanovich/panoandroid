package com.qulix.mdtlib.images.sources;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.FitByMinSideImage;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class FitByMinSideSource implements Source {
   @Override
   public boolean canTryResolveAsync(Description description) {
      return description instanceof FitByMinSideImage;
   }

   @Override
   public Bitmap resolve(Description description) {
      return null;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      if (description instanceof FitByMinSideImage) {
         _logger.log("requested image: " + description);

         FitByMinSideImage fitByMinSideImage = (FitByMinSideImage)description;

         return new SubrequestImage(aux,
                                    fitByMinSideImage.originalImage(),
                                    createPostprocessor(fitByMinSideImage.width, fitByMinSideImage.height),
                                    receiver);
      } else {
         return null;
      }
   }


   private SubrequestImage.Postprocessor createPostprocessor(final int width, final int height) {
      return new SubrequestImage.Postprocessor() {
         @Override public Bitmap process(Bitmap source) {
            float scaleFactor;

            if (((float)width / source.getWidth()) > ((float) height / source.getHeight())) {
               scaleFactor = width / (float) source.getWidth();
            } else {
               scaleFactor = height / (float) source.getHeight();
            }

            int dstWidth =  (int)(scaleFactor * source.getWidth());
            int dstHeight = (int)(scaleFactor * source.getHeight());

            return Bitmap.createScaledBitmap(source,
                                             dstWidth,
                                             dstHeight,
                                             true);
         }
      } ;
   }

   private static final LoggerFactory _loggerFactory = new LoggerFactory("FitByMinSideSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
}
