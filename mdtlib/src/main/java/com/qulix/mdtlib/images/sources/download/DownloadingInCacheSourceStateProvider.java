package com.qulix.mdtlib.images.sources.download;

public interface DownloadingInCacheSourceStateProvider {
   boolean isDownloadingNow(String url);
}
