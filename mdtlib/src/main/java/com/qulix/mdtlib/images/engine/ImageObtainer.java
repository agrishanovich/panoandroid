package com.qulix.mdtlib.images.engine;

import android.graphics.Bitmap;
import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.concurrency.PrioritizedThreadFactory;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.cache.CacheLayer;
import com.qulix.mdtlib.images.cache.ImageCache;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.settings.Settings;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @class ImageObtainer
 *
 * @note Please, note, that this class is intended to intialized only one
 * time at the lifecycle of application.
 *
 * It designed so it should be inited in the Application.onCreate because
 * application needs to configure it.
 *
 */
public final class ImageObtainer {

   public static final boolean DEBUG = false;

   private static ImageObtainer _staticShared;

   public interface Aux {
      Cancellable intermediateImage(Description description,
                                    Receiver<Bitmap> receiver);

      void submitWork(Runnable runnable);
      void postCT(Runnable runnable);
   }

   public static ImageObtainer instance() {
      if (_staticShared == null) {
         throw new IllegalStateException("ImageObtainer not initialized. Call ImageObtainer.init in the Application.onCreate");
      }

      return _staticShared;
   }

   public static void init(Settings settings) {
      if (_staticShared != null) {
         /**
          * As it highly possible that this mechanics works with memory
          * cache, it should not be instantiated twice, this can lead to
          * leaking memory cache.
          */
         throw new IllegalStateException("Initializing ImageObtainer second time. This should not happen.");
      }

      _staticShared = new ImageObtainer(settings);
   }

   private ImageObtainer(Settings settings) {


      _sources = new ArrayList<Source>();

      List<CacheLayer> cacheLayers = Arrays.asList(settings.cacheLayers);

      _sources.addAll(cacheLayers);
      _sources.addAll(Arrays.asList(settings.sources));

      _cache = new ImageCache(_aux, cacheLayers);

      _workers = new WorkersQueue(settings.threadsCount);

      _executors = new PausableThreadPoolExecutor(settings.threadsCount,
                                                  new PrioritizedThreadFactory("Image utils thread", Thread.MIN_PRIORITY));
   }

   public Cancellable requestImage(Description description,
                                   Receiver<Bitmap> receiver) {
       return requestImage(description, receiver, null);
   }

   public Cancellable requestImage(Description description,
                                   Receiver<Bitmap> receiver,
                                   Receiver<Bitmap> preScanReceiver) {
      return requestImage(new Request(description, Request.Type.External, receiver, preScanReceiver));
   }


   public void suspend(final boolean suspend) {
      if (suspend) {
         LoggerFactory.log("paused");
         _executors.pause();
      } else {
         LoggerFactory.log("resumed");
         _executors.resume();
      }
   }

   /**
    * If request completed in syncronous way, nothing can be cancelled, so
    * it will return null
    */
   private Cancellable requestImage(final Request request) {

      // if can be resolved in sync way - return immediately. This is
      // possible in case, for example, if image found in memory cache.
      if (ResolveCycle.resolveSynchronously(_sources,
                                            request)) return null;


      // try to add this request to the currently processing workers:

      final boolean foundWorkerToPutTo = _workers.putToCorrespondingWorker(request);

      if ( ! foundWorkerToPutTo ) {
         LoggerFactory.log("new worker for " + request.description);

         _workers.addNewWorker(new MultiRequestWorker(request,
                                                      _aux,
                                                      _sources,
                                                      _onWorkerEnded));

      } else {
         LoggerFactory.log("attached request for " + request.description);
      }

      return request;
   }

   /**
    * Find worker which executes for same image description. Attach
    * request to it.
    */


   private final Aux _aux = new Aux() {
      @Override public Cancellable intermediateImage(final Description description,
                                                     final Receiver<Bitmap> receiver) {
         return requestImage(new Request(description,
                                         Request.Type.Intermediate,
                                         receiver,
                                         null));
      }

      @Override public void submitWork(Runnable runnable) {
         _executors.execute(runnable);
      }

      @Override public void postCT(Runnable runnable) {
         CTHandler.post(runnable);
      }
   };

   private final MultiRequestWorker.EndHandler _onWorkerEnded
         = new MultiRequestWorker.EndHandler() {
      @Override public void onEnded(final MultiRequestWorker endedWorker,
                                    final Description description,
                                    final Bitmap resultBitmap,
                                    final EndReason reason) {
         _workers.forget(endedWorker);

         if (reason == EndReason.Stopped) return ;
         if (resultBitmap == null) return ;

         final boolean storeToCache = endedWorker.haveEndRequests();
         if (!storeToCache) return ;

         _cache.store(description, resultBitmap);
      }
   };

   private final WorkersQueue _workers;

   private final List<Source> _sources;

   private final ImageCache _cache;

   private final PausableThreadPoolExecutor _executors;
}
