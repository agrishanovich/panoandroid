package com.qulix.mdtlib.images.utility;

import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ImageCachedFromUrl;
import com.qulix.mdtlib.images.description.ImageFromUrl;

public class TranslateOnlyToCacheImages implements Translate<Description> {
   @Override
   public Description translate(Description description) {
      if (description instanceof ImageFromUrl) {
         return new ImageCachedFromUrl(description);
      } else {
         description.mutate(this);
         return description;
      }
   }
}
