package com.qulix.mdtlib.images.widget;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.util.Log;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.qulix.mdtlib.subscription.RunnableSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;
import com.qulix.mdtlib.views.ViewControllerActivity;

public class LazyLoadImageView extends ImageView {

   public interface RequestObserver {
      void requestStarted();
      void requestEnded();
   }

   public interface TransformDescription {
      Description transform(ResizeToFitInRect description);
   }

   public interface TransformDrawable {
      Drawable transform(Drawable drawable);
   }

   @SuppressWarnings("unused")
   private static final String TAG = "LazyLoadImageView";
   /**
    * Constructor, passing call to parent call
    *
    * @param context context
    * @param attrs attributes
    * @param defStyle style
    */
   public LazyLoadImageView(Context context, AttributeSet attrs, int defStyle) {
      super(context, attrs, defStyle);
   }

   /**
    * Constructor, passing call to parent call
    *
    * @param context context
    * @param attrs attributes
    */
   public LazyLoadImageView(Context context, AttributeSet attrs) {
      super(context, attrs);
   }

   /**
    * Constructor, passing call to parent call
    *
    * @param context context
    */
   public LazyLoadImageView(Context context) {
      super(context);
   }

   public Description sentRequestedImageDescription() {
      return _sentRequestedImageDescription;
   }

   @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
      // pass to parent
      super.onSizeChanged(w, h, oldw, oldh);

      boolean changed = _width != w || _height != h;

      if ( ! changed) return ;

      _width = w;
      _height = h;

      requestImage();
      _onSizeChangedSubscription.run();
   }

   public Subscription<Runnable> onSizeChangedSubscription() {
      return _onSizeChangedSubscription;
   }

   public void setRequestObserver(RequestObserver observer) {
      _requestObserver = observer;
   }

   public void setRequestedImage(Description description) {

      setRequestedImage(description, true);
   }

   public void setRequestedImage(Description description, final boolean needShowDefaultImage) {

      _needShowDefaultImage = needShowDefaultImage;

      boolean changed =
            ((_requestedImageDescription == null) != (description == null))
                  || (_requestedImageDescription != null &&  ! _requestedImageDescription.equals(description));


      // if nothing changed - don't continue
      if ( ! changed ) return ;

      _requestedImageDescription = description;

      updateImage();
   }

   public boolean needStopRequestOnDetachedFromWindow() {
      return _needStopRequestOnDetachedFromWindow;
   }

   public void setNeedStopRequestOnDetachedFromWindow(boolean needStopRequestOnDetachedFromWindow) {
      _needStopRequestOnDetachedFromWindow = needStopRequestOnDetachedFromWindow;
   }

   public Subscription<Runnable> onErrorSubscription() {
      return _onErrorSubscription;
   }

	private void updateImage() {
		if (_requestedImageDescription == null) {

         stopRequest(_currentRequest);
         setDefaultImage();

      } else {

         requestImage();
      }
   }

   @Override public void setPadding(int left, int top, int right, int bottom) {
      super.setPadding(left, top, right, bottom);
      updateImage();
   }

   protected void requestImage() {

      if (_requestedImageDescription == null) return ;

      if (_width <= 0 || _height <= 0) return;


      final int targetWidth = _width - getPaddingLeft() - getPaddingRight();
      final int targetHeight = _height - getPaddingTop() - getPaddingBottom();


      final ResizeToFitInRect sizedImage
            = new ResizeToFitInRect(targetWidth,
                                    targetHeight,
                                    _requestedImageDescription);

      Description actualImage = sizedImage;

      if (_postresizeTransformDescription != null) {
         actualImage = _postresizeTransformDescription.transform(sizedImage);
      }

      if (actualImage == null) return ;

       final Receiver<Bitmap> showBitmap = new Receiver<Bitmap>() {
           @Override
           public void receive(Bitmap bitmap) {
               if (bitmap != null) {
                   // on success

                   Drawable resultDrawable = new BitmapDrawable(getContext().getResources(),
                           bitmap);

                   if (_transformDrawable != null) {
                       resultDrawable = _transformDrawable.transform(resultDrawable);
                   }

                   setImageDrawable(resultDrawable);
               } else {
                   setDefaultImage();
                  _onErrorSubscription.run();
               }
           }
       };
      final Receiver<Bitmap> onBitmap = new Receiver<Bitmap>() {
         @Override public void receive(Bitmap bitmap) {
             showBitmap.receive(bitmap);

            if (_requestObserver != null) {
               _requestObserver.requestEnded();
            }

            _currentRequest = null;
         }
      };

      Cancellable previousRequest = _currentRequest;
      //downloading does not start when activity called onPause already.
      if (_downloadsAllowed) {
         _currentRequest
               = ImageObtainer.instance().requestImage(actualImage, onBitmap, showBitmap);
         _sentRequestedImageDescription = actualImage;
      }
      stopRequest(previousRequest);

      if (_currentRequest != null) {
         // request in progress
         if (_needShowDefaultImage) {
            setDefaultImage();
         }

         if (_requestObserver != null) {
            _requestObserver.requestStarted();
         }
      }

   }

   public void setPostresizeTransformDescription(final TransformDescription transform) {
      _postresizeTransformDescription = transform;
   }

   public void setTransformDrawable(final TransformDrawable transform) {
      _transformDrawable = transform;
   }

   private void stopRequest(Cancellable request) {
      if (request != null) {
         request.cancel();
         request = null;
      }
   }

   public boolean isEnabledOutOfMemoryException() {
      return _enabledOutOfMemoryException;
   }

   public void setEnabledOutOfMemoryException(boolean enableOutOfMemoryError) {
      _enabledOutOfMemoryException = enableOutOfMemoryError;
   }

   @Override
   public void setImageResource(int resId) {
      try {
         super.setImageResource(resId);
      } catch (OutOfMemoryError e) {
         Log.e(TAG, "An OutOfMemmoryError occurred when try set image resource:" + resId);
         if (isEnabledOutOfMemoryException()) {
            throw e;
         }
      }
   }

   @Override
   public void setImageDrawable(Drawable drawable) {
      try {
         super.setImageDrawable(drawable);
      } catch (OutOfMemoryError e) {
         Log.e(TAG, "An OutOfMemmoryError occurred when try set image drawable:" + drawable);
         if (isEnabledOutOfMemoryException()) {
            throw e;
         }
      }
   }

   public void setDefault(int drawableId) {
      _defaultDrawableId = drawableId;
   }

   private void setDefaultImage() {
      if (_defaultDrawableId != 0) {
         setImageResource(_defaultDrawableId);
      } else {
         setImageDrawable(null);
      }
   }

   public void reset() {
      setRequestedImage(null);
   }

   @Override public void onAttachedToWindow() {
      super.onAttachedToWindow();
      // if already attached - nothing to handle
      if (_attached) return ;
      _attached = true;
      _downloadsAllowed = true;

      initializeSubscriptionsRunnables();

      final Context context = getContext();
      if (context instanceof ViewControllerActivity) {
         final ViewControllerActivity basicActivity = (ViewControllerActivity)context;

         basicActivity.onPauseSubscription().subscribe(_onPauseSubscriptionRunnable);
         basicActivity.onResumeSubscription().subscribe(_onResumeSubscriptionRunnable);
      }

      updateImage();
   }

   @Override public void onDetachedFromWindow() {
      super.onDetachedFromWindow();
      // do nothing if was not attached
      if (! _attached) return ;
      _attached = false;
      _downloadsAllowed = false;

      final Context context = getContext();
      if (context instanceof ViewControllerActivity) {
         final ViewControllerActivity basicActivity = (ViewControllerActivity)context;

         basicActivity.onPauseSubscription().unSubscribe(_onPauseSubscriptionRunnable);
         basicActivity.onResumeSubscription().unSubscribe(_onResumeSubscriptionRunnable);

         if (needStopRequestOnDetachedFromWindow()) {
            stopRequest(_currentRequest);
            setImageDrawable(null);
         }
      }
   }

   private void initializeSubscriptionsRunnables() {
      _onResumeSubscriptionRunnable = new Runnable() {
         @Override
         public void run() {
            _downloadsAllowed = true;
            updateImage();
         }
      };

      _onPauseSubscriptionRunnable = new Runnable() {
         @Override
         public void run() {
            _downloadsAllowed = false;
            stopRequest(_currentRequest);
         }
      };
   }

   private int _width = 0;
   private int _height = 0;

   private boolean _downloadsAllowed;
   private Description _requestedImageDescription;

   private Cancellable _currentRequest;

   private boolean _attached;

   private int _defaultDrawableId;

   private Runnable _onPauseSubscriptionRunnable;
   private Runnable _onResumeSubscriptionRunnable;
   private RunnableSubscription _onSizeChangedSubscription = new RunnableSubscription();
   private RunnableSubscription _onErrorSubscription = new RunnableSubscription();

   private TransformDescription _postresizeTransformDescription;
   private TransformDrawable _transformDrawable;

   private RequestObserver _requestObserver;

   private Description _sentRequestedImageDescription;

   private boolean _needShowDefaultImage;
   private boolean _needStopRequestOnDetachedFromWindow = true;

   private boolean _enabledOutOfMemoryException = true;
}