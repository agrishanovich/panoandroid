package com.qulix.mdtlib.images.decoder;

import android.graphics.Bitmap;
import android.util.Log;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.utils.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Map;

public class DecoderFactoryGuessDecoder implements DecoderFactory {
   private final Map<String, DecoderFactory> _decoders;
   private final DecoderFactory _defaultDecoder;

   public DecoderFactoryGuessDecoder(Map<String, DecoderFactory> decoders, DecoderFactory defaultDecoder) {
      assert null != defaultDecoder;

      _decoders = decoders;
      _defaultDecoder = defaultDecoder;
   }

   @Override public Decoder create(final Description description) {
      return new Decoder() {
         private Decoder _decoder;
         private boolean _cancelled = false;

         @Override
         public Bitmap workerThreadDecode(InputStream stream, Receiver<Bitmap> preScanReceiver) throws IOException {
            if (_cancelled) return null;

            // guess content type
            final int pushbackLimit = 100;
            PushbackInputStream bufferedStream = new PushbackInputStream(stream, pushbackLimit);
            byte [] firstBytes = new byte[pushbackLimit];
            bufferedStream.read(firstBytes);
            bufferedStream.unread(firstBytes);
            String mimeType = StreamUtils.guessContentType(new ByteArrayInputStream(firstBytes));
            Log.d("GuessDecoder", "mime: " + mimeType + " bytes: " + firstBytes);

            DecoderFactory factory = (_decoders.containsKey(mimeType) ? _decoders.get(mimeType) : _defaultDecoder);
            synchronized (this) {
               _decoder = factory.create(description);
            }
            return _decoder.workerThreadDecode(bufferedStream, preScanReceiver);
         }

         @Override public void cancel() {
            if (_cancelled) return;
            _cancelled = true;

            synchronized (this) {
               if (null != _decoder) {
                  _decoder.cancel();
               }
            }
         }
      };
   }
}
