package com.qulix.mdtlib.images.sources;

import android.graphics.*;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class ResizeToFitInRectSource implements Source {
   @Override
   public boolean canTryResolveAsync(Description description) {
      return description instanceof ResizeToFitInRect;
   }

   @Override
   public Bitmap resolve(Description description) {
      return null;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      if (description instanceof ResizeToFitInRect) {
         _logger.log("requested image: " + description);

         ResizeToFitInRect resizeToFitInRect = (ResizeToFitInRect)description;

         return new SubrequestImage(aux,
                                    resizeToFitInRect.originalImage(),
                                    createPostprocessor(resizeToFitInRect.width, resizeToFitInRect.height),
                                    receiver);
      } else {
         return null;
      }
   }

   private SubrequestImage.Postprocessor createPostprocessor(final int width, final int height) {
      return new SubrequestImage.Postprocessor() {
         @Override public Bitmap process(Bitmap source) {final double widthMultiplier = ((double)width)/((double)source.getWidth());
            final double heightMultiplier = ((double)height)/((double)source.getHeight());

            final double multiplier = Math.min(widthMultiplier,
                    heightMultiplier);


            final int newWidth = (int)(source.getWidth() * multiplier);
            final int newHeight = (int)(source.getHeight() * multiplier);

            return Bitmap.createScaledBitmap(source,
                    newWidth,
                    newHeight,
                    true);
            }
      } ;
   }

   private static final LoggerFactory _loggerFactory = new LoggerFactory("ResizeToFitInRectSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
}
