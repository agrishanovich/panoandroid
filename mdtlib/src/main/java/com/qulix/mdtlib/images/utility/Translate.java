package com.qulix.mdtlib.images.utility;

public interface Translate<T> {
   T translate(T t);
}
