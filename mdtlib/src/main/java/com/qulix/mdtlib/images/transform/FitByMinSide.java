package com.qulix.mdtlib.images.transform;

import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.FitByMinSideImage;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;
import com.qulix.mdtlib.images.widget.LazyLoadImageView.TransformDescription;

public class FitByMinSide implements TransformDescription {
   @Override
   public Description transform(ResizeToFitInRect resizeToFitInRect) {
      return new FitByMinSideImage(resizeToFitInRect.width, resizeToFitInRect.height, resizeToFitInRect.originalImage());
   }
}
