package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

public class ImageFromResource implements Description {
   public ImageFromResource(int resId) {
      if (resId == 0) throw new IllegalArgumentException("resId can not be 0.");

      this.resId = resId;
   }

   @Override
   public Description originalImage() {
      return null;
   }

   @Override public boolean equals(Object o) {
      return o instanceof ImageFromResource
              && resId == ((ImageFromResource)o).resId;
   }

   @Override public String toString() {
      return "resource id : " + resId;
   }

   @Override
   public String key() {
      return toString();
   }

   @Override
   public void mutate(Translate<Description> translator) {

   }

   public final int resId;
}