package com.qulix.mdtlib.images.decoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.orientation.DecodeReorientedBitmap;

import java.io.IOException;
import java.io.InputStream;

public final class DecoderFactoryDirectDecode implements DecoderFactory {

   private int _sampleSize = 0;


   public static DecoderFactoryDirectDecode withSampleSize(final int size) {
      final DecoderFactoryDirectDecode factory = new DecoderFactoryDirectDecode();
      factory._sampleSize = size;
      return factory;
   }

   @Override public Decoder create(final Description description) {

      return new Decoder() {

         final BitmapFactory.Options _options = createBitmapOptions();

         @Override public Bitmap workerThreadDecode(InputStream stream, Receiver<Bitmap> preScanReceiver) throws IOException {
            return DecodeReorientedBitmap.decode(stream, _options);
         }

         @Override public void cancel() {
            _options.requestCancelDecode();
         }

         private BitmapFactory.Options createBitmapOptions() {
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inTempStorage = new byte[256];
            bitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
            bitmapOptions.inSampleSize = _sampleSize;

            return bitmapOptions;
         }
      };

   }
}