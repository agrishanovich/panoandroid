package com.qulix.mdtlib.images.description;

public class TransformationOption extends OneOriginalImageDescription {

   public TransformationOption(String option, boolean heightEnabled, Description originalImage) {
      super(originalImage);
      if (option == null) throw new IllegalArgumentException("option can not be null");
      this.option = option;
      this.heightEnabled = heightEnabled;
   }

   public TransformationOption(String option, Description originalImage) {

      this(option, true, originalImage);
   }

   @Override public boolean equals(Object o) {
      if (o instanceof TransformationOption) {
         TransformationOption that = (TransformationOption) o;
         return that.option.equals(option)
                 && that.heightEnabled == heightEnabled
                 && that.originalImage().equals(originalImage());
      }
      return false;
   }

   @Override public String toString() {
      return "option:" + option + " height enabled:" + heightEnabled + ":" + originalImage();
   }

   @Override public String key() {
      return toString();
   }

   public final String option;
   public final boolean heightEnabled;
}
