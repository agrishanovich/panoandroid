package com.qulix.mdtlib.images.sources;

import android.graphics.*;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.RoundImage;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class RoundImageSource implements Source {
   @Override
   public boolean canTryResolveAsync(Description description) {
      return description instanceof RoundImage;
   }

   @Override
   public Bitmap resolve(Description description) {
      return null;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      if (description instanceof RoundImage) {
         _logger.log("requested image: " + description);

         RoundImage resizeToFitInRectImage = (RoundImage) description;

         return new SubrequestImage(aux,
                                    resizeToFitInRectImage.originalImage(),
                                    createPostprocessor(resizeToFitInRectImage.radius),
                                    receiver);
      } else {
         return null;
      }
   }


   private SubrequestImage.Postprocessor createPostprocessor(final int radius) {
      return new SubrequestImage.Postprocessor() {
         @Override public Bitmap process(Bitmap source) {

            return getRoundBitmap(source,
                                  radius);
         }
      } ;
   }

   public static Bitmap getRoundBitmap(Bitmap source, int radius) {
      return getRoundedBitmap(source);
   }

   private static Bitmap getRoundedBitmap(Bitmap bitmap) {
      final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
      final Canvas canvas = new Canvas(output);

      final int color = Color.BLACK;
      final Paint paint = new Paint();
      final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

      paint.setAntiAlias(true);
      canvas.drawARGB(0, 0, 0, 0);
      paint.setColor(color);

      float radius = bitmap.getWidth() / 2;

      if (bitmap.getHeight() < bitmap.getWidth()) {
         radius = bitmap.getHeight() / 2;
      }
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, radius, paint);

      paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
      canvas.drawBitmap(bitmap, rect, rect, paint);

      bitmap.recycle();

      return output;
   }

   private static final LoggerFactory _loggerFactory = new LoggerFactory("RoundImageSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
}
