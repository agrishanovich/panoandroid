package com.qulix.mdtlib.images.cache;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.utility.TranslateCachedImages;
import com.qulix.mdtlib.images.utility.TranslateOnlyToCacheImages;

public class LayerCachedImagesFromCache implements CacheLayer {

   public LayerCachedImagesFromCache(LayerMemoryLruCache cache) {
      _cache = cache;
   }

   @Override
   public void store(ImageObtainer.Aux aux, Description description, Bitmap bitmap) {
      _cache.store(aux, description, bitmap);
   }

   @Override
   public boolean canTryResolveAsync(Description description) {
      return _cache.canTryResolveAsync(description);
   }

   @Override
   public Bitmap resolve(Description description) {
      Bitmap result = null;
      result = _cache.resolve(description);
      if (result == null) {
         description.mutate(new TranslateOnlyToCacheImages());
         result = _cache.resolve(description);
         description.mutate(new TranslateCachedImages());
      }
      return result;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      return _cache.resolve(aux, description, receiver, preScanReceiver);
   }

   private LayerMemoryLruCache _cache;
}
