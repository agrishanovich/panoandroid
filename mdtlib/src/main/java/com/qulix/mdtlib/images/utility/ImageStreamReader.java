package com.qulix.mdtlib.images.utility;
import java.io.*;

import android.graphics.Bitmap;
import android.util.Log;

import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.decoder.Decoder;
import com.qulix.mdtlib.images.utility.LoggerFactory.Logger;

public final class ImageStreamReader implements Runnable, Cancellable {

   private boolean _cancelled;
   private InputStream _stream;

   private final Receiver<Bitmap> _receiver;
   private final Receiver<Bitmap> _preScanReceiver;
   private final Decoder _decoder;

   private final static LoggerFactory _loggerFactory = new LoggerFactory("ImageStreamReader");
   private final Logger _logger = _loggerFactory.create();

   public ImageStreamReader(final InputStream stream,
                            final Decoder decoder,
                            final Receiver<Bitmap> receiver) {
       this(stream, decoder, receiver, null);
   }

   public ImageStreamReader(final InputStream stream,
                            final Decoder decoder,
                            final Receiver<Bitmap> receiver,
                            final Receiver<Bitmap> preScanReceiver) {

      if (stream == null) throw new IllegalArgumentException("stream can not be null");
      if (decoder == null) throw new IllegalArgumentException("decoder can not be null");
      if (receiver == null) throw new IllegalArgumentException("receiver can not be null");

      _logger.log("spawned");

      _receiver = receiver;
      _preScanReceiver = preScanReceiver;

      // Using FlushedInputStream: http://code.google.com/p/android/issues/detail?id=6066
      _stream = new FlushedInputStream(stream);

      _decoder = decoder;
   }

   /**
    * Cancel activity of downloading image
    */
   @Override public synchronized void cancel() {
      if (_cancelled) return ;

      _logger.log("cancelling");

      _cancelled = true;
      _decoder.cancel();
      close();
   }

   @Override public void run() {
      synchronized(this) {
         if (_cancelled) {
            return;
         }
      }

      try {
         _logger.log("reading");


         Bitmap bitmap = _decoder.workerThreadDecode(_stream, new Receiver<Bitmap>() {
             @Override
             public void receive(Bitmap bitmap) {
                 postPreScan(bitmap);
             }
         });

         _logger.log("read succeed");
         postBitmap(bitmap);

      } catch(Throwable e) {
            if (_cancelled) return;

         _logger.log("Could not load image:" + Log.getStackTraceString(e));

         postBitmap(null);


      } finally {
         close();
      }
   }

   /**
    * Internal method for cleanup connection and download stream.
    *
    * <b>Warning!</b> Call this method <b>ONLY</b> from synchronized block.
    */
   private void close() {
      if (_stream != null) {
         try {
            _stream.close();
         } catch (Throwable e) {
            // this can happen if image download thread interrupted
            // before it could successfully open stream.
            //
            // TODO: should we at all log such kind of problems?
            Log.d("ImageStreamReader", "Could not close stream");
         } finally {
            _stream = null;
         }
      }
   }

   private void postBitmap(final Bitmap bitmap) {
      postBitmapToReceiver(bitmap, _receiver);
   }

   private void postPreScan(final Bitmap bitmap) {
       postBitmapToReceiver(bitmap, _preScanReceiver);
   }

   private void postBitmapToReceiver(final Bitmap bitmap, final Receiver<Bitmap> receiver) {
       CTHandler.post(new Runnable() {
           @Override public void run() {
               if (! _cancelled) {
                   receiver.receive(bitmap);
               }
           }
       });
   }

   private final class FlushedInputStream extends FilterInputStream {

      public FlushedInputStream(InputStream inputStream) {
         super(inputStream);
      }

      @Override public long skip(long n) throws IOException {
         long totalBytesSkipped = 0L;
         while (totalBytesSkipped < n) {
            long bytesSkipped = in.skip(n - totalBytesSkipped);
            if (bytesSkipped == 0L) {
               int b = read();
               if (b < 0) {
                  break;  // we reached EOF
               } else {
                  bytesSkipped = 1; // we read one byte
               }
            }
            totalBytesSkipped += bytesSkipped;
         }
         return totalBytesSkipped;
      }

      @Override public int read(byte[] b, int offset, int length) throws IOException {
         // there is the problem: if we throw exception in stream while
         // there is reading in the bitmapFactory, it will dump a
         // warning into log and this takes significant time. So here we
         // will not throw an exception, instead returning -1 as "no more
         // data" in acse of exception if we are cancelled.

         synchronized (ImageStreamReader.this) {
            if (_cancelled) return -1;
         }

         try {
            return in.read(b, offset, length);
         } catch(IOException e) {
            synchronized (ImageStreamReader.this) {
               if (_cancelled) return -1;
               else throw e;
            }
         }
      }


      @Override public int read(byte[] b) throws IOException {
         // see comment in first read method
         synchronized (ImageStreamReader.this){
            if (_cancelled) return -1;
         }

         try {
            return in.read(b);
         } catch(IOException e) {
            synchronized (ImageStreamReader.this) {
               if (_cancelled) return -1;
               else throw e;
            }
         }
      }


      @Override public int read() throws IOException {
         // see comment in first read method
         synchronized (ImageStreamReader.this) {
            if (_cancelled) return -1;
         }

         try {
            return in.read();
         } catch(IOException e) {
            synchronized (ImageStreamReader.this) {
               if (_cancelled) return -1;
               else throw e;
            }
         }
      }
   }
}