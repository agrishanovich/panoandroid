package com.qulix.mdtlib.images.orientation;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DecodeReorientedBitmap {

   //The size of the buffer should be large enough to be sure that first BUFFER_LENGTH bytes of the stream contain metadata.
   //According to information from:

   //http://stackoverflow.com/questions/3248946/what-is-the-maximum-size-of-jpeg-metadata,
   //http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/xmp/pdfs/XMPSpecificationPart3.pdf    ,page 19, chapter 2.1.3.1,

   //the JPEG metadata size limit is 65536.

   private static final int BUFFER_LENGTH = 65536;

   public static Bitmap decode(InputStream stream, BitmapFactory.Options options){
      BufferedInputStream bis = new BufferedInputStream(stream);
      Matrix matrix = null;

      try {
         byte[] data = new byte[BUFFER_LENGTH];

         bis.mark(BUFFER_LENGTH);
         bis.read(data);

         matrix = orientationMatrix(new ByteArrayInputStream(data));

         bis.reset();

      } catch (IOException e) {
         e.printStackTrace();
         return null;
      }

      Bitmap bmp = BitmapFactory.decodeStream(bis, null, options);

      if (matrix == null || matrix.isIdentity()){
         return bmp;
      } else {
         return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, false);
      }
   }

   private static Matrix orientationMatrix(InputStream stream){
      Matrix matrix = new Matrix();

      int exifOrientation = getOrientation(stream);

      if (exifOrientation <= ExifInterface.ORIENTATION_NORMAL) return matrix;

      switch(exifOrientation){
         case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:                           matrix.postScale(-1, 1);   break;  // top right
         case ExifInterface.ORIENTATION_ROTATE_180:       matrix.postRotate(180);                             break;  // bottom right
         case ExifInterface.ORIENTATION_FLIP_VERTICAL:    matrix.postRotate(180);  matrix.postScale(-1, 1);   break;  // bottom left
         case ExifInterface.ORIENTATION_TRANSPOSE:        matrix.postRotate(90);   matrix.postScale(-1, 1);   break;  // left top
         case ExifInterface.ORIENTATION_ROTATE_90:        matrix.postRotate(90);                              break;  // right top
         case ExifInterface.ORIENTATION_TRANSVERSE:       matrix.postRotate(270);  matrix.postScale(-1, 1) ;  break;  // right bottom
         case ExifInterface.ORIENTATION_ROTATE_270:       matrix.postRotate(270);                             break;  // left bottom
         default:                                         break;  // Unknown
      }

      return matrix;
   }

   private static int getOrientation(InputStream stream){
      int undefined = -1;

      try {
         final Metadata metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(stream), false);
         final ExifIFD0Directory exifIFD0Directory = metadata.getDirectory(ExifIFD0Directory.class);

         if(exifIFD0Directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)){
            return exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
         }
      } catch (Exception e){
         e.printStackTrace();
      }

      return undefined;
   }
}
