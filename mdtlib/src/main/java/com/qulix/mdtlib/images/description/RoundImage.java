package com.qulix.mdtlib.images.description;

public class RoundImage extends OneOriginalImageDescription {
   public RoundImage( final int radius,
                      final Description originalImage) {
      super(originalImage);
      if (radius <= 0) throw new IllegalArgumentException("radius can not be <= 0");
      if (originalImage == null) throw new IllegalArgumentException("originalImage can not be null");

      this.radius = radius;
   }

   @Override public String toString() {
      return "radius:" + radius + ":" + originalImage();
   }

   @Override public boolean equals(final Object o) {
      if (o instanceof RoundImage) {
         RoundImage that = (RoundImage)o;
         return that.radius == radius
               && that.originalImage().equals(originalImage());
      }
      return false;
   }

   @Override
   public String key() {
      return toString();
   }

   public final int radius;
}
