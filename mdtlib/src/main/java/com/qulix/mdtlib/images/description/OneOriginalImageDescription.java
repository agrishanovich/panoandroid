package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

public abstract class OneOriginalImageDescription implements Description {

   public OneOriginalImageDescription(Description originalImage) {
      _originalImage = originalImage;
   }

   @Override
   public Description originalImage() {
      return _originalImage;
   }

   @Override
   public void mutate(Translate<Description> translator) {
      _originalImage = translator.translate(_originalImage);
   }

   private Description _originalImage;

}
