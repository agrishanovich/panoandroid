package com.qulix.mdtlib.images.sources.download;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.TranslateCachedImages;

public class MaybeFromCacheSource implements Source {

   public MaybeFromCacheSource(final DownloadSource.DescriptionToUrl urlGenerator,
                               DownloadingInCacheSourceStateProvider provider) {
      _urlGenerator = urlGenerator;
      _provider = provider;
   }

   @Override
   public boolean canTryResolveAsync(Description description) {
      String url = _urlGenerator.url(description);
      return url != null && !url.isEmpty() && _provider.isDownloadingNow(url);
   }

   @Override
   public Bitmap resolve(Description description) {
      return null;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, final Description description, final Receiver<Bitmap> receiver, final Receiver<Bitmap> preScanReceiver) {
      if (_provider.isDownloadingNow(_urlGenerator.url(description))) {
         final Receiver<Bitmap> bitmapReceiver = new Receiver<Bitmap>() {
            @Override
            public void receive(Bitmap bitmap) {
               description.mutate(new TranslateCachedImages());
               _getRealImageOperation = ImageObtainer.instance().requestImage(description, receiver, preScanReceiver);
            }
         };
         final Cancellable getIntermediateImageOperation = aux.intermediateImage(description, bitmapReceiver);
         Cancellable getImageOperation = new Cancellable() {
            @Override
            public void cancel() {
               getIntermediateImageOperation.cancel();
               if (_getRealImageOperation != null) {
                  _getRealImageOperation.cancel();
                  _getRealImageOperation = null;
               }
            }
         };

         description.mutate(new TranslateCachedImages());
         return getImageOperation;
      } else {
         return ImageObtainer.instance().requestImage(description, receiver, preScanReceiver);
      }
   }

   private DownloadingInCacheSourceStateProvider _provider;
   private DownloadSource.DescriptionToUrl _urlGenerator;
   private Cancellable _getRealImageOperation = null;
}
