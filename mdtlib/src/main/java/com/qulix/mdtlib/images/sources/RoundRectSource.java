package com.qulix.mdtlib.images.sources;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.RoundCornersImage;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class RoundRectSource implements Source {

	/**
	 * Rounding rect is rather time-consuming procedure, so it can be
	 * performed only in thread.
	 */
	@Override public boolean canTryResolveAsync(final Description description) {
		// can resolve async only for RoundCornersImage descriptions
		return description instanceof RoundCornersImage;
	}

	/**
	 * Can not resove in sync way.
	 */
	@Override public Bitmap resolve(final Description description) {
		return null;
	}

	/**
	 * Resolve this image in the async way, note that it receiver can
	 * receive null in case if if no image was found.
	 */
	@Override public Cancellable resolve(final ImageObtainer.Aux aux,
										 final Description description,
										 final Receiver<Bitmap> receiver,
                                         final Receiver<Bitmap> preScanReceiver) {
		if (description instanceof RoundCornersImage) {
			_logger.log("requested image: " + description);

			RoundCornersImage roundCorners = (RoundCornersImage)description;

			return new SubrequestImage(aux,
									   roundCorners.originalImage(),
									   createPostprocessor(roundCorners.radius),
									   receiver);
		} else {
			return null;
		}
	}

	private SubrequestImage.Postprocessor createPostprocessor(final int radius) {
		return new SubrequestImage.Postprocessor() {
			@Override public Bitmap process(Bitmap source) {
				Bitmap output = Bitmap.createBitmap(source.getWidth(),
													source .getHeight(),
													Config.ARGB_8888);
				Canvas canvas = new Canvas(output);

				final int color = 0xff424242;
				final Paint paint = new Paint();
				final Rect rect = new Rect(0, 0, source.getWidth(), source.getHeight());
				final RectF rectF = new RectF(rect);
				final float roundPx = radius;

				paint.setAntiAlias(true);
				canvas.drawARGB(0, 0, 0, 0);
				paint.setColor(color);
				canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

				paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
				canvas.drawBitmap(source, rect, rect, paint);

				return output;

			}
		} ;
	}

	private static final LoggerFactory _loggerFactory = new LoggerFactory("RoundRectSource");
	private final LoggerFactory.Logger _logger = _loggerFactory.create();
}