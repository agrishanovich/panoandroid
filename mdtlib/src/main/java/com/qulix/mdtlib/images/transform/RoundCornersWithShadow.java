package com.qulix.mdtlib.images.transform;

import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;
import com.qulix.mdtlib.images.description.RoundCornersWithShadowImage;
import com.qulix.mdtlib.images.widget.LazyLoadImageView.TransformDescription;

public class RoundCornersWithShadow implements TransformDescription {
   public RoundCornersWithShadow(final int cornerRadius, final int shadowSize) {
      if (cornerRadius <= 0) throw new IllegalArgumentException("cornerRadius can not be <= 0");
      if (shadowSize <= 0) throw new IllegalArgumentException("shadowSize can not be <= 0");

      _cornerRadius = cornerRadius;
      _shadowSize = shadowSize;
   }

   @Override
   public Description transform(ResizeToFitInRect resizeToFitInRect) {
      return new RoundCornersWithShadowImage(_cornerRadius, _shadowSize, resizeToFitInRect);
   }

   private final int _cornerRadius;
   private final int _shadowSize;
}
