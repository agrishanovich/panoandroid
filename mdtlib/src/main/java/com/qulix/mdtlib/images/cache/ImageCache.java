package com.qulix.mdtlib.images.cache;

import java.util.List;

import android.graphics.Bitmap;

import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import android.annotation.TargetApi;


public class ImageCache {
	public ImageCache(ImageObtainer.Aux aux, final List<CacheLayer> layers) {
		_layers = layers;
		_aux = aux;
	}

	public void store(Description description,
					  Bitmap bitmap) {
		for (CacheLayer layer : _layers) {
			layer.store(_aux,
						description,
						bitmap);
		}
	}

	@TargetApi(12)
	public static int calculateBitmapSize(Bitmap bitmap) {
		return bitmap.getRowBytes() * bitmap.getHeight();
	}

	private final List<CacheLayer> _layers;
	private final ImageObtainer.Aux _aux;
}