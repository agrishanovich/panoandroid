package com.qulix.mdtlib.images.transform;

import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.FitByWidthImage;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;
import com.qulix.mdtlib.images.widget.LazyLoadImageView.TransformDescription;

public class FitByWidth implements TransformDescription {
   @Override
   public Description transform(ResizeToFitInRect resizeToFitInRect) {
      return new FitByWidthImage(resizeToFitInRect.width, resizeToFitInRect.height, resizeToFitInRect.originalImage());
   }
}
