package com.qulix.mdtlib.images.cache.disk;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.util.Log;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.decoder.Decoder;
import com.qulix.mdtlib.images.decoder.DecoderFactory;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ImageCachedFromUrl;

import java.io.InputStream;

/**
 * Decoder which spawned by this factory will use cache entry as temporary
 * file. Thus, downloaded file will be put in the cache during downloading.
 */
public class DecoderFactoryDecodeStoringInCache implements DecoderFactory {

	private static final int IO_BUFFER_SIZE = 8 * 1024;

	public DecoderFactoryDecodeStoringInCache(final com.qulix.mdtlib.images.cache.disk.LayerDiskLruCache diskCache) {
		_cache = diskCache;
	}

	@Override public Decoder create(final Description description) {
		return new StoreInCacheDecoder(description);
	}

	private class StoreInCacheDecoder implements Decoder {
		public StoreInCacheDecoder(final Description description) {
			_description = description;
			_decoder = _cache.createDecoder(description);
		}

		@Override public Bitmap workerThreadDecode(InputStream stream, Receiver<Bitmap> preScanReceiver) throws IOException {

         final DiskLruCache.Editor editor;

         if (_description instanceof ImageCachedFromUrl) {
            editor = _cache.edit(_description.originalImage());
         } else {
            editor = _cache.edit(_description);
         }

         if (editor == null) return null;

			final OutputStream cacheFileStream = editor.newOutputStream(LayerDiskLruCache.INDEX_OF_FILE_STREAM);

			if (copyToStream(stream, cacheFileStream)) {
				editor.commit();
			} else {
				editor.abort();
			}

			synchronized(this) {
				if (_cancelled) return null;

            if (_description instanceof ImageCachedFromUrl) {
               return Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
            }

				DiskLruCache.Snapshot snapshot = _cache.snapshot(_description);

				if (snapshot == null) return null;

            _decodeStream
                  = new BufferedInputStream(snapshot.getInputStream(LayerDiskLruCache.INDEX_OF_FILE_STREAM),
                                            IO_BUFFER_SIZE);
			}

			try {
            return _decoder.workerThreadDecode(_decodeStream, preScanReceiver);
			} finally {
				try {
					_decodeStream.close();
				} catch(Throwable ignore) {}
			}
		}

		@Override synchronized public void cancel() {
			if (_cancelled) return;

			_cancelled = true;
			_decoder.cancel();
			if (_decodeStream != null) {
				try {
					_decodeStream.close();
				} catch(Throwable ignore) {}
			}
		}

		private final Description _description;
		private final Decoder _decoder;
		private InputStream _decodeStream;
		private boolean _cancelled;
	}

	private static boolean copyToStream(InputStream inputStream, OutputStream outputStream) {
		OutputStream out = null;
		InputStream in = null;

		try {
			in = new BufferedInputStream(inputStream, IO_BUFFER_SIZE);
			out = new BufferedOutputStream(outputStream, IO_BUFFER_SIZE);

			int b;
			while ((b = in.read()) != -1) {
				out.write(b);
			}
			return true;
		} catch (final IOException e) {
			Log.e("LayerDiskLruCache", "Error in downloadBitmap - " + e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (final IOException ignore) {}


			try {
				if (in != null) {
					in.close();
				}
			} catch (final IOException ignore) {}
		}
		return false;
	}


	private final LayerDiskLruCache _cache;
}