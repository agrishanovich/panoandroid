package com.qulix.mdtlib.images.sources.download;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ImageFromAsset;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class AssetSource implements Source {

   public AssetSource(Context context) {
      _context = context;
   }

   @Override
   public boolean canTryResolveAsync(Description description) {
      return false;
   }

   @Override
   public Bitmap resolve(Description description) {
      if (description instanceof ImageFromAsset) {
         try {
            final AssetManager assetManager = _context.getAssets();
            final InputStream is = assetManager.open(((ImageFromAsset) description).url);
            Bitmap out = BitmapFactory.decodeStream(is);
            is.close();

            return out;
         } catch (IOException e) {
            _logger.log("IOException: " + e.getMessage());

            return null;
         }
      } else {
         return null;
      }
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      return null;
   }

   private final static LoggerFactory _loggerFactory = new LoggerFactory("DownloadSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
   private final Context _context;
}
