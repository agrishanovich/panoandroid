package com.qulix.mdtlib.images.transform;

import com.qulix.mdtlib.images.description.CropTopLeftImage;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;
import com.qulix.mdtlib.images.widget.LazyLoadImageView;

public class CropTopLeft implements LazyLoadImageView.TransformDescription {
   public CropTopLeft(final int width, final int height) {
      _width = width;
      _height = height;
   }

   @Override
   public Description transform(ResizeToFitInRect resizeToFitInRect) {
      return new CropTopLeftImage(_width, _height, resizeToFitInRect);
   }

   private final int _width;
   private final int _height;
}
