package com.qulix.mdtlib.images.sources;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.CropTopLeftImage;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class CropTopLeftSource implements Source {
   @Override
   public boolean canTryResolveAsync(Description description) {
      return description instanceof CropTopLeftImage;
   }

   @Override
   public Bitmap resolve(Description description) {
      return null;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      if (description instanceof CropTopLeftImage) {
         _logger.log("requested image: " + description);

         CropTopLeftImage fitByMinSideImage = (CropTopLeftImage)description;

         return new SubrequestImage(aux,
                 fitByMinSideImage.originalImage(),
                 createPostprocessor(fitByMinSideImage.width, fitByMinSideImage.height),
                 receiver);
      } else {
         return null;
      }
   }


   private SubrequestImage.Postprocessor createPostprocessor(final int width, final int height) {
      return new SubrequestImage.Postprocessor() {
         @Override public Bitmap process(Bitmap source) {

            int outWidth;

            if (source.getWidth() > width) {
               outWidth = width;
            } else {
               outWidth = source.getWidth();
            }

            int outHeight;

            if (source.getHeight() > height) {
               outHeight = height;
            } else {
               outHeight = source.getHeight();
            }

            return Bitmap.createBitmap(source,
                    0,
                    0,
                    outWidth,
                    outHeight);
         }
      } ;
   }

   private static final LoggerFactory _loggerFactory = new LoggerFactory("CropTopLeftSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
}
