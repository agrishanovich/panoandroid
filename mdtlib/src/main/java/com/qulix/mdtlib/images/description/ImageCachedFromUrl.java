package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

public class  ImageCachedFromUrl extends OneOriginalImageDescription {
   public ImageCachedFromUrl(Description originalImage) {
      super(originalImage);
   }

   @Override public String toString() {
      return "cached: " + originalImage().toString();
   }

   @Override public boolean equals(Object o) {
      return o instanceof ImageCachedFromUrl
            && originalImage().equals(((ImageCachedFromUrl) o).originalImage());
   }

   @Override
   public void mutate(Translate<Description> translator) {

   }

   @Override public String key() {
      return toString();
   }

}
