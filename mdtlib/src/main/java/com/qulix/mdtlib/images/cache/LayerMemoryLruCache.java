package com.qulix.mdtlib.images.cache;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.view.Display;
import android.view.WindowManager;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;


public final class LayerMemoryLruCache implements CacheLayer {

    /**
     * This constructor uses context to calculate deafult size of memory cache.
     *
     * This default size will be different for different devices, as it
     * depends on memory, available for application.
     */
    public LayerMemoryLruCache(final Context context) {
        this(memoryCacheSize(context));
    }

    public LayerMemoryLruCache(final int cacheSize) {

        _cache = new LruCache<String, Bitmap>(cacheSize) {
                /**
                 * Measure item size in bytes rather than units which is more practical
                 * for a bitmap cache
                 */
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return ImageCache.calculateBitmapSize(bitmap);
                }
            };
    }

    private static int memoryCacheSize(Context context) {

        final int MEMORY_CLASS_BASELINE = 16;
        final int MINIMAL_FULL_SCREEN_IMAGES_CACHE_MUST_HOLD = 4;
        final int BYTES_PER_PIXEL = 2;
        final int CACHE_SIZE_LIMIT_BYTES = 10 * 1024 * 1024;

        final Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        @SuppressWarnings("deprecation")
            final int width = display.getWidth();
        @SuppressWarnings("deprecation")
            final int height = display.getHeight();

        final int memoryClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();

        final int minimalSize = width * height * BYTES_PER_PIXEL * MINIMAL_FULL_SCREEN_IMAGES_CACHE_MUST_HOLD;

        // The more memory device have, the more space we can allow for the
        // memory cache. This will make devices with more memory request
        // images from caches and internet less frequently.
        final int calculatedSize = (minimalSize * memoryClass) / MEMORY_CLASS_BASELINE;

        return Math.min(calculatedSize,
                        CACHE_SIZE_LIMIT_BYTES);
    }

    /**
     * Memory cache does not do any async work, so this method will always
     * return false.
     */
    @Override public boolean canTryResolveAsync(final Description description) {
        return false;
    }


    /**
     * Tries to search image in cache.
     */
    @Override public Bitmap resolve(Description description) {
        final String key = description.key();

        return _cache.get(key);
    }

    /**
     * This method should not be called as canTryResolveAsync always
     * returns false.
     */
    @Override public Cancellable resolve(final ImageObtainer.Aux aux,
                                         final Description description,
                                         final Receiver<Bitmap> endReceiver,
                                         Receiver<Bitmap> preScanReceiver) {
        throw new RuntimeException("This method should not be called as canTryResolveAsync always return false");
    }

    /**
     * This stores image into image cache
     */
    @Override public void store(final ImageObtainer.Aux aux,
                                final Description description,
                                final Bitmap bitmap) {
        final String key = description.key();
        _cache.put(key, bitmap);
    }

    @Override public String toString() {
        return "LayerMemoryLruCache";
    }

    private final LruCache<String, Bitmap> _cache;
}