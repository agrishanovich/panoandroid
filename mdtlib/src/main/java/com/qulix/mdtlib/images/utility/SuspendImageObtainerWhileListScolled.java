package com.qulix.mdtlib.images.utility;



import com.qulix.mdtlib.images.engine.ImageObtainer;
import android.widget.AbsListView;

public class SuspendImageObtainerWhileListScolled implements AbsListView.OnScrollListener {

	public SuspendImageObtainerWhileListScolled() {
		this(null);
	}

	public SuspendImageObtainerWhileListScolled(final AbsListView.OnScrollListener slave) {
		_slave = slave;
	}

	@Override
    public void onScrollStateChanged(final AbsListView view, final int scrollState) {
		boolean isScrollingNow = scrollState != SCROLL_STATE_IDLE;
		ImageObtainer.instance().suspend(isScrollingNow);

		_logger.log("scroll state: " + scrollState);

		if (_slave != null) {
			_slave.onScrollStateChanged(view, scrollState);
		}
    }

	@Override
    public void onScroll(final AbsListView view,
						 final int firstVisibleItem,
						 final int visibleItemCount,
						 final int totalItemCount) {

		_logger.log("on scroll: first: " + firstVisibleItem + ", visible cnt: " + visibleItemCount + ", total cnt: " + totalItemCount);

		if (_slave != null) {
			_slave.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
		}
    }


	private static final LoggerFactory _loggerFactory = new LoggerFactory("SuspendImageObtainer");
	private final LoggerFactory.Logger _logger = _loggerFactory.create();

	private final AbsListView.OnScrollListener _slave;
}