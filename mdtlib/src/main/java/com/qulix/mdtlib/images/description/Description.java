package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

import java.io.Serializable;

/**
 * This is class for image descriptions
 */
public interface Description extends Serializable {
	public String key();
   public Description originalImage();
   public void mutate(final Translate<Description> translator);
}