package com.qulix.mdtlib.images.sources;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.FitByWidthImage;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class FitByWidthSource implements Source {
   @Override
   public boolean canTryResolveAsync(Description description) {
      return description instanceof FitByWidthImage;
   }

   @Override
   public Bitmap resolve(Description description) {
      return null;
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      if (description instanceof FitByWidthImage) {
         _logger.log("requested image: " + description);

         FitByWidthImage fitByWidthImage = (FitByWidthImage)description;

         return new SubrequestImage(aux,
                                    fitByWidthImage.originalImage(),
                                    createPostprocessor(fitByWidthImage.width),
                                    receiver);
      } else {
         return null;
      }
   }


   private SubrequestImage.Postprocessor createPostprocessor(final int width) {
      return new SubrequestImage.Postprocessor() {
         @Override public Bitmap process(Bitmap source) {
            final float scaleFactor = width / (float) source.getWidth();

            final int height = (int)(scaleFactor * source.getHeight());

            return Bitmap.createScaledBitmap(source,
                                             width,
                                             height,
                                             true);
         }
      } ;
   }

   private static final LoggerFactory _loggerFactory = new LoggerFactory("FitByWidthSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
}
