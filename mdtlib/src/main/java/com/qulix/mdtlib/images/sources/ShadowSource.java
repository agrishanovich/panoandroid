package com.qulix.mdtlib.images.sources;

import android.graphics.*;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ShadowImage;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.SubrequestImage;

public class ShadowSource implements Source {

   /**
    * Rounding rect is rather time-consuming procedure, so it can be
    * performed only in thread.
    */
   @Override public boolean canTryResolveAsync(final Description description) {
      // can resolve async only for RoundCornersImage descriptions
      return description instanceof ShadowImage;
   }

   /**
    * Can not resove in sync way.
    */
   @Override public Bitmap resolve(final Description description) {
      return null;
   }

   /**
    * Resolve this image in the async way, note that it receiver can
    * receive null in case if if no image was found.
    */
   @Override public Cancellable resolve(final ImageObtainer.Aux aux,
                                        final Description description,
                                        final Receiver<Bitmap> receiver,
                                        Receiver<Bitmap> preScanReceiver) {
      if (description instanceof ShadowImage) {
         _logger.log("requested image: " + description);

         ShadowImage shadowImage = (ShadowImage)description;

         _logger.log("sub requested image: " + ((ShadowImage) description).originalImage());

         return new SubrequestImage(aux,
                 shadowImage.originalImage(),
                 createPostprocessor(shadowImage.shadowRadius),
                 receiver);
      } else {
         return null;
      }
   }

   private SubrequestImage.Postprocessor createPostprocessor(final int shadowRadius) {
      return new SubrequestImage.Postprocessor() {
         @Override public Bitmap process(Bitmap source) {
            Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(output);

            final Rect rect = new Rect(0, 0, output.getWidth(), output.getHeight());
            final RectF rectF = new RectF(rect);
            final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

            // this is a shadow drawn around an image with "grey" (actually black with half transparency) color
            paint.setColor(0x80000000);
            canvas.drawRect(rectF, paint);

            // draw image on top of the bottom shadow
            Shader bitmapShader = new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            paint.setAlpha(0xFF); // this is just a mask to draw the whole image
            paint.setMaskFilter(new BlurMaskFilter(shadowRadius, BlurMaskFilter.Blur.INNER));
            paint.setShader(bitmapShader);
            canvas.drawRect(rectF, paint);

            return output;
         }
      } ;
   }

   private static final LoggerFactory _loggerFactory = new LoggerFactory("RoundRectWithShadowSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
}
