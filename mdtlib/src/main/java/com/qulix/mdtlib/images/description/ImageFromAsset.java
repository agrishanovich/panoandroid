package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

public class ImageFromAsset implements Description {
   public ImageFromAsset(String url) {
      if (url == null) throw new IllegalArgumentException("Url can not be null.");

      this.url = url;
   }

   @Override
   public Description originalImage() {
      return null;
   }

   @Override public boolean equals(Object o) {
      return o instanceof ImageFromAsset
              && url.equals(((ImageFromAsset)o).url);
   }

   @Override public String toString() {
      return "asset: " + url;
   }

   @Override
   public String key() {
      return toString();
   }

   @Override
   public void mutate(Translate<Description> translator) {

   }

   public final String url;
}
