package com.qulix.mdtlib.images.decoder;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;

import java.io.IOException;
import java.io.InputStream;

public interface Decoder extends Cancellable {
	Bitmap workerThreadDecode(InputStream stream, Receiver<Bitmap> preScanReceiver) throws IOException;
}
