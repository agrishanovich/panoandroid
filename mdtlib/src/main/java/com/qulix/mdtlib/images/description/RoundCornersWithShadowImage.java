package com.qulix.mdtlib.images.description;

public class RoundCornersWithShadowImage extends OneOriginalImageDescription {
   public RoundCornersWithShadowImage(final int cornerRadius,
                                      final int shadowRadius,
                                      final Description originalImage) {
      super(originalImage);
      if (cornerRadius <= 0) throw new IllegalArgumentException("cornerRadius can not be <= 0");
      if (shadowRadius <= 0) throw new IllegalArgumentException("shadowRadius can not be <= 0");
      if (originalImage == null) throw new IllegalArgumentException("originalImage can not be null");

      this.cornerRadius = cornerRadius;
      this.shadowRadius = shadowRadius;
   }

   @Override public String toString() {
      return "rnd:" + cornerRadius + ":" + "shadow:" + shadowRadius + ":" + originalImage();
   }

   @Override public boolean equals(final Object o) {
      if (o instanceof RoundCornersWithShadowImage) {
         RoundCornersWithShadowImage that = (RoundCornersWithShadowImage)o;
         return that.cornerRadius == cornerRadius
                 && that.shadowRadius == shadowRadius
                 && that.originalImage().equals(originalImage());
      }
      return false;
   }

   @Override public String key() {
      return toString();
   }

   public final int cornerRadius;
   public final int shadowRadius;

}
