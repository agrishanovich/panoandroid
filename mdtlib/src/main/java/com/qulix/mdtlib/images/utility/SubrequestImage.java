package com.qulix.mdtlib.images.utility;

import android.graphics.Bitmap;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.ImageCachedFromUrl;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;

public class SubrequestImage implements Cancellable {

    public static abstract class Postprocessor implements Cancellable {

        public final boolean isCancelled() {
            return _cancelled;
        }

        public abstract Bitmap process(Bitmap bitmap);

        public void cancel() {
            _cancelled = true;
        }

        private boolean _cancelled;
    }

    public SubrequestImage(final ImageObtainer.Aux aux,
                           final Description sourceImageDescription,
                           final Postprocessor postprocessor,
                           final Receiver<Bitmap> resultBitmapReceiver) {

        if (aux == null) throw new IllegalArgumentException("aux can not be null!");
        if (sourceImageDescription == null) throw new IllegalArgumentException("sourceImageDescription can not be null!");
        if (postprocessor == null) throw new IllegalArgumentException("postprocessor can not be null!");
        if (resultBitmapReceiver == null) throw new IllegalArgumentException("resultBitmapReceiver can not be null!");

        _aux = aux;
        _postprocessor = postprocessor;
        _resultBitmapReceiver = resultBitmapReceiver;
        final Cancellable cancellableOfIntermediateRequest
            = aux.intermediateImage(sourceImageDescription,
                                    new Receiver<Bitmap>() {
                                        @Override public void receive(Bitmap bmp) {
                                            if (bmp != null) {
                                                onIntermediateReceived(bmp);
                                            } else {
                                                resultBitmapReceiver.receive(null);
                                            }
                                        }
                                    } );

        if (cancellableOfIntermediateRequest != null) {
            _cancelMethod = cancellableOfIntermediateRequest;
        }
    }

    private void onIntermediateReceived(final Bitmap bitmap) {
        _cancelMethod = _postprocessor;

        _aux.submitWork(new Runnable() {
                @Override public void run() {
                    if (_postprocessor.isCancelled()) return ;

                    Bitmap result = _postprocessor.process(bitmap);

                    if (_postprocessor.isCancelled()) return ;

                    postResult(result);
                }
            } );
    }

    private void postResult(final Bitmap result) {
        _aux.postCT(new Runnable() {
                @Override public void run() {
                    if (_postprocessor.isCancelled()) return;

                    _resultBitmapReceiver.receive(result);
                }
            } );
    }


    @Override public void cancel() {
        if (_cancelMethod != null) {
            _cancelMethod.cancel();
            _cancelMethod = null;
        }
    }

    private final ImageObtainer.Aux _aux;
    private final Postprocessor _postprocessor;
    private final Receiver<Bitmap> _resultBitmapReceiver;

    private Cancellable _cancelMethod;
}