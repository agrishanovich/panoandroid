package com.qulix.mdtlib.images.description;

public final class RoundCornersImage extends OneOriginalImageDescription {

	public RoundCornersImage(final int radius,
							       final Description originalImage) {
      super(originalImage);
		if (radius <= 0) throw new IllegalArgumentException("radius can not be <= 0");
		if (originalImage == null) throw new IllegalArgumentException("originalImage can not be null");

		this.radius = radius;
	}

   @Override public String toString() {
		return "rnd:" + radius + ":" + originalImage();
	}

	@Override public boolean equals(final Object o) {
		if (o instanceof RoundCornersImage) {
			RoundCornersImage that = (RoundCornersImage)o;
			return that.radius == radius
				&& that.originalImage().equals(originalImage());
		}
		return false;
	}

	@Override public String key() {
		return toString();
	}

	public final int radius;
}