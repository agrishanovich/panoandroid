package com.qulix.mdtlib.images.engine;

import android.graphics.Bitmap;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;

final class Request implements Cancellable {

    // there is two types of request - external, requested by user of
    // mechanics, and intermediate, which issued by mechanics internally.
    public static enum Type {
        External,
        Intermediate
    }

    public Request(Description description,
                   Type type,
                   Receiver<Bitmap> receiver,
                   Receiver<Bitmap> preScanReceiver) {

        if (description == null) throw new IllegalArgumentException("description can not be null!");
        if (type == null) throw new IllegalArgumentException("type can not be null!");
        if (receiver == null)  throw new IllegalArgumentException("receiver can not be null!");

        this.description = description;
        this.type = type;
        this.receiver = receiver;
        this.preScanReceiver = preScanReceiver;
    }

    public void setCancelMethod(Cancellable method) {
        _cancelMethod = method;
    }

    @Override public void cancel() {
        if (_cancelMethod != null) {
            _cancelMethod.cancel();
            _cancelMethod = null;
        }
    }

    public final Description description;
    public final Type type;
    public final Receiver<Bitmap> receiver;
    public final Receiver<Bitmap> preScanReceiver;
    private Cancellable _cancelMethod;
}