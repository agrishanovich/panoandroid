package com.qulix.mdtlib.images.engine;

import java.util.ArrayList;
import java.util.List;

/**
 * This class maintains queue of MultiRequestWorker objects and controls
 * the order in which they are started.
 */
final class WorkersQueue {
    public WorkersQueue(final int countOfPureEndRequestsWorkingInParallel) {

        if (countOfPureEndRequestsWorkingInParallel <= 0) throw new IllegalArgumentException("");

        _countOfPureEndRequestsWorkingInParallel = countOfPureEndRequestsWorkingInParallel;
    }

    public void addNewWorker(final MultiRequestWorker worker) {
        _workers.add(worker);
        startAllPossibleWorkers();
    }

    public void forget(final MultiRequestWorker worker) {
        _workers.remove(worker);
        startAllPossibleWorkers();
    }

    public boolean putToCorrespondingWorker(final Request request) {
        for (final MultiRequestWorker worker : _workers) {
            if (worker.isProcessing(request.description)) {
                worker.attach(request);


                startAllPossibleWorkers();
                return true;
            }
        }

        return false;
    }


    private void startAllPossibleWorkers() {

        /**
         * The idea to keep only limited count of pure external request
         * workers running.
         *
         * We will start pure external work runners in case if there is
         * "available space" and will start all wokrers which serving any
         * intermediate request.
         */
        int countOfPureEndRequestWorkersCanBeStarted
            = _countOfPureEndRequestsWorkingInParallel - countOfPureEndRequestWorkersRunning();

        final List<MultiRequestWorker> workersToStart = new ArrayList<MultiRequestWorker>(_workers);

        for (final MultiRequestWorker worker: workersToStart) {

            if (isPureEndRequestWorker(worker)) {

                if (countOfPureEndRequestWorkersCanBeStarted > 0) {
                    countOfPureEndRequestWorkersCanBeStarted--;

                    worker.start();
                }

            } else {
                worker.start();
            }
        }
    }

    private int countOfPureEndRequestWorkersRunning() {
        int count = 0;

        for (final MultiRequestWorker worker: _workers) {

            final boolean running = worker.isStarted();

            if (running && isPureEndRequestWorker(worker)) {
                count++;
            }
        }

        return count;
    }

    private boolean isPureEndRequestWorker(final MultiRequestWorker worker) {
        return worker.haveEndRequests() && (! worker.haveIntermediateRequests());
    }

    private final List<MultiRequestWorker> _workers = new ArrayList<MultiRequestWorker>();
    private final int _countOfPureEndRequestsWorkingInParallel;
}