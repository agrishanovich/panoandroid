package com.qulix.mdtlib.images.sources.download;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ImageFromResource;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.LoggerFactory;

public class ResourceSource implements Source {

   public ResourceSource(Context context) {
      _context = context;
   }

   @Override
   public boolean canTryResolveAsync(Description description) {
      return false;
   }

   @Override
   public Bitmap resolve(Description description) {
      if (description instanceof ImageFromResource) {
         int resId = ((ImageFromResource) description).resId;
         _logger.log("decode image from resource : " + String.valueOf(resId));
         return BitmapFactory.decodeResource(_context.getResources(), resId);
      } else {
         return null;
      }
   }

   @Override
   public Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver) {
      return null;
   }

   private final static LoggerFactory _loggerFactory = new LoggerFactory("DownloadSource");
   private final LoggerFactory.Logger _logger = _loggerFactory.create();
   private final Context _context;
}