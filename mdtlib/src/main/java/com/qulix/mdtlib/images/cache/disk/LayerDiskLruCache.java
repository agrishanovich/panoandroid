package com.qulix.mdtlib.images.cache.disk;

import android.graphics.Bitmap;
import android.util.Log;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.cache.CacheLayer;
import com.qulix.mdtlib.images.decoder.Decoder;
import com.qulix.mdtlib.images.decoder.DecoderFactoryDirectDecode;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.utility.ImageStreamReader;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.LoggerFactory.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public final class LayerDiskLruCache implements CacheLayer {

	static int INDEX_OF_FILE_STREAM = 0;

	public interface Observer {
		void onStoringImage(Description description);
	}

	public LayerDiskLruCache(final File directory,
                            final int appVersion,
                            final int cacheSize) throws IOException {
		this(directory, appVersion, cacheSize, null);
	}

	public LayerDiskLruCache(final File directory,
									 final int appVersion,
									 final int cacheSize,
									 final Observer observer) throws IOException {
		LoggerFactory.log("Setting up log directory: " + directory);

		_cache = DiskLruCache.open(directory,
											appVersion,
											1, // storing only image, so only one value per key
											cacheSize);
		_directory = directory;

		_observer = observer;
	}

	/**
	 * It always possible that image contained in cache, so search can be
	 * performed.
	 */
	@Override public boolean canTryResolveAsync(final Description description) {
		return true;
	}


	/**
	 * Load image from disk is slow procedure, so it can not be performed
	 * in sync way.
	 */
	@Override public Bitmap resolve(Description description) {
		return null;
	}


	/**
	 * This method should not be called as canTryResolveAsync always
	 * returns false.
	 */
	@Override public Cancellable resolve(final ImageObtainer.Aux aux,
										 final Description description,
										 final Receiver<Bitmap> endReceiver,
                                         Receiver<Bitmap> preScanReceiver) {

		final Logger logger = _loggerFactory.create();
		final Decoder decoder = createDecoder(description);

		logger.log("trying to load: " + description);


		return new Cancellable() {
			{
				nothingFound = new Runnable() {
						@Override public void run() {
							if (_cancelled) return;

							endReceiver.receive(null);
						}
					};

				aux.submitWork(new Runnable() {
						@Override public void run() {
							workerFunction();
						}
					} );
			}

			private void workerFunction() {
				if (_cancelled) return;

				final DiskLruCache.Snapshot snapshot = snapshot(description);
				if (snapshot == null) {

					logger.log("not found in cache: " + description);

					aux.postCT(nothingFound);
					return;
				}

				final ImageStreamReader reader = new ImageStreamReader(snapshot.getInputStream(INDEX_OF_FILE_STREAM),
																	   decoder,
																	   endReceiver);

				synchronized (this) {
					if (_cancelled) {
						reader.cancel();
						return;
					}

					_reader = reader;
				}

				logger.log("starting reading for: " + description);
				_reader.run();
				logger.log("read succeed for: " + description);

				snapshot.close();
			}

			@Override public synchronized void cancel() {
				if (_cancelled) return;

				logger.log("cancelling : " + description);

				_cancelled = true;
				if (_reader != null) {
					_reader.cancel();
				}
			}

			final Runnable nothingFound;

			private ImageStreamReader _reader;
			private boolean _cancelled;
		};

	}

	/**
	 * This stores image into image cache
	 */
	@Override public void store(final ImageObtainer.Aux aux,
								final Description description,
								final Bitmap bitmap) {

		if (_observer != null) {
			_observer.onStoringImage(description);
		}

		aux.submitWork(new Runnable() {
				@Override public void run() {
					final DiskLruCache.Snapshot snapshot = snapshot(description);

					if (snapshot != null) {
						// if there is snapshot - file already in cache, no need to resave it.
						//
						// Getting snaphsot will work as "access", moving
						// file to the beginning of the access queue.
						snapshot.close();
						return ;
					}

					final DiskLruCache.Editor editor = edit(description);

               if (editor == null) {
                  LoggerFactory.log("Failed to save to cache: " + description + ": cache editor == null");

                  return;
               }

					try {
						final OutputStream output = editor.newOutputStream(INDEX_OF_FILE_STREAM);

						bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);

						output.close();

						editor.commit();
					} catch (IOException e) {
						LoggerFactory.log("Failed to save to cache: " + description + ": " + Log.getStackTraceString(e));

						try {
							editor.abort();
						} catch (IOException f) {
							LoggerFactory.log("Failed to abort writing: " + description + ": " + Log.getStackTraceString(e));
						}
					}
				}
			} );
	}

	/**
	 * This method used in DecoderFactoryDecodeStoringInCache
	 */

	DiskLruCache.Editor edit(final Description description) {
		try {
         return _cache.edit(createKey(description));
		} catch (Throwable e) {
			LoggerFactory.log("Failed to edit " + description + " because of " + Log.getStackTraceString(e));
			return null;
		}
	}

   /**
	 * This method used in DecoderFactoryDecodeStoringInCache
	 */
	DiskLruCache.Snapshot snapshot(final Description description) {
		try {
         return _cache.get(createKey(description));
		} catch (Throwable e) {
			LoggerFactory.log("Failed to open " + description + " because of " + Log.getStackTraceString(e));
			return null;
		}
	}

	/**
	 * This method used in DecoderFactoryDecodeStoringInCache
	 */

	Decoder createDecoder(final Description description) {
		return _directDecoderFactory.create(description);
	}

	private String createKey(final Description description) {

		final int limitSymbols = 240;

		// DiskLruCache uses key as filename and adds it's own postfix, so
		// we need to ensure that length of the resulting path string will
		// be less than limit symbols. In case if it not, use hash instead of
		// fixed description.
		//
		// However, in very rare conditions, this can cause loading of
		// wrong image. Unfortunately, with current implementation of disk
		// lru cache this is not easily avoidable. ):

		final String key = description.key()
			.replace("-",  "-m")
			.replace("?", "-q")
			.replace("/", "-s")
			.replace(" ", "-p")
			.replace(":", "-c")
			.replace(">", "-g")
			.replace("<", "-l");

		final String resultPath = new File(_directory,
										   key).toString();

		if (resultPath.length() <= limitSymbols) {
			return key;
		} else {
			return hashKeyForDisk(key);
		}
	}

	private static String hashKeyForDisk(String key) {
		String cacheKey;
		try {
			final MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(key.getBytes());
			cacheKey = bytesToHexString(mDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			cacheKey = String.valueOf(key.hashCode());
		}
		return cacheKey;
	}

	private static String bytesToHexString(byte[] bytes) {
		// http://stackoverflow.com/questions/332079
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}

	@Override public String toString() {
		return "LayerDiskLruCache";
	}

	private static DecoderFactoryDirectDecode _directDecoderFactory = new DecoderFactoryDirectDecode();

	private final static LoggerFactory _loggerFactory = new LoggerFactory("LayerDiskLruCache");

	private final DiskLruCache _cache;

	private final File _directory;

	private final Observer _observer;
}
