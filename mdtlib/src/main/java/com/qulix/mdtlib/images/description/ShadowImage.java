package com.qulix.mdtlib.images.description;

public class ShadowImage extends OneOriginalImageDescription {
   public ShadowImage(final int shadowRadius,
                     final Description originalImage) {
      super(originalImage);
      if (shadowRadius <= 0) throw new IllegalArgumentException("shadowRadius can not be <= 0");
      if (originalImage == null) throw new IllegalArgumentException("originalImage can not be null");

      this.shadowRadius = shadowRadius;
   }

   @Override public String toString() {
      return "shadow:" + shadowRadius + ":" + originalImage();
   }

   @Override public boolean equals(final Object o) {
      if (o instanceof RoundCornersWithShadowImage) {
         RoundCornersWithShadowImage that = (RoundCornersWithShadowImage)o;
         return that.shadowRadius == shadowRadius
                 && that.originalImage().equals(originalImage());
      }
      return false;
   }

   @Override public String key() {
      return toString();
   }

   public final int shadowRadius;

}
