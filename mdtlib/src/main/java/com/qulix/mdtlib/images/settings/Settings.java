package com.qulix.mdtlib.images.settings;

import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.cache.CacheLayer;

public final class Settings {
    public Settings(int threadsCount,
                    CacheLayer[] cacheLayers,
                    Source[] sources) {

        this.threadsCount = threadsCount;
        this.cacheLayers = cacheLayers;
        this.sources = sources;
    }

    public final int threadsCount;
    public final CacheLayer[] cacheLayers;
    public final Source[] sources;
}