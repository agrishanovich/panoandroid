package com.qulix.mdtlib.images.transform;

import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ResizeToFitInRect;
import com.qulix.mdtlib.images.description.ShadowImage;
import com.qulix.mdtlib.images.widget.LazyLoadImageView;

public class Shadow implements LazyLoadImageView.TransformDescription {
   public Shadow(final int shadowSize) {
      if (shadowSize <= 0) throw new IllegalArgumentException("shadowSize can not be <= 0");

      _shadowSize = shadowSize;
   }

   @Override
   public Description transform(ResizeToFitInRect resizeToFitInRect) {
      return new ShadowImage(_shadowSize, resizeToFitInRect);
   }

   private final int _shadowSize;
}
