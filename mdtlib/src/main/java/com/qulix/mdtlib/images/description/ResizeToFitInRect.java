package com.qulix.mdtlib.images.description;

public final class ResizeToFitInRect extends OneOriginalImageDescription {

	public ResizeToFitInRect(final int width,
							 final int height,
							 final Description originalImage) {
      super(originalImage);
		if (width <= 0) throw new IllegalArgumentException("width can not be <= 0");
		if (height <= 0) throw new IllegalArgumentException("height can not be <= 0");
		if (originalImage == null) throw new IllegalArgumentException("originalImage can not be null");

		this.width = width;
		this.height = height;
	}

   @Override public String toString() {
		return "rect:" + width + "x" + height + ":" + originalImage();
	}

	@Override public boolean equals(final Object o) {
		if (o instanceof ResizeToFitInRect) {
			ResizeToFitInRect that = (ResizeToFitInRect)o;
			return that.width == width
				&& that.height == height
				&& that.originalImage().equals(originalImage());
		}
		return false;
	}

	@Override public String key() {
		return toString();
	}

	public final int width;
	public final int height;
}