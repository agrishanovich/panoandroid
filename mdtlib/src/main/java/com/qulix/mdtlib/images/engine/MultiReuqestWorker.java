package com.qulix.mdtlib.images.engine;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.sources.Source;

import java.util.ArrayList;
import java.util.List;

final class MultiRequestWorker {

    public interface EndHandler {

        enum EndReason {
            Stopped,
            Completed
        }

        void onEnded(final MultiRequestWorker worker,
                     final Description description,
                     final Bitmap bitmap,
                     final EndReason reason);
    }

    public MultiRequestWorker(final Request request,
                              final ImageObtainer.Aux aux,
                              final List<Source> sources,
                              final EndHandler endHandler) {
        attach(request);
        _aux = aux;
        _sources = sources;
        _endHandler = endHandler;
    }

    private final Description description() {
        return _requests.get(0).description;
    }

    public final boolean isProcessing(Description description) {
        return ! _requests.isEmpty() && description().equals(description);
    }

    private boolean haveRequestsOfType(final Request.Type searchForType) {
        for (Request request : _requests) {
            if (request.type == searchForType) {
                return true;
            }
        }

        return false;
    }

    public final boolean haveEndRequests() {
        return haveRequestsOfType(Request.Type.External);
    }

    public final boolean haveIntermediateRequests() {
        return haveRequestsOfType(Request.Type.Intermediate);
    }

    public final boolean isStarted() {
        return _isStarted;
    }

    public final void start() {
        if (isStarted() || _requests.size() == 0) return;

        _isStarted = true;

        _cancelMethod
            = new ResolveCycle(_aux,
                               description(),
                               _sources,
                               new Receiver<Bitmap>() {
                                   @Override public void receive(Bitmap bitmap) {
                                       onBitmap(bitmap);
                                   }
                               },
                               new Receiver<Bitmap>() {
                                   @Override
                                   public void receive(Bitmap bitmap) {
                                       onPreScan(bitmap);
                                   }
                               });
    }

    public final void attach(final Request request) {
        _requests.add(request);
        request.setCancelMethod(new Cancellable() {
                @Override public void cancel() {
                    detach(request);
                }
            } );
    }

    private void detach(final Request request) {
        _requests.remove(request);

        stopIfNoRequests();
    }

    private void stopIfNoRequests() {
        boolean noMoreRequests = _requests.size() == 0;

        if (noMoreRequests) {
            if (_cancelMethod != null) {
                _cancelMethod.cancel();
                _cancelMethod = null;
            }

            _endHandler.onEnded(MultiRequestWorker.this,
                                null,
                                null,
                                EndHandler.EndReason.Stopped);
        }
    }

    private void onBitmap(Bitmap bitmap) {

        // if you getting this exception, this is sign that there is error
        // in one of image source which makes it fail to correctly cancel
        // on cancel method call. After calling cancel method, no callbacks
        // should be called.
        if (_requests.size() == 0) throw new IllegalStateException("Got onBitmap call when no requests");

        _endHandler.onEnded(MultiRequestWorker.this,
                            description(),
                            bitmap,
                            EndHandler.EndReason.Completed);

        while(_requests.size() > 0) {
            Request request = _requests.get(0);
            request.setCancelMethod(null);
            request.receiver.receive(bitmap);
            _requests.remove(0);
        }

        _cancelMethod = null;
    }

    private void onPreScan(Bitmap bitmap) {
        // if you getting this exception, this is sign that there is error
        // in one of image source which makes it fail to correctly cancel
        // on cancel method call. After calling cancel method, no callbacks
        // should be called.
        if (_requests.size() == 0) throw new IllegalStateException("Got onBitmap call when no requests");

        for (Request request : _requests) {
            if (null != request.preScanReceiver) {
                request.preScanReceiver.receive(bitmap);
            }
        }
    }

    private final List<Request> _requests = new ArrayList<Request>();
    private Cancellable _cancelMethod;

    private final ImageObtainer.Aux _aux;
    private final List<Source> _sources;
    private final EndHandler _endHandler;
    private boolean _isStarted;
}