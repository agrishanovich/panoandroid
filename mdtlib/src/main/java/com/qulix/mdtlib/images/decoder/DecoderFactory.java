package com.qulix.mdtlib.images.decoder;
import com.qulix.mdtlib.images.description.Description;

public interface DecoderFactory {
	Decoder create(Description description);
}