package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

public class TypeIdImage implements Description {

	public TypeIdImage(String type, String id) {
		if (type == null) throw new IllegalArgumentException("type can not be null");
		if (id == null) throw new IllegalArgumentException("id can not be null");

		this.type = type;
		this.id = id;
	}

   @Override
   public Description originalImage() {
      return null;
   }

   @Override public boolean equals(Object o) {
		return o instanceof TypeIdImage
			&& ((TypeIdImage)o).type.equals(type)
			&& ((TypeIdImage)o).id.equals(id);
	}

	@Override public String toString() {
		return "tyepid:" + type + "(" + id + ")";
	}

	@Override public String key() {
		return toString();
	}

   @Override
   public void mutate(Translate<Description> translator) {

   }

   public final String type;
	public final String id;
}