package com.qulix.mdtlib.images.utility;

import android.util.Log;

import com.qulix.mdtlib.images.engine.ImageObtainer;

public class LoggerFactory {

	public static String TAG = "ImageObtainer";

	public interface Logger {
		void log(String message);
	}

	public LoggerFactory(String c) {
		_class = c;
	}

	public Logger create() {
		final int id = _id;
		_id += 1;

		if (ImageObtainer.DEBUG) {
			return new Logger() {
				@Override public void log(String message) {
					Log.d(TAG, _class + " [" + id + "]: " + message);
				}
			};
		} else {
			return new Logger() { @Override public void log(String message) { /* do nothing */ } };
		}
	}

	public static void log(String message) {
		if (ImageObtainer.DEBUG) {
			Log.d(TAG, message);
		}
	}


	private String _class;
	private int _id;
}