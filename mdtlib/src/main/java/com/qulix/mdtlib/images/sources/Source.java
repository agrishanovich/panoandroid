package com.qulix.mdtlib.images.sources;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.engine.ImageObtainer;

import android.graphics.Bitmap;

public interface Source {


	/**
	 * This method allows to determine if source can perform some async
	 * work to obtain image.
	 */
	boolean canTryResolveAsync(Description description);

	/**
	 * Resolve this image in sync way. This method will be tried in first
	 * try to get images from sources capable of returning in sync way.
	 */
	Bitmap resolve(Description description);

    /**
     * Resolve this image in the async way.
     * Note that it receiver can receive null in case if if no image was found.
     * @param preScanReceiver  The receiver of an intermediate low-quality scan of the image.
     */
    Cancellable resolve(ImageObtainer.Aux aux, Description description, Receiver<Bitmap> receiver, Receiver<Bitmap> preScanReceiver);
}