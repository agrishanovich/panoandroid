package com.qulix.mdtlib.images.sources.download;

import android.graphics.Bitmap;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.decoder.DecoderFactory;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.ImageStreamReader;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.LoggerFactory.Logger;

import java.io.InputStream;

public final class DownloadSource implements Source {
   public interface DescriptionToUrl {
      String url(Description description);
   }

   public interface StreamFactory {
      InputStream stream(String url);
   }

   public DownloadSource(final DescriptionToUrl urlGenerator,
                         final StreamFactory streamFactory,
                         final DecoderFactory decoderFactory) {

      if (urlGenerator == null) throw new IllegalArgumentException("urlGenerator can not be null");
      if (streamFactory == null) throw new IllegalArgumentException("streamFactory can not be null");
      if (decoderFactory == null) throw new IllegalArgumentException("decoderFactory can not be null");

      _urlGenerator = urlGenerator;
      _streamFactory = streamFactory;
      _decoderFactory = decoderFactory;
   }

   @Override public boolean canTryResolveAsync(final Description description) {
      String url = _urlGenerator.url(description);
      return url != null && !url.isEmpty();
   }


   /**
    * Download source can't resolve in sync way, so this method always
    * returns null.
    */
   @Override public Bitmap resolve(Description description) {
      return null;
   }

   /**
    * This method starts downloading
    */
   @Override public Cancellable resolve(final ImageObtainer.Aux aux,
                                        final Description description,
                                        final Receiver<Bitmap> endReceiver,
                                        final Receiver<Bitmap> preScanReceiver) {
      final Logger logger = _loggerFactory.create();

      final String url = _urlGenerator.url(description);

      if (url == null || url.isEmpty()) {
         endReceiver.receive(null);
         return null;
      }

      logger.log("starting download: " + url);


      Receiver<Bitmap> receiver = new Receiver<Bitmap>() {
         @Override public void receive(Bitmap bitmap) {

            logger.log("result: " + bitmap);

            endReceiver.receive(bitmap);
         }
      };

      final ImageStreamReader downloader
            = new ImageStreamReader(_streamFactory.stream(url),
                                    _decoderFactory.create(description),
                                    receiver,
                                    preScanReceiver);

      aux.submitWork(downloader);

      return downloader;
   }

   @Override public String toString() {
      return "DownloadSource";
   }

   private final DescriptionToUrl _urlGenerator;
   private final StreamFactory _streamFactory;
   private final DecoderFactory _decoderFactory;

   private final static LoggerFactory _loggerFactory = new LoggerFactory("DownloadSource");
}