package com.qulix.mdtlib.images.cache;
import android.graphics.Bitmap;

import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.sources.Source;

public interface CacheLayer extends Source {
	void store(ImageObtainer.Aux aux, Description description, Bitmap bitmap);
}