package com.qulix.mdtlib.images.sources.download;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.decoder.DecoderFactory;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.description.ImageCachedFromUrl;
import com.qulix.mdtlib.images.engine.ImageObtainer;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.ImageStreamReader;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.LoggerFactory.Logger;

import java.util.ArrayList;
import java.util.List;

public final class CacheSource implements Source, DownloadingInCacheSourceStateProvider {

   public CacheSource(final DownloadSource.DescriptionToUrl urlGenerator,
                      final DownloadSource.StreamFactory streamFactory,
                      final DecoderFactory decoderFactory) {

      if (urlGenerator == null) throw new IllegalArgumentException("urlGenerator can not be null");
      if (streamFactory == null) throw new IllegalArgumentException("streamFactory can not be null");
      if (decoderFactory == null) throw new IllegalArgumentException("decoderFactory can not be null");

      _urlGenerator = urlGenerator;
      _streamFactory = streamFactory;
      _decoderFactory = decoderFactory;
   }

   @Override public boolean canTryResolveAsync(final Description description) {
      String url = _urlGenerator.url(description);
      return url != null && !url.isEmpty();
   }

   @Override
   public boolean isDownloadingNow(String url) {
      boolean result = false;
      for (String downloadingUrl : _downloadingUrls) {
         if (downloadingUrl.equals(url)) {
            result = true;
         }
      }
      return result;
   }

   /**
    * Download source can't resolve in sync way, so this method always
    * returns null.
    */
   @Override public Bitmap resolve(Description description) {
      return null;
   }

   /**
    * This method starts downloading
    */
   @Override public Cancellable resolve(final ImageObtainer.Aux aux,
                                        final Description description,
                                        final Receiver<Bitmap> endReceiver,
                                        Receiver<Bitmap> preScanReceiver) {
      final Logger logger = _loggerFactory.create();

      final String url = _urlGenerator.url(description);

      if (url == null || url.isEmpty()) {
         endReceiver.receive(null);
         return null;
      }

      logger.log("starting download: " + url);

      _downloadingUrls.add(url);

      Receiver<Bitmap> receiver = new Receiver<Bitmap>() {
         @Override public void receive(Bitmap bitmap) {
            logger.log("result: " + bitmap);
            _downloadingUrls.remove(url);
            endReceiver.receive(bitmap);
         }
      };

      final ImageStreamReader downloader
            = new ImageStreamReader(_streamFactory.stream(url),
                                    _decoderFactory.create(description),
                                    receiver);

      aux.submitWork(downloader);
      Cancellable cancellable = new Cancellable() {
         @Override
         public void cancel() {
            _downloadingUrls.remove(url);
            downloader.cancel();
         }
      };

      return cancellable;
   }

   @Override public String toString() {
      return "CacheSource";
   }

   private final DownloadSource.DescriptionToUrl _urlGenerator;
   private final DownloadSource.StreamFactory _streamFactory;
   private final DecoderFactory _decoderFactory;
   private List<String> _downloadingUrls = new ArrayList<String>();

   private final static LoggerFactory _loggerFactory = new LoggerFactory("CacheSource");
}