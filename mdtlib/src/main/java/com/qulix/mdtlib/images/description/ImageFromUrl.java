package com.qulix.mdtlib.images.description;

import com.qulix.mdtlib.images.utility.Translate;

public final class ImageFromUrl implements Description {
   public ImageFromUrl(String url) {
      if (url == null) throw new IllegalArgumentException("Url can not be null.");

      this.url = url;
   }

   @Override
   public Description originalImage() {
      return null;
   }

   @Override public String toString() {
      return "url: " + url;
   }

   @Override public boolean equals(Object o) {
      return o instanceof ImageFromUrl
            && url.equals(((ImageFromUrl) o).url);
   }

   @Override public String key() {
      return toString();
   }

   @Override
   public void mutate(Translate<Description> translator) {

   }

   public final String url;
}