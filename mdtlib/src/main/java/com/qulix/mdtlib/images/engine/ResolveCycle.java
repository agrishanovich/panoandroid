package com.qulix.mdtlib.images.engine;

import android.graphics.Bitmap;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.images.description.Description;
import com.qulix.mdtlib.images.sources.Source;
import com.qulix.mdtlib.images.utility.LoggerFactory;
import com.qulix.mdtlib.images.utility.LoggerFactory.Logger;

import java.util.LinkedList;
import java.util.List;

final class ResolveCycle implements Cancellable {
    public ResolveCycle(final ImageObtainer.Aux aux,
                        final Description description,
                        final List<Source> sources,
                        final Receiver<Bitmap> receiver,
                        final Receiver<Bitmap> preScanReceiver) {

        if (aux == null) throw new IllegalArgumentException("aux can not be null!");
        if (description == null) throw new IllegalArgumentException("description can not be null!");
        if (sources == null) throw new IllegalArgumentException("sources can not be null!");
        if (receiver == null) throw new IllegalArgumentException("receiver can not be null!");

        _logger.log("started for image: " + description);

        _aux = aux;
        _description = description;
        _sourcesLeftToTry = new LinkedList<Source>(sources);
        _receiver = receiver;
        _preScanReceiver = preScanReceiver;


        nextStep();
    }

    @Override public void cancel() {
        if (_cancelMethod != null) {
            _logger.log("cancelled");
            _cancelMethod.cancel();
            _cancelMethod = null;
        }
    }

    private void nextStep() {

        final Source nextSourceToTry = nextSourceToTry();

        if (nextSourceToTry == null) {
            _logger.log("no more sources available");

            _receiver.receive(null);
            _cancelMethod = null;
            return;
        }

        _logger.log("trying from source: " + nextSourceToTry);

        _cancelMethod = nextSourceToTry.resolve(_aux,
                                                _description,
                                                new Receiver<Bitmap>() {
                                                    @Override public void receive(Bitmap bitmap) {

                                                        _logger.log("source " + nextSourceToTry + " returned " + bitmap);

                                                        if (bitmap == null) {
                                                            nextStep();
                                                        } else {
                                                            _cancelMethod = null;
                                                            _receiver.receive(bitmap);
                                                        }
                                                    }
                                                },
                                                _preScanReceiver);
    }

    private Source nextSourceToTry() {
        while (_sourcesLeftToTry.size() > 0) {
            Source source = _sourcesLeftToTry.get(0);

            _sourcesLeftToTry.remove(0);

            if (source.canTryResolveAsync(_description)) {
                return source;
            }
        }

        return null;
    }


    public static boolean resolveSynchronously(final List<Source> sources,
                                               final Request request) {

        final Description description = request.description;

        for (Source source : sources) {

            final Bitmap bitmap = source.resolve(description);

            if ( bitmap != null ) {
            	LoggerFactory.log("image " + description + " resolved synchronously from source " + source);
                request.receiver.receive(bitmap);
                return true;
            }
        }

        return false;
    }


    private final Description _description;
    private final List<Source> _sourcesLeftToTry;
    private final Receiver<Bitmap> _receiver;
    private final Receiver<Bitmap> _preScanReceiver;
    private final ImageObtainer.Aux _aux;

    private final static LoggerFactory _loggerFactory = new LoggerFactory("ResolveCycle");
    private final Logger _logger = _loggerFactory.create();

    private Cancellable _cancelMethod;
}