package com.qulix.mdtlib.api;

import com.qulix.mdtlib.functional.Cancellable;
import org.apache.http.HttpResponse;

public interface ApiResponseHandlerInterface extends Cancellable {
   public void handleResponse(String data, HttpResponse httpResponse);
   public void handleError();
}
