package com.qulix.mdtlib.api;

import com.qulix.mdtlib.pair.Pair;

public interface AuthorizationData {
   public Pair<String, String> authData();
}
