package com.qulix.mdtlib.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DomainError {

   public DomainError() {

   }

   public DomainError(final int code, final String source, String description) {
      _code = code;
      _source = source;
      _description = description;
   }

   public int code() {
      return _code;
   }

   public String source() {
      return _source;
   }

   public String description() {
      return _description;
   }

   @JsonProperty("code")
   private int _code;

   @JsonProperty("src")
   private String _source;

   @JsonProperty("desc")
   private String _description;
}
