package com.qulix.mdtlib.api.utility;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

public class XmlUtility {

   public interface Filter {
      boolean accept(Node node);
   }

   public static class TypeFilter implements Filter {
      public TypeFilter(int type) {
         _type = type;
      }

      @Override public boolean accept(Node node) {
         return node.getNodeType() == _type;
      }

      private int _type;
   }

   public static class NameFilter implements Filter {
      public NameFilter(String name) {
         _name = name;
      }

      @Override public boolean accept(Node node) {
         return _name.equals(node.getNodeName());
      }

      private String _name;
   }

   public static class HaveAttributeFilter implements Filter {
      public HaveAttributeFilter(String attribute, String value) {
         _attribute = attribute;
         _value = value;
      }

      public HaveAttributeFilter(String attribute) {
         this(attribute, null);
      }

      @Override public boolean accept(Node node) {
         Element element = (Element)node;
         String attribute = element.getAttribute(_attribute);

         if (_value != null) {
            return _value.equals(attribute);
         } else {
            return attribute != null;
         }
      }

      private String _attribute;
      private String _value;
   }

   public static class AttributeContainsStringFilter implements Filter {
      public AttributeContainsStringFilter(String attribute, String value) {
         _attribute = attribute;
         _value = value;
      }

      @Override public boolean accept(Node node) {
         Element element = (Element)node;
         String attribute = element.getAttribute(_attribute);

         return attribute != null && _value != null && attribute.contains(_value);
      }

      private String _attribute;
      private String _value;
   }

   public static class AndFilter implements Filter {
      public AndFilter(Filter... filters) {
         _filters = filters;
      }

      @Override public boolean accept(Node node) {
         for (Filter filter : _filters) {
            if ( ! filter.accept(node) ) {
               return false;
            }
         }

         return true;
      }

      private Filter[] _filters;
   }

   public static Node firstChild(Node node, String name) {

      NodeList childNodes = node.getChildNodes();

      final int count = childNodes.getLength();

      for (int a = 0; a < count; ++a) {
         Node item = childNodes.item(a);

         if (item.getNodeName().equals(name)) return item;
      }

      return null;
   }

   public static Node lastChild(Node node, String name) {

      NodeList childNodes = node.getChildNodes();

      final int count = childNodes.getLength();

      for (int a = count - 1; a > 0; --a) {
         Node item = childNodes.item(a);

         if (item.getNodeName().equals(name)) return item;
      }

      return null;
   }

   public static List<Node> childNodes(Node node) {
      return childNodes(node, new Filter() {
            @Override public boolean accept(Node n) {
               return true;
            }} );
   }

   public static List<Node> childNodes(Node node,
                                       Filter filter) {

      List<Node> out = new ArrayList<Node>();

      NodeList childNodes = node.getChildNodes();

      for (int a = 0; a < childNodes.getLength(); ++a) {
         Node childNode = childNodes.item(a);

         if (filter.accept(childNode)) {

            out.add(childNode);
         }
      }

      return out;
   }

   public static String textFromNode(Node node) {
      final NodeList list = node.getChildNodes();
      final int count = list.getLength();

      StringBuilder builder = new StringBuilder();

      for (int a = 0; a < count; ++a) {
         // there is additional fix for the problem with the dom
         // parser. Until 2.2 Android does not correctly support xml
         // &#13; -like escaping (character references by number).
         //
         // See: http://code.google.com/p/android/issues/detail?id=2607
         //
         // so it will be emulated.

         Node subnode = list.item(a);

         String value = subnode.getNodeValue();

         if (value != null) {
            builder.append(value);
         } else {
            // in this case there is possible number in name. There
            // is two possibilities:

            String name = subnode.getNodeName();

            String valueString = null;
            int base = 0;

            if (name != null) {

               if (name.startsWith("#x")) {

                  valueString = name.substring(2);
                  base = 16;

               } else if (name.startsWith("#")) {
                  valueString = name.substring(1);

                  base = 10;
               }

            }

            if (valueString != null) {
               try {

                  char character = (char)Integer.parseInt(valueString, base);

                  builder.append(character);
               } catch(Exception e) {
                  // we can not do more
               }
            }
         }
      }

      return builder.toString().replace("\r\n", "\n").replace("\r", "\n");
   }



}