package com.qulix.mdtlib.api;

import android.util.Log;
import com.fasterxml.jackson.core.type.TypeReference;
import com.qulix.mdtlib.functional.Continuation;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.http.HttpBody;
import com.qulix.mdtlib.http.HttpMethod;
import com.qulix.mdtlib.http.HttpRequestDescription;
import com.qulix.mdtlib.http.HttpStreamResolver;
import com.qulix.mdtlib.json.JsonParser;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.SimpleOperation;
import com.qulix.mdtlib.pair.Pair;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public abstract class ServerAPI {

   final static boolean ENABLE_DEBUG_LOGS = true;
   private static final String TAG = "ServerAPI";

   protected ServerAPI(final int foregroundQueueSize, final int backgroundQueueSize) {
      _foregroundQueue = new RequestsQueue("ForegroundQueue", foregroundQueueSize);
      _backgroundQueue = new RequestsQueue("BackgroundQueue", backgroundQueueSize);
   }

   public abstract String apiEndpoint();

   public abstract <DataType, ErrorData> ApiResponseHandler<DataType, ErrorData> createResponseHandler(final ServerAPI.ServerDataReceiver<DataType> receiver,
                                                                                                       final TypeReference<DataType> dataType,
                                                                                                       final Operation operation,
                                                                                                       final boolean ignoreResponseData);

   public <ResponseType> Operation executeApiCall(Request<ResponseType> request) {
      return execute(request.requestDescription(),
                     serverDataReceiver(request.receiver(),
                                        request.headersReceiver()),
                     request.dataType(),
                     request.ignoreResponseData());
   }

   private <DataType, ErrorData> Operation execute(final HttpRequestDescription request,
                                                   final ServerDataReceiver<DataType> receiver,
                                                   final TypeReference<DataType> dataType,
                                                   final boolean ignoreResponseData) {

      final Operation operation = new SimpleOperation();
      ++_debugRequestNumber;
      dumpLog("Request (" + _debugRequestNumber + "): " + request);
      final ApiResponseHandler<DataType, ErrorData> responseHandler = createResponseHandler(receiver,
                                                                                            dataType,
                                                                                            operation,
                                                                                            ignoreResponseData);
      ResultProcessor<DataType, ErrorData> reader = new ResultProcessor<DataType, ErrorData>(responseHandler, _debugRequestNumber);

      final Cancellable serverOperation = new ApiResponseReader(HttpStreamResolver.instance().resolveToStream(request, reader), reader);
      final Runnable terminate = new Runnable() {
         @Override
         public void run() {
            serverOperation.cancel();
            responseHandler.cancel();
         }
      };
      operation.endedEvent().subscribe(terminate);

      return operation;
   }

   private class ResultProcessor<DataType, ErrorData> implements ApiResponseReader.ResultProcessor, Receiver<HttpResponse> {
      private HttpResponse _httpResponse;
      private ApiResponseHandler<DataType, ErrorData> _responseHandler;
      private int _requestNumber;

      ResultProcessor(ApiResponseHandler<DataType, ErrorData> responseHandler, int requestNumber) {
         _responseHandler = responseHandler;
         _requestNumber = requestNumber;
      }

      @Override
      public void receive(HttpResponse httpResponse) {
         _httpResponse = httpResponse;
      }

      @Override
      public void onString(final String data) {
         dumpLog("Request (" + _requestNumber + ") received string: " + data);
         _responseHandler.handleResponse(data, _httpResponse);
      }

      @Override
      public void onError() {
         _responseHandler.handleError();
      }
   }

   public interface ServerDataReceiver<T> {
      void receive(int httpStatus, T data, Header[] headers, Throwable exception);
   }

   protected <DataType> ServerDataReceiver<DataType> serverDataReceiver(final Continuation<DataType> receiver,
                                                                        final Maybe<Receiver<Header[]>> headersReceiver) {

      return new ServerDataReceiver<DataType>() {
         @Override
         public void receive(int httpStatus, DataType data, Header[] headers, Throwable exception) {
            if (headers != null && headersReceiver.isValue()) {
               headersReceiver.value().receive(headers);
            }

            if (exception == null) {
               receiver.receive(data);
            } else {
               receiver.catchException(exception);
            }
         }
      };
   }

   public <RequestMessage> HttpRequestDescription createHttpRequest(String apiCall, HttpMethod method, RequestMessage message, List<Pair<String, String>> headers) {
      String requestContent = null;
      try {
         if (null != message) {
            requestContent = new JsonParser().toJson(message);
         }

      } catch (IOException e) {
         Log.e("ServerAPI", "Exception during json serialization: " + e.getMessage());
         throw new RuntimeException(e);
      }

      HttpRequestDescription httpRequest;
      try {
         httpRequest = new HttpRequestDescription(method, apiEndpoint() + apiCall, new HttpBody((null != requestContent ? requestContent : "").getBytes("UTF-8")));
         addHeadersIfNeed(httpRequest, headers);
      }
      catch (UnsupportedEncodingException e) {
         throw new RuntimeException(e);
      }
      dumpLog("Request (" + (_debugRequestNumber + 1)  + ") body:" + requestContent);
      return httpRequest;
   }

   private void addHeadersIfNeed(HttpRequestDescription requestDescription, List<Pair<String, String>> headers) {
      if (headers != null) {
         for (Pair<String, String> header : headers) {
            requestDescription.addHttpHeader(header.first, header.second);
         }
      }
   }

   public HttpRequestDescription createMultiPartHttpRequest(String apiCall, HttpMethod method, String mimeType, InputStream stream, List<Pair<String, String>> headers) {
      final List<Pair<String, ContentBody>> multipartParts = new ArrayList<Pair<String, ContentBody>>();
      // add image
      multipartParts.add(Pair.create("image",
                                     (ContentBody)new InputStreamBody(stream, ContentType.create(mimeType, "UTF-8"), "photo.jpg")));
      HttpRequestDescription requestDescription = new HttpRequestDescription(method, apiEndpoint() + apiCall, new HttpBody(multipartParts));
      addHeadersIfNeed(requestDescription, headers);
      return requestDescription;
   }

   public HttpRequestDescription createMultiPartHttpRequest(HttpMethod method, String apiCall, File file) {
      return new HttpRequestDescription(method, apiEndpoint() + apiCall, new HttpBody(file));
   }

   public <RequestMessage> HttpRequestDescription createHttpRequest(String apiCall, HttpMethod method, List<Pair<String, String>> headers) {
      return createHttpRequest(apiCall, method, null, headers);
   }

   protected static void dumpLogError(String logError) {
      if (ENABLE_DEBUG_LOGS) {
         Log.e(TAG, logError);
      }
   }

   protected void dumpLog(String logData) {
      if (ENABLE_DEBUG_LOGS) {
         Log.d(TAG, logData);
      }
   }

   Operation inForeGround(final Request request) {
      RequestHolder requestHolder = new RequestHolder(request, this) {
         @Override
         public void cancel() {
            terminate();
            _foregroundQueue.remove(this);
         }
      };
      _foregroundQueue.add(requestHolder);

      return requestHolder;
   }

   Operation inBackGround(final Request request) {
      RequestHolder requestHolder = new RequestHolder(request, this) {
         @Override
         public void cancel() {
            terminate();
            _backgroundQueue.remove(this);
         }
      };
      _backgroundQueue.add(requestHolder);

      return requestHolder;
   }

   private int _debugRequestNumber = 0;
   private final RequestsQueue _foregroundQueue;
   private final RequestsQueue _backgroundQueue;
}
