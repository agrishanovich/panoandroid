package com.qulix.mdtlib.api;


import android.util.Log;

import com.qulix.mdtlib.exchange.InputData;
import com.qulix.mdtlib.exchange.JsonInputData;
import com.qulix.mdtlib.exchange.Reader;
import com.qulix.mdtlib.functional.Receiver;

public abstract class ReadFromJsonData<DataType> implements ApiResponseReader.ResultProcessor {


   public ReadFromJsonData(Reader<DataType> reader,
                           Receiver<DataType> receiver) {
      _reader = reader;
      _receiver = receiver;
   }

   @Override public void onString(String data) {
      try {
         InputData inputData = new JsonInputData(data);

         DataType outputData = _reader.read(inputData,
                                            InputData.TOPLEVEL);

         _receiver.receive(outputData);

      } catch (JsonInputData.ParsingError e) {
         Log.e("ReadFromJsonData",
               "Reading response error: " + Log.getStackTraceString(e));

         onError();
     } catch (InputData.ElementException e) {

         Log.e("ReadFromJsonData",
               "Reading response error: " + Log.getStackTraceString(e));

         onError();
      }
   }

   private final Reader<DataType> _reader;
   private final Receiver<DataType> _receiver;
}