package com.qulix.mdtlib.api;


import android.util.Log;

import com.qulix.mdtlib.exchange.InputData;
import com.qulix.mdtlib.exchange.Reader;
import com.qulix.mdtlib.functional.Receiver;

public abstract class ReadFromInputData<DataType> implements ApiResponseReader.ResultProcessor {

   public interface Factory {
      InputData create(String string) throws InputData.ElementException;
   }

   public ReadFromInputData(Factory factory,
                            Reader<DataType> reader,
                            Receiver<DataType> receiver) {
      _factory = factory;
      _reader = reader;
      _receiver = receiver;
   }

   @Override public void onString(String data) {
      try {
         InputData inputData = _factory.create(data);

         DataType outputData = _reader.read(inputData,
                                            InputData.TOPLEVEL);

         _receiver.receive(outputData);

      } catch (InputData.ElementException e) {

         Log.e("ReadFromJsonData",
               "Reading response error: " + Log.getStackTraceString(e));

         onError();
      }
   }

   private final Factory _factory;
   private final Reader<DataType> _reader;
   private final Receiver<DataType> _receiver;
}