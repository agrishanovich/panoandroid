package com.qulix.mdtlib.api;

import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.SimpleOperation;

public abstract class RequestHolder extends SimpleOperation implements Executable, Cancellable {
   public RequestHolder(Request request, ServerAPI server) {
      if (request == null) {
         throw new IllegalArgumentException("Request can't be null");
      }

      _request = request;
      _server = server;
      selfEndedEvent().subscribe(new Runnable() {
         @Override
         public void run() {
            cancel();
         }
      });
   }

   @Override
   public Operation execute() {
      return _server.executeApiCall(_request);
   }

   private Request _request;
   private ServerAPI _server;
}
