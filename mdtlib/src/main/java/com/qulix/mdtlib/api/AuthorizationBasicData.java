package com.qulix.mdtlib.api;

import android.util.Base64;
import com.qulix.mdtlib.pair.Pair;

public class AuthorizationBasicData implements AuthorizationData {

   public AuthorizationBasicData(String login, String password) {
      _login = login;
      _password = password;
   }

   @Override
   public Pair<String, String> authData() {
      String encodedString = Base64.encodeToString((_login + ":" + _password).getBytes(), Base64.NO_WRAP);
      return new Pair<String, String>("Authorization", "Basic token=\"" + encodedString + "\"");
   }

   private String _login;
   private String _password;
}
