package com.qulix.mdtlib.api;
import java.io.IOException;
import java.io.InputStream;

import android.util.Log;

import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.concurrency.ThreadPool;
import com.qulix.mdtlib.concurrency.Worker;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.utils.StreamUtils;

public final class ApiResponseReader implements Cancellable {

   private static final int THREAD_POOL_SIZE = 3;

   public interface ResultProcessor {
      void onString(String data);
      void onError();
   }

   public ApiResponseReader(InputStream stream,
                            ResultProcessor processor) {
      this(null, stream, processor);
   }

   public ApiResponseReader(Worker worker,
                            InputStream stream,
                            ResultProcessor processor) {
      if (worker != null) {
         _worker = worker;
      }

      _stream = stream;
      _processor = processor;

      startThread();
   }

   private void startThread() {
      _worker.submit(new Runnable() {
            @Override public void run() {
               readFunction();
            }
         } );
   }

   private void readFunction() {
      synchronized(this) {
         if (_cancelled) return ;
      }

      try {

         InputStream streamToRead = null;
         synchronized(this) {
            streamToRead = _stream;
         }

         if (streamToRead != null) {
            passString(streamToString(streamToRead));
         }

      } catch(IOException e) {

         synchronized(this) {
            if (_cancelled) return ;
            passError(e);
         }

      } finally {
         closeStream();
      }
   }

   private synchronized void closeStream() {
      if (_stream != null) {
         try {
            _stream.close();
         } catch (IOException e) {
            throw new RuntimeException("Closing stream lead to exception!");
         }
         _stream = null;
      }
   }

   /**
    * Thread affinity: any.
    */
   @Override public synchronized void cancel() {
      _cancelled = true;

      closeStream();
   }

   /**
    * Thread affinity: any.
    */
   private void passError(final Exception e) {
      CTHandler.post(new Runnable() {
            @Override public void run() {
               if (_cancelled) return ;

               Log.e("ApiResponseReader", "Error: " + e);
               _processor.onError();
            }
         });
   }

   /**
    * Thread affinity: any.
    */
   private void passString(final String data) {
      CTHandler.post(new Runnable() {
         @Override public void run() {
            if (_cancelled) return ;

            _processor.onString(data);
         }
      });
   }

   private static final String streamToString(InputStream stream) throws IOException {
      return StreamUtils.readAll(stream);
   }

   private static Worker _sharedWorker = ThreadPool.newWithBackgroundPriority(THREAD_POOL_SIZE, "Api response reading pool");
   private Worker _worker = _sharedWorker;

   final private ResultProcessor _processor;

   private InputStream _stream;

   private boolean _cancelled;
}