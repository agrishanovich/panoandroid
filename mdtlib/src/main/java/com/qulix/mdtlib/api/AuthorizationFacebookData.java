package com.qulix.mdtlib.api;

import com.qulix.mdtlib.pair.Pair;

public class AuthorizationFacebookData implements AuthorizationData {

   public AuthorizationFacebookData(String facebookToken) {
      _facebookToken = facebookToken;
   }

   @Override
   public Pair<String, String> authData() {
      return new Pair<String, String>("Authorization", "Facebook token=\"" + _facebookToken + "\"");
   }

   private String _facebookToken;
}
