package com.qulix.mdtlib.api;

import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.utils.StreamUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileResponseReader extends ResponseReader<FileResponseReader.ResultProcessor> {

   public interface ResultProcessor extends ResponseReader.ResultProcessor {
      void onSuccess();
   }

   public FileResponseReader(InputStream stream,
                             String pathToOutputFile,
                             ResultProcessor processor) {
      super(null, stream, processor);
      _pathToOutputFile = pathToOutputFile;
      startThread();
   }

   @Override
   protected void resolveStream(InputStream streamToRead) throws IOException {
      streamToFile(streamToRead, _pathToOutputFile);

      CTHandler.post(new Runnable() {
         @Override
         public void run() {
            if (_cancelled) return;

            _processor.onSuccess();
         }
      });
   }

   private void streamToFile(InputStream stream, String pathToOutputFile) throws IOException {
      File outputFile = new File(pathToOutputFile);
      FileOutputStream outputStream = new FileOutputStream(outputFile, false);
      StreamUtils.copyStream(stream, outputStream);
      outputStream.close();
   }

   private String _pathToOutputFile;
}