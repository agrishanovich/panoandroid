package com.qulix.mdtlib.api;

import android.text.TextUtils;
import android.util.Log;
import com.fasterxml.jackson.core.type.TypeReference;
import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.concurrency.ThreadPool;
import com.qulix.mdtlib.concurrency.Worker;
import com.qulix.mdtlib.json.JsonParser;
import com.qulix.mdtlib.operation.Operation;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

import java.net.ProtocolException;

public abstract class ApiResponseHandler<DataType, ErrorData> implements ApiResponseHandlerInterface {

   private final static String TAG = "ApiResponseHandler";

   private final static int MIN_SUCCESS_STATUS_CODE = 200;
   private final static int MAX_SUCCESS_STATUS_CODE = 299;
   private final static int FAILED_AUTHORIZATION_CODE = 401;
   private final static int FORBIDDEN_CODE = 403;
   private final static int NOT_FOUND_CODE = 404;
   private final static int CONFLICT_CODE = 409;

   public ApiResponseHandler(final ServerAPI.ServerDataReceiver<DataType> receiver,
                             final  TypeReference<DataType> dataType,
                             final  Operation operation,
                             final boolean ignoreResponseData) {
      _receiver = receiver;
      _dataType = dataType;
      _operation = operation;
      _ignoreResponseData = ignoreResponseData;
   }

   @Override
   public void handleResponse(final String data,
                              final HttpResponse httpResponse) {
      submitParseResponse(data, httpResponse);
   }

   @Override
   public void handleError() {
      passResult(0, null, null, new ProtocolException("Failed to retrieve response"));
   }


   private void submitParseResponse(final String data, final HttpResponse httpResponse) {
      _worker.submit(new Runnable() {
         @Override
         public void run() {
            parseResponse(data, httpResponse);
         }
      });
   }

   protected void parseResponse(final String data, final HttpResponse httpResponse) {
      DataType parsedResponse = null;
      Exception exception = null;
      final int statusCode = httpResponse.getStatusLine().getStatusCode();
      if (statusCode >= MIN_SUCCESS_STATUS_CODE && statusCode <= MAX_SUCCESS_STATUS_CODE) {
         if (!TextUtils.isEmpty(data) && !_ignoreResponseData) {
            try {
               parsedResponse = parseData(data, _dataType);
            } catch (Exception e) {
               exception = e;
               Log.e(TAG, "Exception during json parsing: " + e.getMessage());
            }
         }
      } else if (statusCode == FAILED_AUTHORIZATION_CODE) {
         exception = new AuthorizationException();
      } else if (statusCode == NOT_FOUND_CODE) {
         exception = new NotFoundException();
      } else if (statusCode == FORBIDDEN_CODE) {
         exception = new ForbiddenException();
      } else if (statusCode == CONFLICT_CODE) {
         exception = new ConflictException();
      } else {
         try {
            ErrorData errorData = parseData(data, errorBuilder().errorDataType());
            exception = new DomainException(errorBuilder().convert(errorData));
         } catch (Exception e) {
            exception = e;
            Log.e(TAG, "Exception during json parsing: " + e.getMessage());
         }
      }

      passResult(statusCode, parsedResponse, httpResponse.getAllHeaders(), exception);
   }

   protected <T> T parseData(String data, TypeReference<T> typeReference) throws Exception {
      return new JsonParser().fromJson(data, typeReference);
   }

   protected abstract ErrorBuilder<ErrorData> errorBuilder();

   public interface ErrorBuilder <ErrorData> {
      TypeReference<ErrorData> errorDataType();
      DomainError convert(ErrorData errorData);
   }

   @Override
   public void cancel() {
      _cancelled = true;
   }

   protected void passResult(final int statusCode, final DataType resultResponse, final Header[] headers, final Exception resultException) {
      CTHandler.post(new Runnable() {
         @Override
         public void run() {
            if (_cancelled) return;
            _receiver.receive(statusCode, resultResponse, headers, resultException);
            _operation.terminate();
         }
      });
   }

   private final ServerAPI.ServerDataReceiver<DataType> _receiver;
   private final TypeReference<DataType> _dataType;
   private final Operation _operation;
   private final boolean _ignoreResponseData;
   private boolean _cancelled;
   private Worker _worker = ThreadPool.newWithBackgroundPriority(1, "ApiResponseHandler");
}
