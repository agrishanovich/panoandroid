package com.qulix.mdtlib.api;

import com.qulix.mdtlib.operation.Operation;

public class RequestExecutor {
   public RequestExecutor(Request request, ServerAPI server) {
      _request = request;
      _server = server;
   }

   public Operation inBackGround() {
      if (_executed) throw new IllegalStateException("Request already executed!");

      _executed = true;

      return _server.inBackGround(_request);
   }

   public Operation inForeGround() {
      if (_executed) throw new IllegalStateException("Request already executed!");

      _executed = true;

      return _server.inForeGround(_request);
   }

   @Override
   protected void finalize() throws Throwable {
      if (!_executed) throw new IllegalStateException("Request not executed yet! It seems no inBackGround nor inForeGround called!");

      super.finalize();
   }

   private Request _request;
   private ServerAPI _server;
   private boolean _executed;
}
