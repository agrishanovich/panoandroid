package com.qulix.mdtlib.api;

public class AuthorizationException extends Exception {
   @Override
   public String getMessage() {
      return "Authorization failed";
   }
}
