package com.qulix.mdtlib.api;


public class DomainException extends Exception {

   DomainException(DomainError domainError) {
      _domainError = domainError;
   }

   public DomainError errorDetails() {
      return _domainError;
   }

   private DomainError _domainError;
}
