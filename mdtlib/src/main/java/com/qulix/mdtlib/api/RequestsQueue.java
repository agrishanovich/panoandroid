package com.qulix.mdtlib.api;

import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.utils.Validate;

import java.util.*;

public class RequestsQueue {
   public RequestsQueue(String tag, int parallelRequestsCount) {
      _requests = new ArrayList<RequestHolder>();
      _tag = tag;
      _parallelRequestsCount = parallelRequestsCount;
      _currentRequests = new HashMap<RequestHolder, Operation>(parallelRequestsCount);
   }

   public void add(RequestHolder request) {
      Validate.argNotNull(request, "request");
      _requests.add(request);
      executeNext();
   }

   private void executeNext() {
      if (_currentRequests.size() < _parallelRequestsCount && _requests.size() > 0) {
         final RequestHolder currentRequest = _requests.remove(0);
         Operation currentOperation = currentRequest.execute();
         _currentRequests.put(currentRequest, currentOperation);
         currentOperation.endedEvent().subscribe(new Runnable() {
            @Override
            public void run() {
               Operation toCancel = _currentRequests.remove(currentRequest);
               if (toCancel != null) {
                  currentRequest.cancel();
               }
               executeNext();
            }
         });

      }
   }

   public void remove(RequestHolder request) {
      Operation toTerminate = _currentRequests.remove(request);
      if (toTerminate != null) {
         toTerminate.terminate();
      }
      _requests.remove(request);

      executeNext();
   }

   public void clear() {
      _requests.clear();

      Iterator<RequestHolder> iterator = _currentRequests.keySet().iterator();
      while (iterator.hasNext()) {
         RequestHolder request = iterator.next();
         iterator.remove();
         remove(request);
      }
   }


   private final String _tag;
   private final List<RequestHolder> _requests;

   private final int _parallelRequestsCount;
   private final Map<RequestHolder, Operation> _currentRequests;
}
