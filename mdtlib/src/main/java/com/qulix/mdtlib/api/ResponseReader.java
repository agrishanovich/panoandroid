package com.qulix.mdtlib.api;

import android.util.Log;
import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.concurrency.ThreadPool;
import com.qulix.mdtlib.concurrency.Worker;
import com.qulix.mdtlib.functional.Cancellable;

import java.io.IOException;
import java.io.InputStream;

public abstract class ResponseReader<Processor extends ResponseReader.ResultProcessor> implements Cancellable {
   private static final int THREAD_POOL_SIZE = 5;

   public interface ResultProcessor {
      void onError();
   }

   public ResponseReader(InputStream stream,
                         Processor processor) {
      this(null, stream, processor);
   }

   public ResponseReader(Worker worker,
                         InputStream stream,
                         Processor processor) {
      if (worker != null) {
         _worker = worker;
      }

      _stream = stream;
      _processor = processor;
   }

   public void startThread() {
      _worker.submit(new Runnable() {
         @Override public void run() {
            readFunction();
         }
      } );
   }

   protected abstract void resolveStream(InputStream streamToRead) throws IOException;

   /**
    * Thread affinity: any.
    */
   @Override public synchronized void cancel() {
      _cancelled = true;

      closeStream();
   }

   /**
    * Thread affinity: any.
    */
   protected void passError(final Exception e) {
      CTHandler.post(new Runnable() {
         @Override
         public void run() {
            if (_cancelled) return;

            Log.e("ApiResponseReader", "Error: " + e);
            _processor.onError();
         }
      });
   }

   private void readFunction() {
      synchronized(this) {
         if (_cancelled) return ;
      }

      try {

         InputStream streamToRead = null;
         synchronized(this) {
            streamToRead = _stream;
         }

         if (streamToRead != null) {
            resolveStream(streamToRead);
         } else {
            throw new Error("stream cant be null");
         }

      } catch(IOException e) {

         synchronized(this) {
            if (_cancelled) return ;
            passError(e);
         }

      } finally {
         closeStream();
      }
   }

   private synchronized void closeStream() {
      if (_stream != null) {
         try {
            _stream.close();
         } catch (IOException e) {
            throw new RuntimeException("Closing stream lead to exception!");
         }
         _stream = null;
      }
   }

   private static Worker _sharedWorker = ThreadPool.newWithBackgroundPriority(THREAD_POOL_SIZE, "Api response reading pool");
   private Worker _worker = _sharedWorker;
   private InputStream _stream;

   final protected Processor _processor;
   protected boolean _cancelled;
}