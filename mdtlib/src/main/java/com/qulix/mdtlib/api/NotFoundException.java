package com.qulix.mdtlib.api;

public class NotFoundException extends Exception {
   public NotFoundException() {
      super("Not Found: http status 404 received");
   }
}
