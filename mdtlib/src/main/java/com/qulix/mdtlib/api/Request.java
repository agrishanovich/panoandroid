package com.qulix.mdtlib.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.qulix.mdtlib.functional.Continuation;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.http.HttpRequestDescription;
import org.apache.http.Header;

public class Request<DataType> {
   public Request(final HttpRequestDescription requestDescription,
                  final TypeReference<DataType> dataType,
                  final Continuation<DataType> receiver,
                  final Maybe<Receiver<Header[]>> headersReceiver,
                  final boolean ignoreResponseData) {
      _requestDescription = requestDescription;
      _dataType = dataType;
      _receiver = receiver;
      _headersReceiver = headersReceiver;
      _ignoreResponseData = ignoreResponseData;
   }

   public Request(final HttpRequestDescription requestDescription,
                  final TypeReference<DataType> dataType,
                  final Continuation<DataType> receiver,
                  final Maybe<Receiver<Header[]>> headersReceiver) {
      this(requestDescription, dataType, receiver, headersReceiver, false);
   }

   public HttpRequestDescription requestDescription() {
      return _requestDescription;
   }

   public TypeReference<DataType> dataType() {
      return _dataType;
   }

   public Continuation<DataType> receiver() {
      return _receiver;
   }

   public Maybe<Receiver<Header[]>> headersReceiver() {
      return _headersReceiver;
   }

   public boolean ignoreResponseData() {
      return _ignoreResponseData;
   }

   private final HttpRequestDescription _requestDescription;
   private final TypeReference<DataType> _dataType;
   private final Continuation<DataType> _receiver;
   private final Maybe<Receiver<Header[]>> _headersReceiver;
   private final boolean _ignoreResponseData;
}
