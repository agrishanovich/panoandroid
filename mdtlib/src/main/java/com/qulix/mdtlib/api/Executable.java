package com.qulix.mdtlib.api;

import com.qulix.mdtlib.operation.Operation;

public interface Executable {
   Operation execute();
}
