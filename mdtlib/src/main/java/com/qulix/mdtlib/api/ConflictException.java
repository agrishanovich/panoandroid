package com.qulix.mdtlib.api;

public class ConflictException extends Exception {

   @Override
   public String getMessage() {
      return "Conflict exception: received 409 status code";
   }
}
