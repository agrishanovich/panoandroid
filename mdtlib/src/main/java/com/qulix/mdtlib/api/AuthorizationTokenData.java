package com.qulix.mdtlib.api;

import com.qulix.mdtlib.pair.Pair;

public class AuthorizationTokenData implements AuthorizationData {

   public AuthorizationTokenData(String token) {
      _token = token;
   }

   @Override
   public Pair<String, String> authData() {
      return new Pair<String, String>("Authorization", "Token token=\"" + _token + "\"");
   }

   private String _token;
}
