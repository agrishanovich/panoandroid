package com.qulix.mdtlib.functional.reduce;

import com.qulix.mdtlib.functional.Maybe;

import java.util.Collection;

public final class ReduceList {

   public interface Reducer<T, R> {
      R apply(T t, Maybe<R> r);
   }

   public static <T, R> Maybe<R> reduce(final Collection<? extends T> collection,
                                        final Reducer<T, R> reducer,
                                        final Maybe<R> initialValue) {
      Maybe<R> result = initialValue;

      for (T t : collection) {
         result = Maybe.value(reducer.apply(t, result));
      }

      return result;
   }
}
