package com.qulix.mdtlib.functional;

public class DoNothing implements Runnable, Cancellable {
   @Override public void run() {}
   @Override public void cancel() {}
}