package com.qulix.mdtlib.functional;

public class ContinuationWrapper<T> implements Continuation<T> {

   public ContinuationWrapper(Continuation<T> slave) {
      _slave = slave;
   }

   @Override
   public void catchException(Throwable exception) {
      _slave.catchException(exception);
   }

   @Override
   public void receive(T t) {
      _slave.receive(t);
   }

   private Continuation<T> _slave;
}
