package com.qulix.mdtlib.functional.comparing;

public interface Comparator<T> {
   boolean isEquals(T l, T r);
}