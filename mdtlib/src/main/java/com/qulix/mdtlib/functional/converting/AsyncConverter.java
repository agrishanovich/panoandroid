package com.qulix.mdtlib.functional.converting;

import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.operation.Operation;

public interface AsyncConverter<Output, Input> {
   Operation convert(final Input input,
                     final Receiver<Output> resultReceiver);
}
