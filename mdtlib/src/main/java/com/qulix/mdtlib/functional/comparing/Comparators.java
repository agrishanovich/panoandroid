package com.qulix.mdtlib.functional.comparing;

public final class Comparators {

   private Comparators() {}

   public static <T> Comparator<T> byJavaEqualsMethod() {
      return withBasicChecks(new Comparator<T>() {
            @Override public boolean isEquals(final T l, final T r) {
               return l.equals(r);
            }
         });
   }

   public static <T> Comparator<T> withBasicChecks(final Comparator<T> slave) {
      return new Comparator<T>() {
         @Override public boolean isEquals(final T l, final T r) {
            if (l == r) return true;
            if (l == null) return false;
            if (r == null) return false;

            return slave.isEquals(l, r);
         }
      };
   }

   public static <T> Comparator<T> byJavaComparator(final java.util.Comparator<T> comparator) {
      return new Comparator<T>() {
         @Override public boolean isEquals(final T l,
                                           final T r) {
            return comparator.compare(l, r) == 0;
         }
      };
   }
}