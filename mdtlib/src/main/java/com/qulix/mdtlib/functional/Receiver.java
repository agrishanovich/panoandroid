package com.qulix.mdtlib.functional;

/**
 * @class Receiver
 *
 * @brief Receiver is universal method to pass data.
 *
 * This interface can be used to setup typesafe separation from emitter and
 * receiver of data.
 */
public interface Receiver<T> {

   void receive(T t);
}

