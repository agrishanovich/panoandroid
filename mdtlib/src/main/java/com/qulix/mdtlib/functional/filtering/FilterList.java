package com.qulix.mdtlib.functional.filtering;

import com.qulix.mdtlib.functional.comparing.Comparator;
import com.qulix.mdtlib.functional.converting.Converter;
import com.qulix.mdtlib.functional.searching.Find;
import com.qulix.mdtlib.utils.Validate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class FilterList {

   public interface Predicate<T> {
      boolean shouldBeKept(final T t);
   }

   public static <T> Predicate<T> reversedPredicate(final Predicate<T> predicate) {
      return new Predicate<T>() {
         @Override
         public boolean shouldBeKept(T t) {
            return !predicate.shouldBeKept(t);
         }
      };
   }

   public static <T> List<T> filter(final Predicate<T> predicate,
                                    final List<T> originalList) {
      Validate.argNotNull(predicate, "predicate");
      Validate.argNotNull(originalList, "originalList");

      final List<T> result = new ArrayList<T>();

      for (final T t: originalList) {
         if (predicate.shouldBeKept(t)) {
            result.add(t);
         }
      }

      return result;
   }

   public static <T> List<T> filter(final Converter<Boolean, T> predicate,
                                    final Collection<T> source) {
      Validate.argNotNull(predicate, "predicate");
      Validate.argNotNull(source, "source");

      final List<T> result = new ArrayList<T>();

      for (final T t: source) {
         if (predicate.convert(t)) {
            result.add(t);
         }
      }

      return result;
   }

   public static <T> List<T> uniqueBy(final Collection<T> source, final Comparator<T> comparator) {
      Validate.argNotNull(comparator, "comparator");
      Validate.argNotNull(source, "source");

      final List<T> result = new ArrayList<T>();

      for (final T t: source) {
         final boolean alreadyAdded = Find.indexBy(comparator, t, result).isValue();

         if (!alreadyAdded) {
            result.add(t);
         }
      }

      return result;
   }

}