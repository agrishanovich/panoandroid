package com.qulix.mdtlib.functional.converting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.operation.CompositeOperation;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.utils.Validate;

public class ConvertList {
   public static <To, From> List<To> convert(final List<From> from,
                                             final Converter<To, From> converter) {

      final List<To> result = new ArrayList<To>(from.size());

      for (final From object : from) {
         result.add(converter.convert(object));
      }

      return result;
   }

   public static <To, From> List<To> convertJoin(final List<From> from,
                                                 final Converter<List<To>, From> converter) {
      final List<To> result = new ArrayList<To>(from.size());

      for (final List<To> resList : convert(from, converter)) {
         result.addAll(resList);
      }

      return result;
   }

   public static <T> List<T> join(final List<T> one, final List<T> two) {
      final List<T> result = new ArrayList<T>(one);

      result.addAll(two);

      return result;
   }

   public static <To, From> Operation convert(final Collection<From> from,
                                              final AsyncConverter<To, From> converter,
                                              final Receiver<List<To>> onResult) {

      return new AsyncConverteringOperation<To, From>(from,
                                                      converter,
                                                      onResult);
   }

   private static final class AsyncConverteringOperation<Output, Input> extends CompositeOperation {

      public AsyncConverteringOperation(final Collection<Input> input,
                                        final AsyncConverter<Output, Input> converter,
                                        final Receiver<List<Output>> outputListReceiver) {
         Validate.argNotNull(input, "input");
         Validate.argNotNull(converter, "converter");
         Validate.argNotNull(outputListReceiver, "outputListReceiver");

         _input = new LinkedList<Input>(input);

         Validate.assertIsTrue(_input.size() > 0, "input list can't be null");

         _converter = converter;
         _outputListReceiver = outputListReceiver;

         _outputList = new ArrayList<Output>(_input.size());

         nextStep();
      }

      private void nextStep() {
         if (_input.size() == 0) {
            _outputListReceiver.receive(_outputList);
         } else {
            final Input nextToConvert = _input.remove(0);

            setSlave(_converter.convert(nextToConvert,
                                        new Receiver<Output>() {
                                           @Override
                                           public void receive(final Output output) {
                                              _outputList.add(output);
                                              nextStep();
                                           }
                                        }));
         }
      }


      private final List<Input> _input;
      private final List<Output> _outputList;

      private final AsyncConverter<Output, Input> _converter;
      private final Receiver<List<Output>> _outputListReceiver;
   }
}
