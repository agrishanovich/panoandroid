package com.qulix.mdtlib.functional.converting;

public interface Converter<To, From> {
   To convert(From from);
}
