package com.qulix.mdtlib.functional;

/**
 * @class Factory
 *
 * @brief This is generic factory interface.
 */
public interface Factory <T> {

	/**
	 * Ask factory to produce it's value
	 *
	 * @return value
	 */
	public T create();
}