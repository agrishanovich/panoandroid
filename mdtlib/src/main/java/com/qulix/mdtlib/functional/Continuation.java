package com.qulix.mdtlib.functional;

public interface Continuation<T> extends Receiver<T> {
   public void catchException(Throwable exception);

   public static class DoNothing<T> implements Continuation<T> {
      @Override
      public void catchException(Throwable exception) {

      }

      @Override
      public void receive(T t) {

      }
   }
}
