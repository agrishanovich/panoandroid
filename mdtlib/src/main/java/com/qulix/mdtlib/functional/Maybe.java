package com.qulix.mdtlib.functional;

import com.qulix.mdtlib.functional.comparing.Comparator;
import com.qulix.mdtlib.functional.comparing.Comparators;
import com.qulix.mdtlib.functional.converting.Converter;
import com.qulix.mdtlib.utils.Validate;

import java.io.Serializable;

public class Maybe <T> implements Serializable {
   public static <T> Maybe<T> value(T value) {
      return new Maybe<T>(value);
   }

   public static <T> Maybe<T> nothing() {
      return new Maybe<T>();
   }

   public static <T> Maybe<T> join(final Maybe<Maybe<T>> t) {
      if (t.isValue()) return t.value();
      else return nothing();
   }

   public Maybe(T value) {
      Validate.argNotNull(value, "value");
      _value = value;
      _haveValue = true;
   }

   public Maybe() {
      _value = null;
      _haveValue = false;
   }

   public T value() {
      if ( ! isValue()) throw new Error("Accessing value from maybe without value");

      return _value;
   }


   public boolean isValue() {
      return _haveValue;
   }

   public T or(final T defaultValue) {
      if (isValue()) return value();
      else return defaultValue;
   }

   public Maybe<T> orMaybe(final Maybe<T> defaultValue) {
      if (isValue()) return this;
      else return defaultValue;
   }

   public Maybe<T> orValue(final T defaultValue) {
      if (isValue()) return this;
      else return Maybe.value(defaultValue);
   }

   public <R> R convertOr(final R defaultValue,
                          final Converter<R, T> converter) {
      return convert(converter).or(defaultValue);
   }

   public <R> Maybe<R> convert(final Converter<R, T> converter) {
      if (isValue()) return Maybe.value(converter.convert(value()));
      else return nothing();
   }

   public boolean isEqualsBy(final Comparator<T> comparator,
                             final Maybe<T> other)  {
      if (isValue() != other.isValue()) return false;
      if (!isValue()) return true;

      return comparator.isEquals(value(),
                                 other.value());
   }

   public boolean isEquals(final Maybe<T> other) {
      return isEqualsBy(Comparators.<T>byJavaEqualsMethod(),
                        other);
   }

   public void valueTo(final Receiver<T> receiver) {
      if (isValue()) receiver.receive(value());
   }

   public static <T> Maybe<T> nullIsNothing(final T value) {
      if (value == null) return nothing();
      else return value(value);
   }


   private T _value;
   private boolean _haveValue;
}