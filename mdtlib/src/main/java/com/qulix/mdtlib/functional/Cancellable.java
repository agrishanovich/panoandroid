package com.qulix.mdtlib.functional;

public interface Cancellable {
   void cancel();
}