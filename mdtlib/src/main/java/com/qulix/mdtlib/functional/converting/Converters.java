package com.qulix.mdtlib.functional.converting;

import com.qulix.mdtlib.functional.Maybe;

public final class Converters {
   private Converters() {}

   public static Converter<Maybe<Integer>, String> INT_FROM_STRING = new Converter<Maybe<Integer>, String>() {
         @Override public Maybe<Integer> convert(final String string) {
            if (string.isEmpty()) return Maybe.<Integer>nothing();

            try {
               return Maybe.value(Integer.valueOf(string));
            } catch (final NumberFormatException nfe) {
               return Maybe.<Integer>nothing();
            }
         }
      };
}