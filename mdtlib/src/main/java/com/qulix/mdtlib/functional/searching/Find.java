package com.qulix.mdtlib.functional.searching;

import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.functional.comparing.Comparator;
import com.qulix.mdtlib.functional.converting.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class Find {

   public static <T> Maybe<Integer> indexBy(final Comparator<T> comparator,
                                            final T value,
                                            final Collection<T> collection) {
      int index = 0;

      for (final T t: collection) {
         if (comparator.isEquals(t, value)) {
            return Maybe.value(index);
         }

         ++index;
      }

      return Maybe.nothing();
   }

   public static <T> Maybe<Integer> indexIf(final Converter<Boolean, T> predicate,
                                            final Collection<T> collection) {
      int index = 0;

      for (final T t: collection) {
         if (predicate.convert(t)) {
            return Maybe.value(index);
         }

         ++index;
      }

      return Maybe.nothing();
   }

   public static <T> Maybe<T> similarElementBy(final Comparator<T> comparator,
                                            final T value,
                                            final Collection<T> collection) {
      for (final T t: collection) {
         if (comparator.isEquals(t, value)) {
            return Maybe.value(t);
         }
      }

      return Maybe.nothing();
   }

   public static <T> Maybe<T> elementIf(final Converter<Boolean, T> predicate,
                                        final Collection<T> collection) {
      for (final T t: collection) {
         if (predicate.convert(t)) return Maybe.value(t);
      }

      return Maybe.<T>nothing();
   }

   public static <T> List<T> missingInSecondCollection(final Comparator<T> comparator,
                                                       final Collection<T> elementsToSearch,
                                                       final Collection<T> collectionToSearchIn) {

      final List<T> result = new ArrayList<T>();

      for (final T elt: elementsToSearch) {
         final boolean notPresentInSecond = ! (indexBy(comparator,
                                                       elt,
                                                       collectionToSearchIn)
                                               .isValue());
         if (notPresentInSecond) {
            result.add(elt);
         }
      }

      return result;
   }

   public static <T> List<T> intersection(final Comparator<T> comparator,
                                          final Collection<T> left,
                                          final Collection<T> right) {
      final List<T> intersection = new ArrayList<T>();

      for (final T candidate : left) {

         final Maybe<Integer> indexInRight = indexBy(comparator,
                                                     candidate,
                                                     right);
         if (indexInRight.isValue()) {
            // in both lists, part of intersection

            intersection.add(candidate);
         }
      }

      return intersection;
   }

   public static <T> List<T> replace(final Comparator<T> comparator,
                                     final Collection<T> collectionToBeChanged,
                                     final Collection<T> collectionOfReplacements) {
      final List<T> result = new ArrayList<T>();

      final List<T> replacements = new ArrayList<T>(collectionOfReplacements);

      for (final T original : collectionToBeChanged) {

         final Maybe<T> foundReplacement = similarElementBy(comparator, original, replacements);

         // in both lists, replace by item found
         result.add(foundReplacement.or(original));
      }

      return result;
   }

   public static <T> List<T> difference(final Comparator<T> comparator,
                                        final Collection<T> left,
                                        final Collection<T> rawRight) {
      final List<T> diff = new ArrayList<T>();

      final List<T> right = new ArrayList<T>(rawRight);

      for (final T candidate : left) {

         Converter<Boolean, T> predicate = new Converter<Boolean, T>() {
            @Override
            public Boolean convert(T t) {
               return comparator.isEquals(t, candidate);
            }
         };

         final Maybe<T> elementInRight = elementIf(predicate, right);

         if (elementInRight.isValue()) {
            // in both lists, remove from rights
            right.remove(elementInRight.value());
         } else {
            // only in left list, put to diff
            diff.add(candidate);
         }
      }

      // elements were not removed from right list is the elements not
      // found in left list, so add to diff too

      diff.addAll(right);

      return diff;
   }

   public static <T> boolean ifSame(final Comparator<T> comparator,
                                    final Collection<T> left,
                                    final Collection<T> right) {
      return difference(comparator, left, right).size() == 0;
   }
}
