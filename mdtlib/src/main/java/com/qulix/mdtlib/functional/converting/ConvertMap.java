package com.qulix.mdtlib.functional.converting;

import com.qulix.mdtlib.functional.Maybe;

import java.util.Map;

public class ConvertMap {

   public static <K, V> Converter<Maybe<V>, Maybe<K>> keyToValue(final Map<K, V> map) {
      return new Converter<Maybe<V>, Maybe<K>>() {
         @Override
         public Maybe<V> convert(Maybe<K> key) {
            if (key.isValue() && map.containsKey(key.value())) {
               return Maybe.value(map.get(key.value()));
            } else {
               return Maybe.nothing();
            }
         }
      };
   }
}
