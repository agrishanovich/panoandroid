package com.qulix.mdtlib.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface UnixTimestamp {
    Prescision prescision() default Prescision.SECONDS;

    public enum Prescision {
        SECONDS,
        MILLISECONDS
    }

    public static class Serializer extends JsonSerializer<Date> implements ContextualSerializer {
        private static class Impl extends JsonSerializer<Date> {
            private final BeanProperty _property;

            public Impl(BeanProperty property) {
                _property = property;
            }

            @Override
            public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
                long timestamp;
                UnixTimestamp info = _property.getAnnotation(UnixTimestamp.class);
                Prescision prescision = (null != info ? info.prescision() : Prescision.SECONDS);
                switch (prescision) {
                    case MILLISECONDS:
                       timestamp = date.getTime();
                       break;
                    default:
                        timestamp = date.getTime() / 1000;
                }

                JsonFormat format = _property.getAnnotation(JsonFormat.class);
                if (null != format && JsonFormat.Shape.STRING == format.shape()) {
                    jsonGenerator.writeString(Long.toString(timestamp));

                } else {
                    jsonGenerator.writeNumber(timestamp);
                }
            }
        }

        @Override
        public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty property) throws JsonMappingException {
            return new Impl(property);
        }

        @Override
        public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
            throw new UnsupportedOperationException();
        }
    }

    public static class Deserializer extends JsonDeserializer<Date> implements ContextualDeserializer {
        private static class Impl extends JsonDeserializer<Date> {
            private final BeanProperty _property;

            public Impl(BeanProperty property) {
                _property = property;
            }

            @Override
            public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                long timestamp;
                JsonFormat format = _property.getAnnotation(JsonFormat.class);
                if (null != format && JsonFormat.Shape.STRING == format.shape()) {
                    timestamp = Long.parseLong(jsonParser.getText());

                } else {
                    timestamp = jsonParser.getLongValue();
                }

                Date date;
                UnixTimestamp info = _property.getAnnotation(UnixTimestamp.class);
                Prescision prescision = (null != info ? info.prescision() : Prescision.SECONDS);
                switch (prescision) {
                    case MILLISECONDS:
                        date = new Date(timestamp);
                       break;
                    default:
                        date = new Date(timestamp * 1000);
                }
                return date;
            }
        }

        @Override
        public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty property) throws JsonMappingException {
            return new Impl(property);
        }

        @Override
        public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            throw new UnsupportedOperationException();
        }
    }
}
