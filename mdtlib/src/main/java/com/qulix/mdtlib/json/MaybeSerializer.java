package com.qulix.mdtlib.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.qulix.mdtlib.functional.Maybe;

import java.io.IOException;

public class MaybeSerializer extends JsonSerializer<Maybe> {
   @Override
   public void serialize(Maybe maybe, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
      if (maybe.isValue()) {
         serializerProvider.defaultSerializeValue(maybe.value(), jsonGenerator);
      } else {
         serializerProvider.defaultSerializeNull(jsonGenerator);
      }
   }
}
