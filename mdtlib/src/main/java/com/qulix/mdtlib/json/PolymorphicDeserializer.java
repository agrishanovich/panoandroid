package com.qulix.mdtlib.json;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;

public class PolymorphicDeserializer extends JsonDeserializer<Object> implements ContextualDeserializer {
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD})
    public @interface Rule {
        String valueOf();
    }

    public static class PolymorphicDeserializerImpl<T> extends JsonDeserializer<T> {
        private final Rule _rule;
        private final Map<String, Class<? extends T>> _subtypes;

        public PolymorphicDeserializerImpl(Rule rule, Map<String, Class<? extends T>> subtypes) {
            _rule = rule;
            _subtypes = subtypes;
        }

        @Override
        public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            ObjectMapper mapper = (ObjectMapper)jsonParser.getCodec();
            JsonNode node = mapper.readTree(jsonParser);
            String typeID = nodeAtPath(node, _rule.valueOf()).asText();
            Class<? extends T> dataType = _subtypes.get(typeID);
            ObjectReader reader = mapper.reader(dataType);
            T object = reader.readValue(node);
            return object;
        }

        private JsonNode nodeAtPath(JsonNode root, String path) {
            String[] components = path.split("/");
            JsonNode currentNode = root;
            for (String field : components) {
                currentNode = (field.equals(".") ? currentNode : currentNode.get(field));
            }
            return currentNode;
        }
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty beanProperty) throws JsonMappingException {
        Rule rule = beanProperty.getAnnotation(Rule.class);

        Map<String, Class<? extends Object>> subtypes = new HashMap<String, Class<? extends Object>>();
        JsonSubTypes subTypesInfo = beanProperty.getAnnotation(JsonSubTypes.class);
        for (JsonSubTypes.Type type : subTypesInfo.value()) {
            subtypes.put(type.name(), (Class<? extends Object>)type.value());
        }

        return new PolymorphicDeserializerImpl<Object>(rule, subtypes);
    }

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return null;
    }
}
