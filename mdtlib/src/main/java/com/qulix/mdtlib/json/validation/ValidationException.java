package com.qulix.mdtlib.json.validation;

@SuppressWarnings("serial")
public class ValidationException extends Exception {
   public ValidationException() {
      super();
   }

   public ValidationException(String detailMessage) {
      super(detailMessage);
   }

   public ValidationException(String detailMessage, Throwable throwable) {
      super(detailMessage, throwable);
   }

   public ValidationException(Throwable throwable) {
      super(throwable);
   }
}
