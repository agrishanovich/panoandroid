package com.qulix.mdtlib.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.qulix.mdtlib.functional.Maybe;

import java.io.IOException;
import java.lang.reflect.Method;

public class JsonParser {
   private ObjectMapper _jackson;

   @JsonFilter("filter-implicit-members")
   private class FilterImplicitMembersMixIn {}

   public JsonParser() {
      _jackson = new ObjectMapper();
      _jackson.setVisibilityChecker(
              _jackson.getVisibilityChecker()
                      .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                      .withGetterVisibility(JsonAutoDetect.Visibility.ANY)
                      .withSetterVisibility(JsonAutoDetect.Visibility.ANY)
                      .withCreatorVisibility(JsonAutoDetect.Visibility.ANY));
      _jackson.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      _jackson.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
      _jackson.configure(MapperFeature.REQUIRE_SETTERS_FOR_GETTERS, true);
      _jackson.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
      _jackson.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_COMMENTS, true);
      _jackson.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      _jackson.registerModule(new SimpleModule()
                                      .addSerializer(Maybe.class, new MaybeSerializer())
                                      .addDeserializer(Maybe.class, new MaybeDeserializer()));

      PropertyFilter filter = new SimpleBeanPropertyFilter() {
         @Override protected boolean include(BeanPropertyWriter beanPropertyWriter) {
            boolean isMethod = (beanPropertyWriter.getMember().getMember() instanceof Method);
            boolean specifiedPropertyName = (null != beanPropertyWriter.getAnnotation(JsonProperty.class));
            return !isMethod || specifiedPropertyName;
         }

         @Override protected boolean include(PropertyWriter propertyWriter) {
            return !(propertyWriter instanceof BeanPropertyWriter) || include((BeanPropertyWriter)propertyWriter);
         }

         @Override public void serializeAsField(Object obj, JsonGenerator jgen, SerializerProvider provider, PropertyWriter writer) throws Exception {
            if (writer instanceof BeanPropertyWriter) {
               BeanPropertyWriter propertyWriter = (BeanPropertyWriter)writer;
               boolean isMaybe = Maybe.class.isAssignableFrom(propertyWriter.getPropertyType());
               if (isMaybe) {
                  Maybe maybe = (Maybe)propertyWriter.get(obj);
                  if (maybe == null || !maybe.isValue()) {
                     // Ignore this field
                     return;
                  }
               }
            }
            super.serializeAsField(obj, jgen, provider, writer);
         }
      };
      FilterProvider filters = new SimpleFilterProvider().addFilter("filter-implicit-members", filter);
      _jackson.setFilters(filters);
      _jackson.addMixInAnnotations(Object.class, FilterImplicitMembersMixIn.class);
   }

   public <T> T fromJson(String json, TypeReference<T> type) throws IOException {
      T obj = _jackson.<T>readValue(json, type);
      return obj;
   }

   public <T> T fromJson(String json, Class<T> type) throws IOException {
      T obj = _jackson.readValue(json, type);
      return obj;
   }

   public JsonNode treeFromJson(String json) throws IOException {
      JsonNode node = _jackson.readTree(json);
      return node;
   }

   public String treeToJson(JsonNode node) throws IOException {
      return _jackson.writerWithDefaultPrettyPrinter().writeValueAsString(node);
   }

   public String toJson(Object src) throws IOException {
      return _jackson.writeValueAsString(src);
   }

   public ObjectNode createNode() {
      return _jackson.createObjectNode();
   }

   public ArrayNode createArray() {
      return _jackson.createArrayNode();
   }
}
