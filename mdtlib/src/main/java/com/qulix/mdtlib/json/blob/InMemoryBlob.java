/**
 * Created by makarovvm on 08.05.13
 */

package com.qulix.mdtlib.json.blob;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class InMemoryBlob implements Blob {
    private byte[] _data;

    public static class Builder implements Blob.Builder {
        private InMemoryBlob _blob;
        private ByteArrayOutputStream _stream;

        public OutputStream stream() {
            if (null == _stream) {
                _stream = new ByteArrayOutputStream();
                _blob = new InMemoryBlob();
            }
            return _stream;
        }

        public Blob build() {
            _blob._data = _stream.toByteArray();
            _stream = null;
            return _blob;
        }
    }

    public InputStream stream() {
        return new ByteArrayInputStream(_data);
    }

    public void recycle() {
        _data = null;
    }
}
