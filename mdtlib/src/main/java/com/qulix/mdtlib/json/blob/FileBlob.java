/**
 * Created by makarovvm on 08.05.13
 */

package com.qulix.mdtlib.json.blob;

import java.io.*;

public class FileBlob implements Blob {
    private String _path;
    private boolean _isTemporary;

    public static class Builder implements Blob.Builder {
        private FileBlob _blob;

        public Builder(String path) {
            _blob = new FileBlob(path, false);
        }

        public Builder setIsTemporary(boolean isTemporary) {
            _blob._isTemporary = isTemporary;
            return this;
        }

        public OutputStream stream() {
            try {
                File file = new File(_blob.path());
                if (!file.exists()) {
                    file.getParentFile().mkdir();
                    file.createNewFile();
                }
                return new FileOutputStream(_blob.path());

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public Blob build() {
            return _blob;
        }
    }

    public FileBlob(String path, boolean isTemporary) {
        _path = path;
        _isTemporary = isTemporary;
    }

    public InputStream stream() {
        try {
            return new FileInputStream(_path);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void recycle() {
        if (_isTemporary) {
            new File(_path).delete();
        }
    }

    public String path() {
        return _path;
    }
}
