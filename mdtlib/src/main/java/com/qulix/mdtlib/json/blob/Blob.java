/**
 * Created by makarovvm on 07.05.13
 */

package com.qulix.mdtlib.json.blob;

import java.io.InputStream;
import java.io.OutputStream;


public interface Blob {

    public interface Builder {
        OutputStream stream();
        Blob build();
    }

    InputStream stream();
    void recycle();
}
