/**
 * Created by makarovvm on 08.05.13
 */

package com.qulix.mdtlib.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.qulix.mdtlib.json.blob.Blob;

import java.io.IOException;
import java.io.OutputStream;

public abstract class BlobDeserializer extends JsonDeserializer<Blob> {

    abstract public Blob.Builder blobBuilder();

    @Override
    public Blob deserialize(com.fasterxml.jackson.core.JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Blob.Builder builder = blobBuilder();
        OutputStream stream = builder.stream();
        jsonParser.readBinaryValue(stream);
        stream.close();
        return builder.build();
    }
}
