package com.qulix.mdtlib.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.qulix.mdtlib.functional.Maybe;

import java.io.IOException;

public class MaybeDeserializer extends JsonDeserializer<Maybe> implements ContextualDeserializer {

   public MaybeDeserializer() {
      this(SimpleType.construct(Maybe.class));
   }

   private MaybeDeserializer(JavaType type) {
      _type = type;
   }

   @Override
   public JsonDeserializer<?> createContextual(DeserializationContext ctx, BeanProperty property) throws JsonMappingException {
      return new MaybeDeserializer(property.getType().containedType(0));
   }

   @Override
   public Maybe getNullValue() {
      return Maybe.nothing();
   }

   @Override
   public Maybe deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException, JsonProcessingException {
      return Maybe.nullIsNothing(ctx.readValue(jsonParser, _type));
   }

   private final JavaType _type;
}
