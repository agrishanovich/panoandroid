/**
 * Created by makarovvm on 15.05.13
 */

package com.qulix.mdtlib.json.validation;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;

public class Validator {
    public static <DataType> void validate(DataType data) throws ValidationException {
        if (data == null) {
            return;
        }

        for (Field field : data.getClass().getDeclaredFields()) {

            boolean wasAccessible = field.isAccessible();
            field.setAccessible(true);

            try {
                Object value = field.get(data);

                // Throw ValidationException when any of the
                // required field in the passed object is null.
                if (value == null && field.getAnnotation(Optional.class) == null) {
                    throw new ValidationException("Required field " + field.getName() + " is null");
                }

                // Validate sibling objects in the collections.
                if (value != null && value instanceof Collection<?>) {
                    boolean forbidsNulls = (field.getAnnotation(ContainsNulls.class) == null);
                    Iterator<?> it = ((Collection<?>)value).iterator();
                    while (it.hasNext()) {
                        Object sibling = it.next();
                        if (sibling != null) {
                            validate(sibling);

                        } else if (forbidsNulls) {
                            throw new ValidationException("Container " + field.getName() + " shall not contain null values");
                        }
                    }
                }

            } catch (IllegalAccessException e) {
                throw new ValidationException("error accessing required field " + field.getName());

            } finally {
                field.setAccessible(wasAccessible);
            }
        }
    }
}
