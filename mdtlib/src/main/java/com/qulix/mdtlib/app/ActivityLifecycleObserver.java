package com.qulix.mdtlib.app;

import android.app.Activity;

public interface ActivityLifecycleObserver {
   void onStartActivity(Activity activity);
   void onResumeActivity(Activity activity);
   void onPauseActivity(Activity activity);
   void onStopActivity(Activity activity);
}
