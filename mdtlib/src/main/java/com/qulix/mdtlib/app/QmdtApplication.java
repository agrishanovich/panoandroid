package com.qulix.mdtlib.app;

import android.app.Application;

import com.qulix.mdtlib.app.error.FatalErrorHandler;
import com.qulix.mdtlib.concurrency.CTHandler;

public class QmdtApplication extends Application {
   public QmdtApplication() {
      super();

      _instance = this;

      CTHandler.init();
   }

   public static QmdtApplication instance() {
      if (_instance == null) {
         // to make some of library code work, Context instance is
         // neede. At most cases, Application context is enough, so, to
         // avoid passing context at all points, library code uses
         // QmdtApplication.instance() method to access application instance.
         //
         // Application instance will not leak, as this only one instance.
         //
         // to make this work correctly, you need to make sure that
         // application's Application class extends QmdtApplication.
         throw new RuntimeException("Application class is not inherited from QmdtApplication, see comments.");
      }
      return _instance;
   }

   public FatalErrorHandler defaultFatalErrorHandler() {
      return mDefaultFatalErrorHandler;
   }


   private FatalErrorHandler mDefaultFatalErrorHandler = new FatalErrorHandler() {
      @Override
      public void receive(final Throwable throwable) {
         throw new RuntimeException("Crash in application",
                                    throwable);
      }
   };

   private static QmdtApplication _instance;
}