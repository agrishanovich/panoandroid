package com.qulix.mdtlib.app.error;

import com.qulix.mdtlib.functional.Receiver;

public interface FatalErrorHandler extends Receiver<Throwable> {
   /* please note - receive method of FatalErrorHandler must not throw an
      exception itself */
}
