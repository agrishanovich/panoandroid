package com.qulix.mdtlib.app;

import android.content.Intent;

import java.io.Serializable;

public class ActivityResult implements Serializable {

   public ActivityResult(int requestCode, int resultCode, Intent data) {
      _requestCode = requestCode;
      _resultCode = resultCode;
      _data = data;
   }

   public int requestCode() {
      return _requestCode;
   }

   public int resultCode() {
      return _resultCode;
   }

   public Intent data() {
      return _data;
   }

   private int _requestCode;
   private int _resultCode;
   private Intent _data;
}
