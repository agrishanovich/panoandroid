package com.qulix.mdtlib.app;

import android.app.Activity;
import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.subscription.ReceiverSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;

public class ActivityDrivenApplicationStateProvider implements ApplicationStateProvider, ActivityLifecycleObserver {
   private ApplicationState _state = ApplicationState.InBackground;
   private ReceiverSubscription<ApplicationState> _onStateChanged = new ReceiverSubscription<ApplicationState>();

   private ApplicationState _pendingState = ApplicationState.InBackground;
   private int _resumedCount;

   @Override public ApplicationState state() {
      return _state;
   }

   @Override public Subscription<Receiver<ApplicationState>> onStateChanged() {
      return _onStateChanged;
   }

   private void setState(ApplicationState state) {
      if (state != _state) {
         _state = state;
         _onStateChanged.receive(state);
      }
   }

   private void looksLike(ApplicationState state) {
      _pendingState = state;
      CTHandler.post(new Runnable() {
         @Override public void run() {
            setState(_pendingState);
         }
      });
   }

   @Override public void onStartActivity(Activity activity) {
      // todo: track 'started' states to differentiate between Active & Passive states
   }

   @Override public void onResumeActivity(Activity activity) {
      _resumedCount++;
      if (1 == _resumedCount) {
         looksLike(ApplicationState.Active);
      }
   }

   @Override public void onPauseActivity(Activity activity) {
      _resumedCount--;
      if (0 == _resumedCount) {
         looksLike(ApplicationState.InBackground);
      }
   }

   @Override public void onStopActivity(Activity activity) {
      // todo: track 'stopped' states to differentiate between Active & Passive states
   }
}
