package com.qulix.mdtlib.app;

import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.subscription.interfaces.Subscription;

public interface ApplicationStateProvider {
   public enum ApplicationState {
      InBackground,
      Passive,
      Active
   }

   ApplicationState state();
   Subscription<Receiver<ApplicationState>> onStateChanged();
}
