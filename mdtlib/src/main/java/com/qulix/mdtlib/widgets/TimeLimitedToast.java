package com.qulix.mdtlib.widgets;

import com.qulix.mdtlib.concurrency.CTHandler;
import android.content.Context;
import android.widget.Toast;


public class TimeLimitedToast {
   //limited duration in millisec
   private int closeAfter = 0;
   private Context context;
   private String text;
   private Runnable onClose;
   private Toast _toast;


   public TimeLimitedToast(Context context, int textId, int duration) {
      this(context, context.getString(textId), duration);
   }

   public TimeLimitedToast(Context context, String text, int duration) {
      this.context = context;
      this.text = text;
      this.closeAfter = duration;
      show();
   }

   public TimeLimitedToast(Context context, int textId, int duration, Runnable onClose) {
      this(context, context.getString(textId), duration);
      this.onClose = onClose;
   }

   private void show() {
      _toast = Toast.makeText(context, text, Toast.LENGTH_LONG);

      _toast.show();

      CTHandler.postDelayed(new Runnable(){
            @Override
            public void run() {
               _toast = null;

               if (_toast != null) {
                  _toast.cancel();
               }

               if (onClose != null) {
                  onClose.run();
               }
            }
         }, closeAfter);

   }

   public void close() {
      if (_toast == null) return ;
      _toast.cancel();
      _toast = null;
      onClose = null;
   }
}
