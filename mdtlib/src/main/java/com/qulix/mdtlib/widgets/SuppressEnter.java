package com.qulix.mdtlib.widgets;

import android.widget.EditText;
import android.view.KeyEvent;
import android.view.View;

public class SuppressEnter implements EditText.OnKeyListener {
   public boolean onKey(View view, int keyCode, KeyEvent event){
      if (event.getAction() == KeyEvent.ACTION_DOWN || event.getAction() == KeyEvent.ACTION_UP){
         switch (keyCode)
            {
               case KeyEvent.KEYCODE_DPAD_CENTER:
               case KeyEvent.KEYCODE_ENTER:
                  return true;
               default:
                  break;
            }
      }

      return false;
   }
}