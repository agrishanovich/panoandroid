package com.qulix.mdtlib.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;


/**
 * @class ResizeNotifyFrame
 *
 * @brief This is frame widget with possiblity to track size of area,
 * occupied with frame.
 *
 * <b>The Problem</b>: No listener can be set to track down size, which
 * occupied by some view or control, thus making it impossible to track
 * size changes of views, which is declared in layout xml files.
 *
 * <b>The solution</b>: This class will make it possible to register resize
 * handler while in other parts of it behaviour it will work just like
 * usual FrameLayout.
 */
public class ResizeNotifyFrame extends FrameLayout {

   /**
    * Constructor, passing call to parent call
    *
    * @param context context
    * @param attrs attributes
    * @param defStyle style
    */
   public ResizeNotifyFrame(Context context, AttributeSet attrs, int defStyle) {
      super(context, attrs, defStyle);

   }

   /**
    * Constructor, passing call to parent call
    *
    * @param context context
    * @param attrs attributes
    */
   public ResizeNotifyFrame(Context context, AttributeSet attrs) {
      super(context, attrs);

   }

   /**
    * Constructor, passing call to parent call
    *
    * @param context context
    */
   public ResizeNotifyFrame(Context context) {
      super(context);

   }

   /**
    * @class SizeObserver
    *
    * @brief Interface for resize notifications handler.
    *
    */
   public interface SizeObserver {
      /**
       * This method will be called every time, when this frame is
       * changed it's size when laying out.
       *
       * @param w new width of frame
       * @param h new height of frame
       */
      void onEveryResize(int w, int h);
   }

   private SizeObserver _observer;

   /**
    * Set size observer, replacing previous. Pass null to deregister observer.
    *
    * @param observer observer to set, or null to remove any.
    */
   public void setSizeObserver(SizeObserver observer){
      _observer = observer;
   }

   /**
    * Overriden function for onSizeChanged event. It will pass call
    * directly to parent and then notify observer.
    *
    * @param w new width of frame
    * @param h new height of frame
    * @param oldw old width of frame
    * @param oldh old height of frame
    */
   @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
      // pass to parent
      super.onSizeChanged(w, h, oldw, oldh);

      // pass to observer
      if (_observer != null) {
         _observer.onEveryResize(w, h);
      }
   }

}
