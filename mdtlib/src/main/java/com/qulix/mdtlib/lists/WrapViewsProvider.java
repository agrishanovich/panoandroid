package com.qulix.mdtlib.lists;

import java.io.Serializable;

public interface WrapViewsProvider extends Serializable {
   ListViewsProvider wrap(ListViewsProvider slave);
}
