package com.qulix.mdtlib.lists;

import com.qulix.mdtlib.utils.ViewClickHandler;
import com.qulix.mdtlib.functional.Factory;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.lists.dataset.DataSet;
import com.qulix.mdtlib.subscription.interfaces.Subscription;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * @class DataAdapter
 *
 * @brief Class serves as adapter between list of raw data (provided by
 * Set) and ListDataAdapter
 *
 */
@SuppressWarnings("JavaDoc")
public class DataAdapter<DataType, ItemType extends ListItem<DataType>> implements ListViewsProvider {

   public DataAdapter(DataSet<DataType> dataSet,
                      Factory <ItemType> listItemFactory,
                      Receiver<DataType> selectReceiver) {

      _dataSet = dataSet;

      _listItemFactory = listItemFactory;
      _selectReceiver = selectReceiver;
   }

   @Override public Subscription<Runnable> onUpdate() {
      // as this is stateless, we will return event of data set.
      return _dataSet.onUpdate();
   }

   @Override public int getViewTypeCount() {
      return 1;
   }

   @Override public int getItemViewType(int position) {
      return DEFAULT_TYPE;
   }

   @Override
   public Object getItem(int position) {
      return _dataSet.get(position);
   }

   @Override public int getCount() {
      return _dataSet.count();
   }

   @SuppressWarnings("unused")
   protected ItemType createItem(int position) {
      return _listItemFactory.create();
   }

   @SuppressWarnings("unchecked")
   @Override public View getView(int position, View convertView, ViewGroup parent) {
      ItemViewHolder<DataType, ItemType> holder;

      if (convertView != null && convertView instanceof ItemViewHolder<?, ?>) {

         holder = (ItemViewHolder<DataType, ItemType>) convertView;
      } else {
         Context context = parent.getContext();

         final ItemType item = createItem(position);
         item.setContext(context);

         if (_selectReceiver != null) {
            item.getView().setOnClickListener(new ViewClickHandler(null) {
                  @Override public void handleClick() {
                     _selectReceiver.receive(item.getData());
                  }
               } );
         }


         holder = new ItemViewHolder<DataType, ItemType>(context,
                                                         item);
      }

      // update it with appropriate data from set
      DataType data = _dataSet.get(position);
      holder.resetItemData(data);

      // return view
      return holder;
   }

   private static final int DEFAULT_TYPE = 0;

   private DataSet<DataType> _dataSet;
   private Factory <ItemType> _listItemFactory;
   private Receiver<DataType> _selectReceiver;
}
