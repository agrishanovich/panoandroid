package com.qulix.mdtlib.lists;
import android.view.ViewGroup;
import android.view.View;
import android.widget.BaseAdapter;
import com.qulix.mdtlib.functional.Factory;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.lists.dataset.DataSet;

class ListDataAdapter
   extends BaseAdapter {

   public ListDataAdapter() {

   }

   public ListDataAdapter(ListViewsProvider adapter) {
      setAdapter(adapter);
   }

   public void setAdapter(ListViewsProvider adapter) {
      _adapter = adapter;
      _adapter.onUpdate().subscribeWeak(_refreshRunnable);
   }

   public void refresh() {
      notifyDataSetChanged();
   }

   public int getCount() {
      return _adapter.getCount();
   }

   public Object getItem(int position) {
      return _adapter.getItem(position);
   }

   public long getItemId(int position) {
      return position;
   }

   public int getViewTypeCount(){
      return _adapter.getViewTypeCount();
   }

   public int getItemViewType(int position) {
      return _adapter.getItemViewType(position);
   }

   public View getView(int position, View convertView, ViewGroup parent) {
      return _adapter.getView(position, convertView, parent);
   }

   private ListViewsProvider _adapter;
   private Runnable _refreshRunnable = new Runnable() {
         @Override public void run() {
            refresh();
         }
      };
}

public final class ListSettings {

   private ListSettings() { /* not for instantiation */ }

   /**
    * Use this construction when you need simple case list selection - pass
    * receiver here and it will be called whenever item clicked.
    */
   public static <DataType, ItemType extends ListItem<DataType> >
                                             BaseAdapter create(DataSet<DataType> dataSet,
                                                                Factory <ItemType> listItemFactory,
                                                                Receiver<DataType> selectReceiver) {

      return new ListDataAdapter(new DataAdapter<DataType, ItemType>(dataSet,
                                                                     listItemFactory,
                                                                     selectReceiver));
   }

   /**
    * Use this construction when you need simple case list selection - pass
    * receiver here and it will be called whenever item clicked.
    */
   public static <DataType, ItemType extends ListItem<DataType> >
   BaseAdapter create(DataSet<DataType> dataSet,
                      Factory <ItemType> listItemFactory,
                      WrapViewsProvider wrapViewsProvider,
                      Receiver<DataType> selectReceiver) {

      return new ListDataAdapter(wrapViewsProvider.wrap(
              new DataAdapter<DataType, ItemType>(dataSet,
                      listItemFactory,
                      selectReceiver)));
   }

   public static BaseAdapter create(ListViewsProvider provider) {

      return new ListDataAdapter(provider);
   }

   /**
    * Use this construction when you need simple case list - which only
    * displays data, or when click handling is complex and handled inside the item.
    */
   public static <DataType, ItemType extends ListItem<DataType> >
                                             BaseAdapter create(DataSet<DataType> dataSet,
                                                                Factory <ItemType> listItemFactory) {
      return create(dataSet, listItemFactory, null);
   }

}