package com.qulix.mdtlib.lists.dataset;

import com.qulix.mdtlib.subscription.interfaces.Subscription;

/**
 * @class DataSet
 *
 * @brief Data set for lists.
 *
 * This class represents set of data to be used in lists. It have next properties:
 *
 * * It is linear, in means it can be accessed by index
 * * It is fast enought to be used in list view
 * * It finite
 * * Count of data known
 */
public interface DataSet<DataType> {
   DataType get(int index);

   int count();

   Subscription<Runnable> onUpdate();
}
