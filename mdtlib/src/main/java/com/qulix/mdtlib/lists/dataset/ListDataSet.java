package com.qulix.mdtlib.lists.dataset;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @class ListProvider
 *
 * @brief This is data set, which provides specified from list.
 *
 */
@SuppressWarnings("serial")
public class ListDataSet<T> extends BasicDataSet<T> implements Serializable {

   public ListDataSet(List<T> data) {
      setData(data);
   }

   public ListDataSet() {

   }

   public T get(int index) {
      if (_data == null) {
         return null;
      } else {
         return _data.get(index);
      }
   }

   public int count() {
      if (_data == null) {
         return 0;
      } else {
         return _data.size();
      }
   }

   public void setData(List<T> data) {
      _data = new LinkedList<T>(data);

      notifyUpdated();
   }

   public void appendData(List<T> data) {
      if (_data == null) {
         setData(data);
      } else {

         _data.addAll(data);

         notifyUpdated();
      }
   }

   public boolean haveData() {
      return _data != null;
   }


   private List<T> _data = null;
}