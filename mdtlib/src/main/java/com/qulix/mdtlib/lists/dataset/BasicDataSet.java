package com.qulix.mdtlib.lists.dataset;

import com.qulix.mdtlib.subscription.RunnableSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;



public abstract class BasicDataSet<DataType> implements DataSet<DataType> {

   @Override public Subscription<Runnable> onUpdate() {
      return _onUpdate;
   }

   protected final void notifyUpdated() {
      _onUpdate.run();
   }

   private RunnableSubscription _onUpdate = new RunnableSubscription();
}
