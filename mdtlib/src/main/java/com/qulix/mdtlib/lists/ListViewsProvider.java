package com.qulix.mdtlib.lists;

import com.qulix.mdtlib.subscription.interfaces.Subscription;
import android.view.View;
import android.view.ViewGroup;

public interface ListViewsProvider<DataType> {

   int getCount();
   int getViewTypeCount();
   int getItemViewType(int position);

   DataType getItem(int position);

   View getView(int position, View convertView, ViewGroup parent);

   Subscription<Runnable> onUpdate();
}