package com.qulix.mdtlib.lists;

import android.widget.Checkable;
import android.widget.FrameLayout;
import android.content.Context;

/**
 * @class ItemViewHolder
 *
 * @brief This is view item holder
 *
 * To make sure that list item will be unreferenced as fast as list
 * will unreference view.
 *
 * This frame layout needed to solve next problems:
 *
 * 1. We need access to list item to update elements in it, but we have
 * list which, when return view to us for reuse don't know anything
 * about our list items. So we wrapping item in item holder and passing
 * holder to list. After that we can be sure that list will give us
 * holder for reuse and we can transform View to ItemViewHolder and set
 * data.
 *
 * Old solution was to keep map of items, but this caused problem with
 * leaking of items. This soulution will unreference list item as fast
 * as list view will unreference item view.
 *
 * 2. Additionally, this object will keep data for item until item is
 * attached to view and notify cleanup for item as fast as it will be
 * detached. This gives us natural possibility to perform list/cover
 * slide items to be cleaned up as fast as possible, which is extremely
 * useful in case of too many async work at bg and limited resources.
 */
public class ItemViewHolder<DataType, ItemType extends ListItem<DataType>> extends FrameLayout implements Checkable {

   ItemType _item;

   boolean _attached;

   DataType _itemData;

   boolean _isChecked;

   public ItemViewHolder(Context context, ItemType item) {
      super(context);
      _item = item;
      addView(item.getView());
   }

   /**
    * In case if item is attached for now, this method will pass data
    * directly to the item. In opposite case, if view is not attached,
    * data will be keept for the moment when item is attached or GC'ed
    * which comes earlier. In case of attach - this data will be
    * passed to item via "update" method.
    *
    * @param data data to set to item. Must not be null.
    */
   public void resetItemData(DataType data) {
      if (data == null) throw new IllegalArgumentException("Data can not be null!");

      if (_attached) {
         _item.update(data);
      }

      _itemData = data;
   }

   /**
    * On attach to window we should update item with pending data if
    * any.
    */
   @Override public void onAttachedToWindow() {
      super.onAttachedToWindow();

      // if already attached - nothing to handle
      if (_attached) return ;

      _attached = true;

      if (_itemData != null) {
         _item.update(_itemData);
      }
   }

   /**
    * On detach of window we need to make item cleanup.
    */
   @Override public void onDetachedFromWindow() {
      if (_attached) {  // do nothing if was not attached
         _attached = false;
         _isChecked = false;
         _item.cleanup();
      }

      super.onDetachedFromWindow();
   }

   public ItemType item() {
      return _item;
   }

   public DataType data() {
      return _itemData;
   }

   @Override
   public boolean isChecked() {
      return _isChecked;
   }

   @Override
   public void setChecked(boolean checked) {
      _isChecked = checked;
      _item.setChecked(_isChecked);
   }

   @Override
   public void toggle() {
      _isChecked = !_isChecked;
      _item.setChecked(_isChecked);
   }

}