package com.qulix.mdtlib.lists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

/**
 *
 * @author dryganets
 *
 * @param <DataType>
 */
public abstract class ListItem<DataType> {

   private View _rootLayout;
   private DataType _data;
   private Context _context;

   public ListItem() {

   }

   public DataType getData() {
      return _data;
   }

   public void setContext(Context context) {
      _context = context;
   }

   public Context getContext() {
      return _context;
   }

   /**
    * Childes can override this method to make cleanup. This method will be
    * called on detach event. Note, that cleanup should be done to state
    * from where item can be correctly reconstructed with call to update.
    */
   public void cleanup() {}

   public final View getView() {
      if (_rootLayout == null) {

         LayoutInflater inflater = (LayoutInflater)getContext().getSystemService
                 (Context.LAYOUT_INFLATER_SERVICE);
         _rootLayout = inflater.inflate(getItemLayoutId(), null);
         initLayout();
      }
      return _rootLayout;
   }

   /**
    * For overriding in child types
    */
   protected abstract void update();

   /**
    * update layout components
    *
    * @param data data to update with
    */
   public final void update(DataType data) {
      _data = data;
      update();
   }

   /**
    * This method used in case data for ListView refetched and View going to be
    * reused with another data
    */
   public void clear() {
      getView();
   }

   /**
    * inflate layout components
    */
   protected void initLayout() {}

   /**
    *  Override this method to getting message of change checked state of the item.
    */
   public void setChecked(boolean checked) {};


   /**
    *
    * @return id of layout to inflate
    */
   protected abstract int getItemLayoutId();
}
