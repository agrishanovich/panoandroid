package com.qulix.mdtlib.lists.dataset;

import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.OperationTracker;
import com.qulix.mdtlib.functional.Maybe;
import java.util.List;

import com.qulix.mdtlib.functional.Receiver;

/**
 * @class CachedLoadDataSet
 *
 * @brief Executes loader and stores data internally.
 *
 * The main idea of this class is to provide the way to somewhere store
 * data, obtained from loader to avoid calling loader every time part
 * of data is needed.

 */
public class CachedLoadDataSet<DataType> extends BasicDataSet<DataType> {

   public interface Loader<DataType> {
      Maybe<Operation> request(Receiver<List<DataType>> receiver);
   }


   public CachedLoadDataSet(Loader<DataType> loader,
                            OperationTracker operationTracker) {

      if (loader == null) throw new IllegalArgumentException("Loader can not be null.");
      if (operationTracker == null) throw new IllegalArgumentException("Operation tracker can not be null.");

      _operationTracker = operationTracker;
      _loader = loader;
   }

   public int count() {
      if (_data != null) {
         return _data.size();
      } else {
         return 0;
      }
   }

   public DataType get(int index) {
      if (_data != null) {
         return _data.get(index);
      } else {
         return null;
      }
   }

   public void reload() {
      clear();
      Maybe<Operation> gettingData
         = _loader.request(new Receiver<List<DataType>>() {
               @Override public void receive(List<DataType> data) {
                  _data = data;
                  notifyUpdated();
               }
            } );

      if (gettingData.isValue()) {
         _operationTracker.registerForTermination(gettingData.value());
      }
   }

   private void clear() {
      if (_data != null) {
         _data.clear();
         _data = null;
      }

      notifyUpdated();
   }

   private List<DataType> _data;

   private Loader<DataType> _loader;

   private OperationTracker _operationTracker;
}