package com.qulix.mdtlib.utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


public class PhoneUtils {
   public static void callNumber(Context context, String number) {
      // we need to create new task in case if context is not activity.
      boolean newTask = ! (context instanceof Activity);
      Intent it = new Intent(Intent.ACTION_CALL,
                             Uri.parse("tel: " + number));
      if (newTask) {
         it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      }
      context.startActivity(it);
   }


}