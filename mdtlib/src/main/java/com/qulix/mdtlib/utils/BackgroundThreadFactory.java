package com.qulix.mdtlib.utils;

import java.util.concurrent.ThreadFactory;

public class BackgroundThreadFactory {
	
	public static ThreadFactory instance(final String name) {
		return new ThreadFactory(){
			public Thread newThread(Runnable r) {
				Thread t =  new Thread(r, name);
				t.setPriority(Thread.MIN_PRIORITY);
				return t;
			} 
		};
	}
	
	
}