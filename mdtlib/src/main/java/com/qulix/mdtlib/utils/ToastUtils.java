package com.qulix.mdtlib.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtils {
   public static void showShortText(Context context,
                                    String text) {
      Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
   }
}