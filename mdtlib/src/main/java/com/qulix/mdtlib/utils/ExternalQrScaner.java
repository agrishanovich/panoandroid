package com.qulix.mdtlib.utils;

import java.io.Serializable;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * User of external qr scaner should implement ActivityResultHandler to
 * pass result to instance of qr scanner.
 */

@SuppressWarnings("serial")
public class ExternalQrScaner implements Serializable {

   private static final String
      QR_PACKAGE = "com.google.zxing.client.android",
      QR_ACTION = QR_PACKAGE + ".SCAN",
      QR_SCAN_MODE = "SCAN_MODE",
      QR_CODE_MODE = "QR_CODE_MODE",
      QR_SCAN_RESULT = "SCAN_RESULT";


   public ExternalQrScaner(int requestIdToUse) {
      _requestIdToUse = requestIdToUse;
   }

   /**
    * @return true if scanner was opened successfully, false in case if no
    * application found.
    */
   public boolean tryOpenScanner(Activity activity) {

      final Intent intent = new Intent(QR_ACTION);
      intent.putExtra(QR_SCAN_MODE, QR_CODE_MODE);

      // This flag clears the called app from the activity stack, so users arrive in the expected
      // place next time this application is restarted.
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

      try {
         activity.startActivityForResult(intent,
                                         _requestIdToUse);

         return true;
      } catch (ActivityNotFoundException e) {
         return false;
      }
   }

   /**
    * In case if tryOpenScanner returned false, this method can be called
    * to open market on page with external scaner application.
    */
   public void showExternalScanerInMarket(Context context) {
      Uri uri = Uri.parse("market://search?q=pname:" + QR_PACKAGE);
      Intent intent = new Intent(Intent.ACTION_VIEW, uri);
      context.startActivity(intent);
   }


   /**
    * @return scanned qr string in case if scanned, null if this not
    * scanned or if this is not scanning result
    */
   public String qrStringFromActivityResult(int requestCode, int resultCode, Intent data) {
      if (requestCode == _requestIdToUse
          && resultCode == Activity.RESULT_OK) {

         return data.getStringExtra(QR_SCAN_RESULT);
      }

      return null;
   }

   private int _requestIdToUse;
}