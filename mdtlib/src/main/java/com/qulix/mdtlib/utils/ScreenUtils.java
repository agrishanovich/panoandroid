package com.qulix.mdtlib.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class ScreenUtils {

   enum DeviceType {
      PHONE,
      TABLET
   }

   public static void logCurrentState(Context context) {

      if (isPhone(context)) {
         Log.i(_logClassTag, "Device is phone!");
      }

      if (isTablet(context)) {
         Log.i(_logClassTag, "Device is tablet!");
      }

      if (isPortrait(context)) {
         Log.i(_logClassTag, "Screen orientation is portrait!");
      }

      if (isLandscape(context)) {
         Log.i(_logClassTag, "Screen orientation is landscape!");
      }
   }

   public static boolean isPhone(Context context) {
      return getDeviceType(context) == DeviceType.PHONE;
   }

   public static boolean isTablet(Context context) {
      return getDeviceType(context) == DeviceType.TABLET;
   }

   public static boolean isLandscape(Context context) {
      return getCurrentConfiguration(context).orientation == Configuration.ORIENTATION_LANDSCAPE;
   }

   @SuppressWarnings("deprecation")
   public static boolean isPortrait(Context context) {
      int orientation = getCurrentConfiguration(context).orientation;
      return (orientation == Configuration.ORIENTATION_PORTRAIT) ||
              (orientation == Configuration.ORIENTATION_SQUARE);
   }

   public static DeviceType getDeviceType(Context context) {

      if (!_isTypeDetected) {
         Configuration config = getCurrentConfiguration(context);

         if (((android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) && (config.smallestScreenWidthDp >= 600)) ||
                 /*(config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE ||*/
                 (config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {

            _deviceType = DeviceType.TABLET;
         } else {
            _deviceType = DeviceType.PHONE;
         }

         _isTypeDetected = true;
      }

      return _deviceType;
   }

   private static Configuration getCurrentConfiguration(Context context) {
      return context.getResources().getConfiguration();
   }

   public static int displayWidth(Context context) {
      return size(context).x;
   }

   public static int displayHeight(Context context) {
      return size(context).y;
   }

   @SuppressWarnings("deprecation")
   private static Point size(Context context) {
      WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
      Display display = wm.getDefaultDisplay();

      final Point size = new Point();

      try {
         display.getSize(size);
      } catch (java.lang.NoSuchMethodError ignore) { // Older device
         size.x = display.getWidth();
         size.y = display.getHeight();
      }

      return size;
   }

   private static DeviceType _deviceType;
   private static boolean _isTypeDetected = false;

   private static final String _logClassTag = "ScreenUtils";
}
