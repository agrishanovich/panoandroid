package com.qulix.mdtlib.utils;

import android.view.View;
import com.qulix.mdtlib.views.interfaces.ViewController;


/**
 * @class ViewClickHandler
 *
 * @brief Suppressing by time click handler
 *
 * The problem is: in case if after view click there is still time to click
 * another time, second click will be passed to handler too. So object of
 * this class is intended to do click suppressing for some time after
 * click to avoid fast clicks pass.
 *
 * Note, however, that this should not be used where multiple clicks
 * actually must be passed.
 */
@SuppressWarnings("JavaDoc")
public abstract class ViewClickHandler implements View.OnClickListener {

   private static final int ClickBlockTimeout = 200;

   private long lastClickTime;

   public ViewClickHandler(ViewController controller) {
      _controller = controller;
   }

   public ViewClickHandler(ViewController controller, View view) {
      this(controller);

      view.setOnClickListener(this);
   }

   public ViewClickHandler(ViewController controller, View view, int targetViewId) {
      this(controller, view.findViewById(targetViewId));
   }

   // override this method in subclasses for change timeout interval
   public int clickBlockTimeout() {
      return ClickBlockTimeout;
   }

   /**
    * This method must be overrided by the user to handle click.
    */
   public abstract void handleClick();

   @Override public final void onClick(View v) {
      long current = System.currentTimeMillis();

      if (lastClickTime + clickBlockTimeout() < current) {
         lastClickTime = current;

         //TODO this check is needed because call of onClick()
         //TODO is asynchronous and handleClick could be called from inactive VC
         if (_controller == null || _controller.isActive()) {
            handleClick();
         }
      }
   }

   private ViewController _controller;
}