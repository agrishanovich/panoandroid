package com.qulix.mdtlib.utils;

import android.view.View;
import android.widget.AdapterView;
import com.qulix.mdtlib.views.interfaces.ViewController;

public abstract class ViewEventHandler implements View.OnClickListener, AdapterView.OnItemClickListener {

   private static final int EVENT_BLOCKING_TIME_DEFAULT = 200;

   public ViewEventHandler(ViewController viewController) {
      _viewController = viewController;
   }

   public ViewEventHandler(ViewController viewController, boolean eventBlockingTimeEnabled) {
      _viewController = viewController;
      _eventBlockingTimeEnabled = eventBlockingTimeEnabled;
   }

   @Override
   public final void onClick(View v) {
      if (mustDispatchEvent()) {
         handleEvent();
      }
   }

   @Override
   public final void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      if (mustDispatchEvent()) {
         handleEvent(parent, view, position, id);
      }
   }

   protected void handleEvent() {}
   protected void handleEvent(AdapterView<?> parent, View view, int position, long id) {}

   public int eventBlockingTime() {
      return EVENT_BLOCKING_TIME_DEFAULT;
   }

   private boolean mustDispatchEvent() {
      long currentTime = System.currentTimeMillis();

      boolean viewControllerActiveOrNull = _viewController == null || _viewController.isActive();

      boolean eventBlockingTimeExpired = false;
      if (_lastEventBlockingTime + eventBlockingTime() < currentTime) {
         _lastEventBlockingTime = currentTime;
         eventBlockingTimeExpired = true;
      }

      if (_eventBlockingTimeEnabled) {
         return eventBlockingTimeExpired && viewControllerActiveOrNull;
      } else {
         return viewControllerActiveOrNull;
      }
   }

   private ViewController _viewController;

   private long _lastEventBlockingTime = 0;
   private boolean _eventBlockingTimeEnabled = true;
}
