package com.qulix.mdtlib.utils;

import android.telephony.SmsManager;

public class SmsUtils {
   public static void sendSms(String phone,
                              String text) {
      SmsManager.getDefault().sendTextMessage(phone,
                                              null,
                                              text,
                                              null,
                                              null);
   }
}