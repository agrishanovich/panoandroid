package com.qulix.mdtlib.utils;

import android.os.Looper;

import java.util.Arrays;
import java.util.Collection;

public class Validate {
   public static void notNull(final Object val,
                              final String variableName) {
      argNotNull(val, variableName);
   }

   public static void argNotNull(final Object val,
                                 final String variable) {
      throwIf(val, IS_NULL, ARGUMENT_EXCEPTION, variable);
   }

   public static void stateNotNull(final Object val,
                                   final String variable) {
      throwIf(val, IS_NULL, STATE_EXCEPTION, variable);
   }

   public static void notEmpty(final String val,
                               final String variableName) {
      argNotEmpty(val, variableName);
   }

   public static void argNotEmpty(final String val,
                                  final String variableName) {
      throwIf(val, STRING_IS_EMPTY, ARGUMENT_EXCEPTION, variableName);
   }

   public static void stateNotEmpty(final String val,
                                    final String variableName) {
      throwIf(val, STRING_IS_EMPTY, STATE_EXCEPTION, variableName);
   }

   public static void notEmpty(final Collection<?> val,
                               final String variableName) {
      argNotEmpty(val, variableName);
   }

   public static void argNotEmpty(final Collection<?> val,
                                  final String variableName) {
      throwIf(val, COLLECTION_IS_EMPTY, ARGUMENT_EXCEPTION, variableName);
   }

   public static void stateNotEmpty(final Collection<?> val,
                                    final String variableName) {
      throwIf(val, COLLECTION_IS_EMPTY, STATE_EXCEPTION, variableName);
   }

   public static void isMainThread() {
      if (Looper.myLooper() != Looper.getMainLooper()) {
         throw new IllegalStateException("Method can only be accessed on the applications main thread.");
      }
   }

   public static void isWorkerThread() {
      if (Looper.myLooper() == Looper.getMainLooper()) {
         throw new IllegalStateException("Method can only be accessed on the worker thread.");
      }
   }

   public interface ValidateThread {
      void validate();
   }

   public static ValidateThread rememberThread() {
      final long currentThreadId = Thread.currentThread().getId();
      return new ValidateThread() {
         @Override public void validate() {
            final long validableThreadId = Thread.currentThread().getId();

            if (validableThreadId != currentThreadId) {
               throw new IllegalStateException("This method can not be used on this thread.");
            }
         }
      };
   }

   public static void assertIsTrue(boolean test, String message) {
      if (!test) {
         throw new IllegalArgumentException(message);
      }
   }

   public static void isTrue(boolean test, String message, Object... messageParams) {
      if (!test) {
         try {
            message = String.format(message, messageParams);
         } catch (RuntimeException ex) {
            message = message + " " + Arrays.toString(messageParams);
         }
         throw new IllegalArgumentException(message);
      }
   }


   private interface ExceptionFactory {
      RuntimeException newException(final String message);
   }

   private static final ExceptionFactory STATE_EXCEPTION = new ExceptionFactory() {
      @Override public RuntimeException newException(final String message) {
         return new IllegalStateException(message);
      }
   };

   private static final ExceptionFactory ARGUMENT_EXCEPTION = new ExceptionFactory() {
      @Override public RuntimeException newException(final String message) {
         return new IllegalArgumentException(message);
      }
   };

   private interface FailCheck<T> {
      boolean check(T object);
      String message();
   }

   private static final FailCheck<Object> IS_NULL = new FailCheck<Object>() {
      @Override public boolean check(final Object object) {
         return object == null;
      }

      @Override public String message() {
         return "must not be null";
      }
   };

   private static final FailCheck<String> STRING_IS_EMPTY = new FailCheck<String>() {
      @Override public boolean check(final String string) {
         return string == null || string.length() == 0;
      }

      @Override public String message() {
         return "must not be empty";
      }
   };

   private static final FailCheck<Collection<?>> COLLECTION_IS_EMPTY = new FailCheck<Collection<?>>() {
      @Override public boolean check(final Collection<?> collection) {
         return collection == null || collection.size() == 0;
      }

      @Override public String message() {
         return "must not be empty";
      }
   };




   private static <T> void throwIf(final T value,
                                   final FailCheck<T> ifFail,
                                   final ExceptionFactory exception,
                                   final String variableName) {
      if (ifFail.check(value)) {
         throw exception.newException(variableName
                                              + " "
                                              + ifFail.message());
      }
   }


}
