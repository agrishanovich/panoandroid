package com.qulix.mdtlib.utils;

import android.content.Context;
import android.os.Vibrator;

public class VibraUtils {
   public static void vibrateIfPossible(Context context,
                                        long milliseconds) {
      Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);

      vibrator.vibrate(milliseconds);
   }
}