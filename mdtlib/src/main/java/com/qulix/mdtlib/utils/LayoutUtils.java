package com.qulix.mdtlib.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public class LayoutUtils {
   public static View inflateLayout(Context context, int layoutId) {
      LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      return inflater.inflate(layoutId, null);
   }

   public static int dpToPx(Context context, float dp) {
      // Convert the dps to pixels
      final float scale = context.getResources().getDisplayMetrics().density;
      return (int) (dp * scale + 0.5f);
   }

   public static int pxToDp(Context context, float px) {
      // Convert the px to dp
      final float scale = context.getResources().getDisplayMetrics().density;
      return (int) ((px - 0.5f) / scale);
   }

   public static int xLargeDpToPx(Context context, float dp) {
      // Convert the dps to pixels for tables
      return (int) dpToPx(context, 2 * dp);
   }

   public static int xLargePxToDp(Context context, float px) {
      // Convert the pixels to dps for tablets
      return (int) (pxToDp(context, px) / 2);
   }
}