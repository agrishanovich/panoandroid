package com.qulix.mdtlib.utils;

import java.io.*;

public class StreamUtils {
   public static String readAll(InputStream stream) throws IOException {
      final int READ_STEP_SIZE = 20000;

      char buffer[] = new char[READ_STEP_SIZE];

      final BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "utf-8"),
                                                       READ_STEP_SIZE);

      final StringBuilder builder = new StringBuilder();

      int read = 0;
      while ((read = reader.read(buffer, 0, READ_STEP_SIZE)) > 0) {
         builder.append(buffer, 0, read);
      }

      return builder.toString();
   }

   public static  void copyStream(InputStream inputStream, OutputStream outputStream) throws IOException {
      final int BUFFER_SIZE = 1024;

      byte[] buffer = new byte[BUFFER_SIZE];
      int length = 0;
      while ((length = inputStream.read(buffer)) > 0) {
         outputStream.write(buffer, 0, length);
      }
   }

   public static String guessContentType(InputStream stream) throws IOException {
      String mime = null;

      byte [] firstBytes = new byte[2];
      stream.read(firstBytes);
      int header = ((firstBytes[0] & 0xFF) << 8) | (firstBytes[1] & 0xFF);
      switch (header) {
         case 0xFFD8:
            mime = "image/jpeg";
            break;
         case 0x424D:
            mime = "image/bmp";
            break;
      }

      return mime;
   }
}