package com.qulix.mdtlib.http;

import android.util.Log;
import com.qulix.mdtlib.concurrency.ThreadPool;
import com.qulix.mdtlib.concurrency.Worker;
import com.qulix.mdtlib.connection.BackedStream;
import com.qulix.mdtlib.connection.StreamResolver;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.subscription.RunnableSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;
import org.apache.http.HttpResponse;
import org.apache.http.client.RedirectHandler;
import org.apache.http.cookie.Cookie;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

final class HttpStreamRequester implements Cancellable {

   public interface RecoveryWay {
      void retry();
      void fail();
   }

   public interface RecoveryStrategy {
      Cancellable decideRecoveryWay(RecoveryWay way);
      void close();
   }

   public HttpStreamRequester(Worker worker,
                              HttpRequestDescription request,
                              long offset,
                              Receiver<InputStream> streamReceiver,
                              Receiver<HttpResponse> responseReceiver,
                              Runnable errorRunable,
                              RecoveryStrategy recoveryStrategy,
                              HttpStreamConstruction.SuccessCondition successCondition,
                              RedirectHandler redirectHandler) {
      _worker = worker;
      _request = request;
      _offset = offset;
      _streamReceiver = streamReceiver;
      _responseReceiver = responseReceiver;
      _errorRunable = errorRunable;
      _recoveryStrategy = recoveryStrategy;
      _successCondition = successCondition;
      _redirectHandler = redirectHandler;

      if (_worker == null) throw new IllegalArgumentException("_worker can not be null");
      if (_request == null) throw new IllegalArgumentException("_request can not be null");

      if (_streamReceiver == null) throw new IllegalArgumentException("_streamReceiver can not be null");
      if (_errorRunable == null) throw new IllegalArgumentException("_errorRunable can not be null");
      if (_recoveryStrategy == null) throw new IllegalArgumentException("_recoveryStrategy can not be null");

      startTry();
   }

   private void startTry() {

      _construction = new HttpStreamConstruction(_worker,
                                                 _request,
                                                 _offset,
                                                 new Receiver<InputStream>() {
                                                    @Override public void receive(InputStream stream) {
                                                       _construction = null;
                                                       cleanup();
                                                       _streamReceiver.receive(stream);
                                                    }
                                                 },
                                                 _responseReceiver,
                                                 new Receiver<Throwable>() {
                                                    @Override
                                                    public void receive(Throwable error) {
                                                       _construction = null;
                                                       startRecovery();
                                                       Log.e("HttpStreamRequester", "Failed: " + error);
                                                       error.printStackTrace();
                                                    }
                                                 },
                                                 _successCondition,
                                                 _redirectHandler);

   }

   private void startRecovery() {
      _recoveryRequest = _recoveryStrategy.decideRecoveryWay(new RecoveryWay() {
         @Override
         public void retry() {
            _recoveryRequest = null;
            startTry();
         }

         @Override
         public void fail() {
            _recoveryRequest = null;
            _errorRunable.run();
         }
      });
   }

   @Override
   public void cancel() {
      cleanup();
   }

   private void cleanup() {
      _recoveryStrategy.close();

      if (_recoveryRequest != null) {
         _recoveryRequest.cancel();
         _recoveryRequest = null;
      }

      if (_construction != null) {
         _construction.cancel();
         _construction = null;
      }
   }

   private Worker _worker;
   private HttpRequestDescription _request;
   private long _offset;
   private Receiver<InputStream> _streamReceiver;
   private Receiver<HttpResponse> _responseReceiver;
   private Runnable _errorRunable;
   private RecoveryStrategy _recoveryStrategy;
   private HttpStreamConstruction.SuccessCondition _successCondition;

   private HttpStreamConstruction _construction;
   private Cancellable _recoveryRequest;
   private RedirectHandler _redirectHandler;
}


public class HttpStreamResolver implements StreamResolver<HttpRequestDescription> {

   private static final int THREAD_POOL_SIZE = 10;

   private static volatile HttpStreamResolver _sharedInstance;

   public static HttpStreamResolver instance() {
      if (_sharedInstance == null) {
         _sharedInstance = new HttpStreamResolver();
      }

      return _sharedInstance;
   }

   /**
    * This is not fully sigleton - normal instantiation allowed too.
    */
   public HttpStreamResolver() {

   }

   static public List<Cookie> getCookies() {
      return HttpStreamConstruction.getCookies();
   }

   static public void clearCookies() {
      HttpStreamConstruction.clearCookies();
   }

   static public void updateCookies(List<Cookie> cookies) {
      HttpStreamConstruction.updateCookies(cookies);
   }

   public final BackedStream resolveToStream(final HttpRequestDescription request) {

      return resolveToStream(request, null, null);
   }

   public final BackedStream resolveToStream(final HttpRequestDescription request, Receiver<HttpResponse> httpResponseReceiver) {
      return resolveToStream(request, httpResponseReceiver, null);
   }

   public final BackedStream resolveToStream(final HttpRequestDescription request, RedirectHandler redirectHandler) {

      return resolveToStream(request, null, redirectHandler);
   }

   public final BackedStream resolveToStream(final HttpRequestDescription request,
                                             final Receiver<HttpResponse> httpResponseReceiver,
                                             final RedirectHandler redirectHandler) {
      return new BackedStream(new BackedStream.StreamFactory() {

         @Override
         public void close() { /* ignore this call, nothing to do */ }

         @Override
         public Cancellable requestStream(final long offset,
                                          final Receiver<InputStream> streamReceiver,
                                          final Runnable errorReceiver) {

            return new HttpStreamRequester(worker(),
                  request,
                  offset,
                  streamReceiver,
                  httpResponseReceiver,
                  errorReceiver,
                  createStreamRequesterRecoveryStrategy(),
                  createSuccessCondition(),
                  redirectHandler);

         }

      },
            createStreamRecoveryStrategy());
   }

   protected HttpStreamConstruction.SuccessCondition createSuccessCondition() {
      return null;
   }

   protected BackedStream.RecoveryStrategy createStreamRecoveryStrategy() {
      return new BackedStream.RecoveryStrategy() {
         @Override
         public Cancellable decideRecoveryWay(IOException e,
                                              BackedStream.RecoveryWay way) {
            _errorEvent.run();
            way.fail();

            return null;
         }

         @Override
         public void close() { /* do nothing */ }
      };
   }

   protected HttpStreamRequester.RecoveryStrategy createStreamRequesterRecoveryStrategy() {
      return new HttpStreamRequester.RecoveryStrategy() {
         @Override
         public Cancellable decideRecoveryWay(HttpStreamRequester.RecoveryWay way) {
            _errorEvent.run();
            way.fail();

            return null;
         }

         @Override
         public void close() { /* do nothing */ }
      };
   }

   private Worker worker() {
      if (_worker == null) {
         _worker = spawnWorker();
      }

      return _worker;
   }

   public Subscription<Runnable> errorEvent() {
      return _errorEvent;
   }

   /**
    * This method can be overriden to make resolver use other executors service.
    */
   protected Worker spawnWorker() {
      return _staticWorker;
   }

   private static volatile Worker _staticWorker = ThreadPool.newWithBackgroundPriority(THREAD_POOL_SIZE, "Default http stream resolver thread pool");
   private Worker _worker;

   private RunnableSubscription _errorEvent = new RunnableSubscription();


}