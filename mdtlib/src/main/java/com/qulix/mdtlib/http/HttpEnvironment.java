package com.qulix.mdtlib.http;

import javax.net.ssl.SSLContext;

public class HttpEnvironment {
   public static void setSSLContext(SSLContext context) {
      HttpStreamConstruction.setSSLContext(context);
   }
}
