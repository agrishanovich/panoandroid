package com.qulix.mdtlib.http;

import com.qulix.mdtlib.pair.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public enum HttpMethod {

   GET {
      @Override HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException {
         return newGetHttpRequest(requestDescription.url(),
                 requestDescription.parameters());
      }
   },
   POST {
      @Override HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException {
         return newPostHttpRequest(requestDescription.url(),
                                   requestDescription.parameters(),
                                   requestDescription.body());
      }
   },
   DELETE {
      @Override
      HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException {
         return newDeleteHttpRequest(requestDescription.url(),
                 requestDescription.parameters());
      }
   },
   HEAD {
      @Override
      HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException {
         return newHeadHttpRequest(requestDescription.url(),
                 requestDescription.parameters());
      }
   },
   PUT {
      @Override HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException {
         return newPutHttpRequest(requestDescription.url(),
                                   requestDescription.parameters(),
                                   requestDescription.body());
      }
   },
   PATCH {
      @Override HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException {
         return newPatchHttpRequest(requestDescription.url(),
                                  requestDescription.parameters(),
                                  requestDescription.body());
      }
   };


   abstract HttpRequestBase createHttpRequest(HttpRequestDescription requestDescription) throws IOException, ClientProtocolException;

   /**
    * Create GET request. This will build request from the parameters and
    * return it.
    *
    * @param baseUrl request url
    * @param parameters parameters for this request
    * @param headers additional headers for this request
    *
    * @return newly constructed HttpRequestBase object
    */
   static HttpRequestBase newGetHttpRequest(String baseUrl,
                                            List<Pair<String, String>> parameters)
           throws ClientProtocolException, IOException {

      String url = formatUrl(baseUrl, parameters);

      // create GET request
      HttpGet request = new HttpGet(url);

      return request;
   }


   static HttpRequestBase newHeadHttpRequest(String baseUrl,
                                             List<Pair<String, String>> parameters)
           throws ClientProtocolException, IOException {

      String url = formatUrl(baseUrl, parameters);

      // create HEAD request
      HttpHead request = new HttpHead(url);

      return request;
   }

   static HttpRequestBase newPostHttpRequest(String baseUrl,
                                             List<Pair<String, String>> parameters,
                                             HttpBody body) {
      HttpPost request = new HttpPost(baseUrl);
      return fillRequest(request, parameters, body);
   }

   static HttpRequestBase newPatchHttpRequest(String baseUrl,
           List<Pair<String, String>> parameters,
           HttpBody body) {
      HttpPatch request = new HttpPatch(baseUrl);
      return fillRequest(request, parameters, body);
   }

   static HttpRequestBase newPutHttpRequest(String baseUrl,
           List<Pair<String, String>> parameters,
           HttpBody body) {
      HttpPut request = new HttpPut(baseUrl);
      return fillRequest(request, parameters, body);
   }

   static HttpRequestBase fillRequest(HttpEntityEnclosingRequestBase requestBase,
           List<Pair<String, String>> parameters,
           HttpBody body) {
      HttpEntity entity = null;
      if (null == body) {
         List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(parameters.size());

         for (Pair<String, String> param : parameters) {
            String name = param.first;
            String value = param.second;

            nameValuePairs.add(new BasicNameValuePair(name, value));
         }

         try {
            entity = new UrlEncodedFormEntity(nameValuePairs, "utf-8");

         } catch(UnsupportedEncodingException e) {
            try {
               entity = new UrlEncodedFormEntity(nameValuePairs);

            } catch(UnsupportedEncodingException ee) {
               throw new RuntimeException("Panic: can't put encoded entity");
            }
         }

      } else {
         entity = body.createEntity();
      }

      requestBase.setEntity(entity);

      return requestBase;
   }

   static HttpRequestBase newDeleteHttpRequest(String baseUrl, List<Pair<String, String>> parameters) {
      String url = formatUrl(baseUrl, parameters);
      HttpDelete request = new HttpDelete(url);
      return request;
   }

   private static String formatUrl(final String urlBase, List<Pair<String, String>> parameters) {
      StringBuilder result = new StringBuilder(urlBase);

      int length = 0;

      if (parameters != null) {
         length = parameters.size();
      }

      for (int i = 0; i < length; i++) {
         if (i == 0) {
            result.append("?");
         }

         String key = encode(parameters.get(i).first);
         String value = encode(parameters.get(i).second);

         result.append(key).append("=").append(value);

         if (i != length - 1) {
            result.append("&");
         }
      }

      return result.toString();
   }

   /**
    * Try to encode string to the utf-8.
    *
    * @param str string
    * @return encoded string
    */
   private static String encode(String str) {
      try {
         return URLEncoder.encode(str, "utf-8");
      } catch (UnsupportedEncodingException e) {
         throw new RuntimeException("Url encoder doesn't support utf-8");
      }
   }
}