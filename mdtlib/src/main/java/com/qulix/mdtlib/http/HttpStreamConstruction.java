package com.qulix.mdtlib.http;

import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.concurrency.Worker;
import com.qulix.mdtlib.cookie.PersistentCookieStore;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.BrowserCompatSpec;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;


class AllowingAllSSLSocketFactory extends SSLSocketFactory {
   SSLContext sslContext = SSLContext.getInstance("TLS");

   public AllowingAllSSLSocketFactory(KeyStore trustStore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
      super(trustStore);

      TrustManager tm = new X509TrustManager() {
         public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
         }

         public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
         }

         public X509Certificate[] getAcceptedIssuers() {
            return null;
         }
      };

      sslContext.init(null, new TrustManager[]{tm}, null);
   }

   @Override
   public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
      return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
   }

   @Override
   public Socket createSocket() throws IOException {
      return sslContext.getSocketFactory().createSocket();
   }
}

class TrustSSLSocketFactory extends SSLSocketFactory {

   public TrustSSLSocketFactory(SSLContext sslContext) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
      super(null);
      _sslContext = sslContext;
   }

   @Override
   public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
      return _sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
   }

   @Override
   public Socket createSocket() throws IOException {
      return _sslContext.getSocketFactory().createSocket();
   }

   private SSLContext _sslContext;
}

final class HttpStreamConstruction implements Cancellable {

   private static final int CONNECTION_TIMEOUT = 60 * 1000;

   private static final int READ_TIMEOUT = 60 * 1000;

   private static final int SKIPPING_STEP = 1024 * 20;

   static {
      _cookieStore = new PersistentCookieStore();
   }

   static public List<Cookie> getCookies() {
      return _cookieStore.getCookies();
   }

   public static void setSSLContext(SSLContext sslContext) {
      _sslContext = sslContext;
   }

   static public void updateCookies(List<Cookie> cookies) {

      List<Cookie> tempCookies = new ArrayList<Cookie>(cookies);
      _cookieStore.clear();

      for (Cookie cookie : tempCookies) {
         _cookieStore.addCookie(cookie);
      }
   }

   static public void clearCookies() {
      _cookieStore.clear();
   }

   public interface SuccessCondition {
      boolean isResponseOk(HttpResponse response);
   }

   HttpStreamConstruction(Worker worker,
                          HttpRequestDescription request,
                          long offset,
                          Receiver<InputStream> streamReceiver,
                          Receiver<HttpResponse> responseReceiver,
                          Receiver<Throwable> errorRunnable) {
      this(worker, request, offset, streamReceiver, responseReceiver, errorRunnable, null, null);
   }

   HttpStreamConstruction(Worker worker,
                          HttpRequestDescription request,
                          long offset,
                          Receiver<InputStream> streamReceiver,
                          Receiver<HttpResponse> responseReceiver,
                          Receiver<Throwable> errorRunnable,
                          SuccessCondition successCondition) {
      this(worker, request, offset, streamReceiver, responseReceiver, errorRunnable, successCondition, null);
   }

   HttpStreamConstruction(Worker worker,
                          HttpRequestDescription request,
                          long offset,
                          Receiver<InputStream> streamReceiver,
                          Receiver<HttpResponse> responseReceiver,
                          Receiver<Throwable> errorRunnable,
                          SuccessCondition successCondition,
                          RedirectHandler redirectHandler) {
      _worker = worker;
      _request = request;
      _offset = offset;
      _streamReceiver = streamReceiver;
      _errorRunable = errorRunnable;
      _successCondition = successCondition;
      _responseReceiver = responseReceiver;
      _redirectHandler = redirectHandler;

      startThread();
   }


   private void startThread() {
      _worker.submit(new Runnable() {
         @Override
         public void run() {
            construction();
         }
      });
   }

   private void construction() {
      synchronized (this) {
         if (_cancelled) return;
      }

      // perform stream constructing

      HttpMethod method = _request.method();

      HttpRequestBase httpRequest;

      try {
         httpRequest = method.createHttpRequest(_request);
         for (Map.Entry<String, String> entry : _request.headers().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            httpRequest.setHeader(key, value);
         }
      } catch (Exception e) {
         checkCancelAndPostError(e);
         return;
      }

      HttpResponse response;

      DefaultHttpClient client;

      SSLSocketFactory factory = null;
      synchronized (this) {
         if (_cancelled) return;

         try {
            if (_sslContext == null) {
               try {
                  KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                  trustStore.load(null, null);

                  factory = new AllowingAllSSLSocketFactory(trustStore);
                  factory.setHostnameVerifier(new AllowAllHostnameVerifier());
               } catch (Exception e) {
                  throw new Error(e.toString());
               }
            } else {
               factory = new TrustSSLSocketFactory(_sslContext);
            }
         } catch (Exception e) {
            checkCancelAndPostError(e);
            return;
         }
      }

      client = createHttpClient(factory);
      if (_redirectHandler != null) {
         client.setRedirectHandler(_redirectHandler);
      }
      _client = client;

      try {
         response = client.execute(httpRequest);
      } catch (Exception e) {
         checkCancelAndPostError(e);
         return;
      }

      boolean success = isSuccess(response);

      if (!success) {
         checkCancelAndPostError(new IOException("Response is not success"));
         return;
      }

      postResponse(response);

      InputStream stream;

      try {

         if (response.getEntity() == null) {  //handle response from head request
            stream = new ByteArrayInputStream(new byte[]{});
         } else {
            stream = response.getEntity().getContent();
         }
      } catch (Exception e) {
         checkCancelAndPostError(e);
         return;
      }

      synchronized (this) {
         if (_cancelled) {
            try {
               stream.close();
            } catch (IOException ignore) {
               //ignore it
            }

            return;
         }

         _stream = stream;
      }

      if (_offset > 0) {
         try {

            byte[] buffer = new byte[SKIPPING_STEP];

            for (int offset = 0; offset < _offset; ++offset) {
               int countToRead = (int) Math.min(SKIPPING_STEP,
                     _offset - offset);

               int reallyRead = stream.read(buffer, offset, countToRead);

               offset += reallyRead;
            }

         } catch (IOException e) {
            checkCancelAndPostError(e);
            return;
         }
      }

      postSuccess();

   }

   private synchronized void checkCancelAndPostError(Throwable error) {
      if (_cancelled) return;

      cancel();
      postError(error);
   }

   private boolean isSuccess(HttpResponse response) {
      return _successCondition == null || _successCondition.isResponseOk(response);

   }

   private void postError(final Throwable error) {
      CTHandler.post(new Runnable() {
         @Override
         public void run() {
            _errorRunable.receive(error);
         }
      });
   }

   private void postSuccess() {
      CTHandler.post(new Runnable() {
         @Override
         public void run() {
            passStream();
         }
      });
   }

   private void postResponse(final HttpResponse response) {
      if (_responseReceiver == null) return;

      CTHandler.post(new Runnable() {
         @Override
         public void run() {
            synchronized (HttpStreamConstruction.this) {
               if (_cancelled) return;

               _responseReceiver.receive(response);
            }
         }
      });
   }

   private synchronized void passStream() {
      final InputStream stream = _stream;
      _stream = null;

      final HttpClient client = _client;
      _client = null;

      if (stream == null) return;

      _streamReceiver.receive(new InputStream() {
         @Override
         public int available() throws IOException {
            throwClosedIfClosed();

            try {
               return stream.available();
            } catch (NullPointerException ignore) {
               // ignore
            }

            return 0;
         }

         @Override
         public void close() throws IOException {

            markClosed();

            new Thread() {
               @Override
               public void run() {
                  // shutting down connection manager is here because previously,
                  // in some cases, calling stream.close here deadlocked main thread.
                  //
                  // This is possible that this problem is no more actual,
                  // because of closing stream on separate thread, this is
                  // subject of checking (we are closing stream in separate
                  // thread to avoid NetworkOnMainThreadException)
                  client.getConnectionManager().shutdown();
                  try {
                     stream.close();
                  } catch (IOException e) {
                     // ignore
                  } catch (NullPointerException ignore) {
                     // ignore
                  } catch (IllegalArgumentException ignore) {

                  }
               }
            }.start();
         }

         @Override
         public void mark(int readlimit) {
            stream.mark(readlimit);
         }

         @Override
         public boolean markSupported() {
            return stream.markSupported();
         }

         @Override
         public int read(byte[] buffer) throws IOException {
            throwClosedIfClosed();

            try {
               return stream.read(buffer);
            } catch (NullPointerException ignore) { }
              catch (IllegalArgumentException ignore) { }


            return 0;
         }

         @Override
         public int read() throws IOException {
            throwClosedIfClosed();

            try {
               return stream.read();
            } catch (NullPointerException ignore) { }
              catch (IllegalArgumentException ignore) { }

            return 0;
         }

         @Override
         public int read(byte[] buffer, int offset, int length) throws IOException {
            throwClosedIfClosed();

            try {
               return stream.read(buffer, offset, length);
            } catch (NullPointerException ignore) { }
              catch (IllegalArgumentException ignore) { }

            return 0;
         }

         @Override
         public void reset() throws IOException {
            throwClosedIfClosed();
            stream.reset();
         }

         @Override
         public long skip(long byteCount) throws IOException {
            throwClosedIfClosed();
            return stream.skip(byteCount);
         }

         private synchronized void throwClosedIfClosed() throws IOException {
            if (_isClosed) throw new IOException("Stream is closed!");
         }

         private synchronized void markClosed() {
            _isClosed = true;
         }

         private boolean _isClosed;
      });
   }


   @Override
   public void cancel() {
      _worker.submit(new Runnable() {
         @Override
         public void run() {
            clear();
         }
      });
   }

   private synchronized void clear() {
      _cancelled = true;

      if (_client != null) {
         _client.getConnectionManager().shutdown();
         _client = null;
      }

      if (_stream != null) {

         try {
            _stream.close();
         } catch (IOException e) {
            // ignore
         }

         _stream = null;
      }
   }

   private DefaultHttpClient createHttpClient(SSLSocketFactory sslSocketFactory) {

      HttpParams params = new BasicHttpParams();

      // HttpConnectionManagerParams.setMaxTotalConnections(params, 100);
      HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
      HttpConnectionParams.setSoTimeout(params, READ_TIMEOUT);
      // HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
      HttpProtocolParams.setContentCharset(params, "UTF-8");
      // Create and initialize scheme registry
      SchemeRegistry schemeRegistry = new SchemeRegistry();
      schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
      schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

      // Create an HttpClient with the ThreadSafeClientConnManager.
      // This connection manager must be used if more than one thread will
      // be using the HttpClient.
      ClientConnectionManager cm = new ThreadSafeClientConnManager(params, schemeRegistry);

      DefaultHttpClient client = new DefaultHttpClient(cm, params);

      client.setCookieStore(_cookieStore);

      CookieSpecFactory csf = new CookieSpecFactory() {
         public CookieSpec newInstance(HttpParams params) {
            return new BrowserCompatSpec() {
               @Override
               protected List<Cookie> parse(HeaderElement[] elems, CookieOrigin origin) throws MalformedCookieException {
                  // XXX: Hack for hosts returning cookies with domain which is base for the origin
                  // (i.e. m2.anastasiadate.test setting cookie with domain anastasiadate.test).
                  // Such cookies are valid ("tail matching" for domains), but only if they contain leading dot
                  // so we add a leading dot here if it is not present
                  List<Cookie> cookies = super.parse(elems, origin);
                  List<Cookie> result = new LinkedList<Cookie>();
                  for (Cookie cookie : cookies) {
                     if (cookie.getDomain().startsWith(".") || cookie.getDomain().equalsIgnoreCase(origin.getHost())) {
                        result.add(cookie);
                     } else {
                        String hostDomainLC = origin.getHost().toLowerCase();
                        String cookieDomainLC = cookie.getDomain().toLowerCase();
                        if (hostDomainLC.endsWith(cookieDomainLC)) {
                           BasicClientCookie resultCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                           resultCookie.setDomain("." + cookie.getDomain());
                           result.add(resultCookie);
                        } else {
                           result.add(cookie);
                        }
                     }
                  }
                  return result;
               }
            };
         }
      };
      client.getCookieSpecs().register("fix-cookie-domain", csf);
      client.getParams().setParameter(ClientPNames.COOKIE_POLICY, "fix-cookie-domain");
      client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);

      return client;
   }

   private Worker _worker;
   private HttpRequestDescription _request;
   private long _offset;
   private Receiver<InputStream> _streamReceiver;
   private Receiver<Throwable> _errorRunable;

   private InputStream _stream;
   private HttpClient _client;
   private boolean _cancelled;

   private Receiver<HttpResponse> _responseReceiver;
   private RedirectHandler _redirectHandler;

   private static PersistentCookieStore _cookieStore;
   private static SSLContext _sslContext;
   private SuccessCondition _successCondition;
}
