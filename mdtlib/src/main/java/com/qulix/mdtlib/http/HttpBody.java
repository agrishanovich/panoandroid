/**
 * Created by makarovvm on 24.10.13
 */

package com.qulix.mdtlib.http;


import com.qulix.mdtlib.pair.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class HttpBody {

   private byte[] _data;
   private InputStream _stream;
   private List<Pair<String, ContentBody>> _parts;
   private File _file;

   public HttpBody(byte[] bodyBytes) {
      _data = bodyBytes;
   }

   public HttpBody(File file) {
      _file = file;
   }

   public HttpBody(InputStream bodyStream) {
      _stream = bodyStream;
   }

   public HttpBody(List<Pair<String, ContentBody>> parts) {
      _parts = Collections.unmodifiableList(parts);
   }

   public HttpEntity createEntity() {
      HttpEntity entity = null;
      if (null != _stream) {
         entity = new InputStreamEntity(_stream, -1);
      } else if (null != _parts) {
         MultipartEntityBuilder builder = MultipartEntityBuilder.create();
         builder.setMode(HttpMultipartMode.STRICT);
         for (Pair<String, ContentBody> part : _parts) {
            builder.addPart(part.first, part.second);
         }
         entity = builder.build();

      } else if (null != _file) {
         MultipartEntityBuilder builder = MultipartEntityBuilder.create();
         builder.addBinaryBody("file", _file);
         entity = builder.build();
      } else {
         entity = new ByteArrayEntity(_data);
      }

      return entity;
   }
}
