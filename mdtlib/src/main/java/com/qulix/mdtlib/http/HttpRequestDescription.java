package com.qulix.mdtlib.http;


import com.qulix.mdtlib.pair.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpRequestDescription {

   /**
    * Parameters of this request.
    */
   private List<Pair<String, String>> _parameters = new ArrayList<Pair<String, String>>();

   /// base url of this request
   private String _url;

   /// method of this request
   private HttpMethod _method;

   // headers for this request
   private Map<String, String> _httpHeaders = new HashMap<String, String>();

   private final HttpBody _body;

   /**
    * Constructor for the simple request to construct request with
    * disabled "no encoding" crutch.
    *
    * @param method method of request one of constants GET or POST, use constants from HttpUtils
    * @param url  base url of request
    */
   public HttpRequestDescription(HttpMethod method,
                                 String url) {
      this(method, url, null);
   }

   public HttpRequestDescription(HttpMethod method,
                                 String url,
                                 HttpBody body) {
      _method = method;
      _url = url;

      if (_url == null) throw new IllegalArgumentException("url can not be null.");
      if (_method == null) throw new IllegalArgumentException("method can not be null.");

      _body = body;
   }

   /**
    * Get list of parameters for this request.
    * This list is guaranteed to be new clean object every time.
    *
    * @return list of parameters.
    */
   public List<Pair<String, String>> parameters() {
      return new ArrayList<Pair<String, String>>(_parameters);
   }

   /**
    * Add string parameter to this request.
    *
    * @param paramName parameter name
    * @param value	 parameter value
    */
   public void add(String paramName, String value) {
      _parameters.add(Pair.create(paramName, value));
   }

   /**
    * Add long parameter to the request, converting it to string
    *
    * @param paramName parameter name
    * @param value	 parameter value
    */
   public void add(String paramName, long value) {
      _parameters.add(Pair.create(paramName, Long.toString(value)));
   }

   /**
    * Add a long parameter in case if it nonzero.
    *
    * @param paramName name of parameter
    * @param value	 value
    */
   public void addNonZero(String paramName, long value) {
      if (value != 0) {
         add(paramName, value);
      }
   }

   public void addHttpHeader(String name, String value) {
      _httpHeaders.put(name, value);
   }

   public void removeHttpHeader(String name) {
      _httpHeaders.remove(name);
   }

   public void clearHttpHeaders() {
      _httpHeaders.clear();
   }

   public Map<String, String> headers() {
      return new HashMap<String, String>(_httpHeaders);
   }

   /**
    * Get method of this request.
    *
    * @return method of this request.
    */
   public HttpMethod method() {
      return _method;
   }

   /**
    * Get base url of this request. This should not contain parameters in
    * case of get request - parameters will be added later.
    *
    * @return base url of this request.
    */
   public String url() {
      return _url;
   }

   public HttpBody body() {
      return _body;
   }

   public String toString() {
      return "(Request: method: "
         + method()
         + ", "
         + "url: "
         + "\""
         + url()
         + "\""
         + ", "
         + "parameters: " + _parameters
         + ")";
   }
}

