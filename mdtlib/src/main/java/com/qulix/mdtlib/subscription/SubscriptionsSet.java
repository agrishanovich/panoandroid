package com.qulix.mdtlib.subscription;

import com.qulix.mdtlib.subscription.interfaces.Subscription;
import com.qulix.mdtlib.subscription.interfaces.UnsubscribeAction;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionsSet {

   public <ListenerType> UnsubscribeAction subscribe(final Subscription <ListenerType> subscription,
                                                     final ListenerType listener) {
      subscription.subscribe(listener);
      final Runnable unsubscribeRun = new Runnable() {
         @Override public void run() {
            subscription.unSubscribe(listener);
         }
      };

      _unSubscribeActions.add(unsubscribeRun);
      return new UnsubscribeAction() {
         @Override
         public void run() {
            unsubscribeRun.run();
            _unSubscribeActions.remove(unsubscribeRun);
         }
      };
   }

   public void unSubscribeAll() {
      for (Runnable action : _unSubscribeActions) {
         action.run();
      }

      _unSubscribeActions.clear();
   }

   private List<Runnable> _unSubscribeActions = new ArrayList<Runnable>();
}
