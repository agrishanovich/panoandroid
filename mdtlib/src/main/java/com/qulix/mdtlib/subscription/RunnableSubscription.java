package com.qulix.mdtlib.subscription;

import com.qulix.mdtlib.subscription.BaseSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;

public final class RunnableSubscription extends BaseSubscription <Runnable> implements Runnable {

   @Override public void run() {
      run(new Action<Runnable>() {
         @Override public void doIt(Runnable r) {
            r.run();
         }
      } );
   }

   @Override public Subscription<Runnable> subscribe(Runnable listener) {
      if (_fireImmediately) {
         listener.run();
      }

      return super.subscribe(listener);
   }

   public void fireImmediately() {
      _fireImmediately = true;
      run();
   }

   private boolean _fireImmediately;
}