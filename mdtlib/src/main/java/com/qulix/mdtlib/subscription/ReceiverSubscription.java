package com.qulix.mdtlib.subscription;

import com.qulix.mdtlib.functional.Receiver;

public class ReceiverSubscription <Type> extends BaseSubscription <Receiver<Type>> implements Receiver<Type> {

   @Override public void receive(final Type data) {
      run(new Action<Receiver<Type>>() {
         @Override public void doIt(Receiver<Type> r) {
            r.receive(data);
         }
      } );
   }
}