package com.qulix.mdtlib.subscription.interfaces;

/**
 * @class Subscription
 *
 * Provides a way to create a subscription for some event.
 *
 * This unified interface is part of event handling mechanics.
 */
public interface Subscription <Listener> {

   Subscription<Listener> subscribe(Listener listener);
   Subscription<Listener> subscribeWeak(Listener listener);

   Subscription<Listener> unSubscribe(Listener listener);
}