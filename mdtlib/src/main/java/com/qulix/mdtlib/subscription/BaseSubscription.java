package com.qulix.mdtlib.subscription;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.qulix.mdtlib.subscription.interfaces.Subscription;

/**
 * This is base implementation of subscription mechanics. It represents
 * universal way to keep set of listeners and perform notification on
 * events.
 *
 * It complies for the following rule: if listener is not subscribed, it
 * will not get event.
 *
 * This leads to following cases:
 *
 * 1. If listener is _not_ subscribed when event occurs, it will not get
 * event even if it subscribed by one of other subscribers at the time of
 * notification.
 *
 * 2. If listener is subscribed when event occurs, but was unsubscribed
 * before getting notification, it will not get event.
 *
 * To be universal enough, this class allows weak and strong subscribing,
 * and separates iteration over the listeners from actual action to be
 * performed.
 *
 * For such separation interface Action is used. To perform an action on
 * all subscribers, next code can be used.
 *
 * interface Listener {
 *     void method();
 * }
 *
 * mSubscription.run(new BaseSubscription.Action<Listener>() {
 *         @Override public void doIt(final Listener listener) {
 *             listener.method();
 *         }
 *     } );
 *
 * It also safe to interrupt iterating by calling
 * Action.dontPropagateEventFurther():
 *
 * interface Listener {
 *     bool shouldNotPropagateFurther();
 * }
 *
 * mSubscription.run(new BaseSubscription.Action<Listener>() {
 *         @Override public void doIt(final Listener listener) {
 *             if (listener.shouldNotPropagateFurther()) {
 *                 dontPropagateEventFurther();
 *             }
 *         }
 *     } );
 *
 * See also ReceiverSubscription and RunnableSubscription.
 */
public class BaseSubscription <ListenerType> implements Subscription <ListenerType> {

   private static final class StopNotifyingException extends RuntimeException {
      private static final long serialVersionUID = 1L;

      public StopNotifyingException() {
         super("Stop notifying");
      }
   }

   /**
    * This is interface class used to represent action.
    */
   public static abstract class Action<ListenerType> {
      public abstract void doIt(ListenerType listener);


      /**
       * Call this method to interrupt iterating
       */
      protected final void dontPropagateEventFurther() {
         throw new StopNotifyingException();
      };

   }

   /**
    * Interface used internally to represent reference to the listener.
    */
   private interface ListenerRef<ListenerType> {
      ListenerType get();
   }


   @Override public Subscription<ListenerType> subscribe(final ListenerType listener) {
      if (listener == null) throw new IllegalArgumentException("listener can not be null");

      return addListenerReference(new ListenerRef<ListenerType>() {
         @Override public ListenerType get() {
            return listener;
         }
      } );
   }

   @Override public Subscription<ListenerType> subscribeWeak(ListenerType listener) {
      if (listener == null) throw new IllegalArgumentException("listener can not be null");

      final WeakReference<ListenerType> weakRef = new WeakReference<ListenerType>(listener);

      return addListenerReference(new ListenerRef<ListenerType>() {
         @Override public ListenerType get() {
            return weakRef.get();
         }
      });
   }

   private Subscription<ListenerType> addListenerReference(final ListenerRef<ListenerType> listener) {
      // drop expired listeners, this will prevent collecting listener
      // refs for weak subscription in case if event never fired
      removeFrom(mListeners, null);

      mListeners.add(listener);

      return this;
   }

   @Override public Subscription<ListenerType> unSubscribe(ListenerType listener) {
      if (listener == null) throw new IllegalArgumentException("listener can not be null");

      removeFrom(mListeners, listener);

      for (final List<ListenerRef<ListenerType>> listenersReceivingEventNow
              : mListenersReceivingEventsNow) {

         removeFrom(listenersReceivingEventNow, listener);
      }


      return this;
   }

   public final void run(Action<ListenerType> action) {

      final List<ListenerRef<ListenerType>> listenersToGetThisEvent = new LinkedList<ListenerRef<ListenerType>>(mListeners);

      // we need to use list here, because it possible that listener will
      // fire event again, so, to prevent destroying event delivering
      // state, we need to keep list of listeners to get event per run
      // call
      mListenersReceivingEventsNow.add(listenersToGetThisEvent);

      try {
         while(listenersToGetThisEvent.size() > 0) {

            final ListenerRef<ListenerType> deliverEventTo = listenersToGetThisEvent.get(0);
            listenersToGetThisEvent.remove(0);


            final ListenerType listener = deliverEventTo.get();

            if (listener != null) {
               // if listener is there, call it's action
               action.doIt(listener);
            } else {
               // otherwise we have an expired listner, which was subscribed
               // by the weak means and was already deleted. So we no need to
               // keep this reference anymore.
               mListeners.remove(deliverEventTo);
            }
         }
      } catch (final StopNotifyingException sne) {
         // ignored, as only purpose of this exception is to stop
         // notifying
      } finally {
         mListenersReceivingEventsNow.remove(listenersToGetThisEvent);
      }
   }

   public final boolean haveSubscribers() {
      return mListeners.size() > 0;
   }

   /**
    * Removes the specified listener from list of listener references.
    *
    * @param refsList list to remove listener from
    * @param listener listener to remove
    */
   private void removeFrom(List<ListenerRef<ListenerType>> refsList,
                           ListenerType listener) {
      if (refsList == null) return ;

      final ListIterator<ListenerRef<ListenerType>> i = refsList.listIterator();

      while (i.hasNext()) {

         final ListenerType candidate = i.next().get();

         if (listener == candidate) {
            i.remove();
         }
      }
   }

   /**
    * This list contains listeners currently subscribed.
    */
   private List<ListenerRef<ListenerType>> mListeners = new ArrayList<ListenerRef<ListenerType>>();

   /**
    * This list contains references to lists for listeners, currently
    * getting events. This is needed to correctly follow rule 2: when any
    * listener is unsusbcribed during notifying cycle, it should be
    * removed from the list of listeners to get this event.
    */
   private List<List<ListenerRef<ListenerType>>> mListenersReceivingEventsNow = new ArrayList<List<ListenerRef<ListenerType>>>();
}