package com.qulix.mdtlib.concurrency;

public interface Worker {
   public void submit(Runnable action);
}