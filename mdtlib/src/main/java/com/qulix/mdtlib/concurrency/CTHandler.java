package com.qulix.mdtlib.concurrency;

import android.os.Handler;

/**
 * @class CTHandler
 *
 * @brief Controller Thread Handler - this class is wrapper over
 * android.os.Handler class.
 *
 * The main idea is to provide isolation layer between Handler class and
 * it's role. This needed for two main things:
 *
 * 1. Avoid creating tens and hundreds of handlers in project, while only
 * one (main "controller" thread) is actually needed.
 *
 * 2. Be more async-tests friendly. Search for async tests documentation,
 * this will be described there.
 *
 * This class was created keeping potential multiprocessing in mind, so
 * every potential process should call init method at the entry of it's
 * controller thread. In case if this is one process - nothing will be
 * happen, but in case of multiple processes, handlers will be created.
 *
 * There will be no bindings, so this object need no release methods.
 */
public class CTHandler {

   private static Handler _handler;

   /**
    * Init method - should be called in every potential controller thread
    * entry point.
    */
   public static void init() {
      get();
   }

   /**
    * Get handler instance, which bound to Controller Thread (main, UI thread).
    *
    * @return instance of handler bound to main thread
    */
   private static Handler get() {
      if (_handler == null) {
         _handler = new Handler();
      }
      return _handler;
   }

   /**
    * Shortcut for get().post
    */
   public static void post(Runnable runnable) {
      get().post(runnable);
   }

   public static boolean postDelayed(java.lang.Runnable runnable, long delayMillis) {
      return get().postDelayed(runnable, delayMillis);
   }


   /**
    * <b>Testing only</b>
    *
    * This is method, which is intended to be used in tests, which involve
    * working with asyncronous code. As most async requests in project
    * returns data with handlers, we need to have possibility to intercept
    * handler.
    *
    * See documentation for async tests for details.
    *
    * @param handler handler to set as override
    */
   public static void putHandlerUseThisMethodForTestingOnly(Handler handler) {
      _handler = handler;
   }

   public static void remove(Runnable runnable) {
      get().removeCallbacks(runnable);
   }
}