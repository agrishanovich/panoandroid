package com.qulix.mdtlib.concurrency;

import com.qulix.mdtlib.app.QmdtApplication;
import com.qulix.mdtlib.app.error.FatalErrorHandler;
import com.qulix.mdtlib.operation.SimpleOperation;

public abstract class ThreadOperation extends SimpleOperation {

   protected interface Result {
      void run();
      void cleanupIfTerminated();
   }

   public ThreadOperation() {
      this(NewThreadWorker.forRandomBackgroundTasks());
   }

   public ThreadOperation(final Worker worker) {
      this(worker, QmdtApplication.instance().defaultFatalErrorHandler());
   }

   public ThreadOperation(final Worker worker,
                          final FatalErrorHandler fatalErrorHandler) {

      if (fatalErrorHandler == null) throw new IllegalArgumentException("FatalErrorHandler can not be null.");

      _worker = worker;
      _fatalErrorHandler = fatalErrorHandler;
   }

   protected final void start() {
      if (_started) return;
      _started = true;

      _worker.submit(new Runnable() {
         @Override public void run() {
            threadMethod();
         }
      } );
   }

   protected abstract void runInThread();

   private void threadMethod() {
      try {
         if (isEnded()) return;

         runInThread();

      } catch(final Throwable error) {
         postResult(new Runnable() {
            @Override public void run() {
               _fatalErrorHandler.receive(error);
            }
         } );
      }
   }

   protected final void postResult(final Runnable result) {
      if (isEnded()) return;

      CTHandler.post(new Runnable() {
         @Override public void run() {
            if (isEnded()) return;

            try {
               result.run();
            } catch (final Throwable error) {
               _fatalErrorHandler.receive(error);
            }

            try {
               terminate();
            } catch (final Throwable error) {
               _fatalErrorHandler.receive(error);
            }
         }
      } );
   }

   protected final void postResult(final Result result) {
      CTHandler.post(new Runnable() {
         @Override public void run() {
            try {
               if (isEnded()) {
                  result.cleanupIfTerminated();
               } else {
                  result.run();
               }
            } catch (final Throwable error) {
               _fatalErrorHandler.receive(error);
            }

            if (isEnded()) return;

            try {
               terminate();
            } catch (final Throwable error) {
               _fatalErrorHandler.receive(error);
            }
         }
      } );
   }



   private final Worker _worker;
   private final FatalErrorHandler _fatalErrorHandler;
   private boolean _started;
}