package com.qulix.mdtlib.concurrency;

import java.util.concurrent.ThreadFactory;

import com.qulix.mdtlib.utils.Validate;

public final class NewThreadWorker implements Worker {

   public static Worker forRandomBackgroundTasks() {
      return new NewThreadWorker(new PrioritizedThreadFactory("Random background task",
                                                              Thread.MIN_PRIORITY));
   }

   public NewThreadWorker(final ThreadFactory threadFactory) {
      Validate.argNotNull(threadFactory, "threadFactory");

      mThreadFactory = threadFactory;
   }

   @Override
   public void submit(final Runnable action) {
      mThreadFactory.newThread(action).start();
   }

   private final ThreadFactory mThreadFactory;
}