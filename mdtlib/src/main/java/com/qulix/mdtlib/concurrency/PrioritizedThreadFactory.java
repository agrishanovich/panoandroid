package com.qulix.mdtlib.concurrency;

import java.util.concurrent.ThreadFactory;

public final class PrioritizedThreadFactory implements ThreadFactory {

    public PrioritizedThreadFactory(final String name, final int priority) {
        _name = name;
        _priority = priority;
    }


    @Override public Thread newThread(Runnable r) {
        Thread t =  new Thread(r, _name);
        t.setPriority(_priority);
        return t;
    }

    private final String _name;
    private final int _priority;
}