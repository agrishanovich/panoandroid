package com.qulix.mdtlib.concurrency;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ThreadPool implements Worker {

   private static final long MS_OF_INACTIVITY_SHUTDOWN_AFTER = 5000;

   public static ThreadPool newWithBackgroundPriority(int poolSize, String name) {
      return new ThreadPool(poolSize, name, Thread.MIN_PRIORITY);
   }

   public static ThreadPool newWithNormalPriority(int poolSize, String name) {
      return new ThreadPool(poolSize, name, Thread.NORM_PRIORITY);
   }

   public ThreadPool(int poolSize,
                     String name,
                     int priority) {

      _poolSize = poolSize;
      _name = name;
      _priority = priority;
   }

   @Override public void submit(final Runnable action) {

      _currentShutdownCommand = null;

      _countOfActive += 1;

      executor().execute(new Runnable() {
            @Override public void run() {

               action.run();

               CTHandler.post(new Runnable() {
                     @Override public void run() {
                        _countOfActive -= 1;

                        checkShutdown();
                     }
                  });
            }
         });
   }

   public void terminateAll() {
      if (_executor != null) {
         _executor.shutdownNow();
         _executor = null;
      }
      _countOfActive = 0;
   }

   private void checkShutdown() {
      if (_countOfActive == 0) {

         if (_currentShutdownCommand != null) return ;

         _currentShutdownCommand = new Runnable() {
               @Override public void run() {

                  // this check will allow to perform shutdown only for
                  // runnable was not cancelled
                  if (_currentShutdownCommand != this)  return ;

                  if (_executor != null) {
                     _executor.shutdown();
                     _executor = null;
                  }
               }
            };

         CTHandler.postDelayed(_currentShutdownCommand,
                                     MS_OF_INACTIVITY_SHUTDOWN_AFTER);
      }
   }

   private ExecutorService executor() {
      if (_executor == null) {
         _executor = Executors.newFixedThreadPool(_poolSize,
                                                  new ThreadFactory(){
                                                     public Thread newThread(Runnable r) {
                                                        Thread t =  new Thread(r, _name);
                                                        t.setPriority(_priority);
                                                        return t;
                                                     }
                                                  });
      }

      return _executor;
   }

   private volatile ExecutorService _executor;

   private int _priority;
   private int _poolSize;
   private String _name;

   private int _countOfActive;

   private Runnable _currentShutdownCommand;
}