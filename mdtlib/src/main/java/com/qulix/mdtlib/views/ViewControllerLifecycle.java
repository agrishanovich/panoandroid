package com.qulix.mdtlib.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import com.qulix.mdtlib.app.ActivityResult;
import com.qulix.mdtlib.functional.Factory;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.views.interfaces.AppView;
import com.qulix.mdtlib.views.interfaces.ViewController;
import com.qulix.mdtlib.views.menu.MenuItem;
import com.qulix.mdtlib.views.traits.BackHandler;
import com.qulix.mdtlib.views.traits.MenuProvider;
import org.apache.commons.lang3.SerializationUtils;

import java.util.ArrayList;
import java.util.List;

public class ViewControllerLifecycle  {

   private static final String CONTROLLER_KEY = "written_view_controller";

   public static void startWithController(Context context,
                                          Class<?> activityClass,
                                          ViewController controller,
                                          boolean singleTop) {

      Intent intent = new Intent(context, activityClass);

      putController(intent, controller);

      boolean newTask = ! (context instanceof Activity);

      if (newTask) {
         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      }

      if (singleTop) {
         intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      }

      context.startActivity(intent);
   }



   public static void putController(Intent intent,
                                    ViewController controller) {

      intent.putExtra(CONTROLLER_KEY,
                      SerializationUtils.serialize(controller));
   }

   public ViewControllerLifecycle(Activity activity,
                                  Factory<? extends ViewController> controllerFactory,
                                  Intent intent,
                                  Bundle bundle) {

      _activity = activity;

      if (_controller == null) {
         if (bundle != null) {
            _controller = getFromBundle(bundle);
         }
      }

      if (_controller == null) {
         if (intent != null) {
            _controller = getFromIntent(intent);
         }
      }

      if (_controller == null) {
         if (controllerFactory != null) {
            _controller = controllerFactory.create();
         }
      }

      if (_controller == null) {
         throw new IllegalStateException("Controller is null, impossible to work.");
      }

      _controller.setActivity(_activity);
   }

   private ViewController getFromIntent(Intent intent) {
      try {
         return  (ViewController) SerializationUtils.deserialize(intent.getByteArrayExtra(CONTROLLER_KEY));
      } catch (RuntimeException exception) {
         exception.printStackTrace();
         return null;
      }
   }

   private ViewController getFromBundle(Bundle bundle) {
      try {
         return  (ViewController) SerializationUtils.deserialize(bundle.getByteArray(CONTROLLER_KEY));
      } catch (RuntimeException exception) {
         exception.printStackTrace();
         return null;
      }
   }

   public void handleOnStop() {
      if (_controller.isActive()) {

         _controller.onDeactivate();
      }
   }

   public void handleOnResume(Activity activity) {
      if (_controller.isActive()) {
         return ;
      }

      _controller.onActivate(_activityResult);
      _activityResult = Maybe.nothing();

      if (!_controller.isActive()) {
         return;
      }

      if (_controller instanceof MenuProvider) {
         try {
            _activity.invalidateOptionsMenu();
         } catch (java.lang.NoSuchMethodError e) {
            // suppress this error
         }
      }

      AppView appView = _controller.view();

      if (appView == null) {
         throw new IllegalStateException("App view can not be null here.");
      }

      View view = appView.view();

      if (view == null) {
         throw new IllegalStateException("View can not be null here.");
      }

      activity.setContentView(view);
   }

   public void onActivityResult(final int requestCode,
                                final int resultCode,
                                final Intent data) {
      _activityResult = Maybe.value(new ActivityResult(requestCode, resultCode, data));
   }

   public boolean onBackAction() {
      if (_controller.isActive() && _controller instanceof BackHandler) {
         return ((BackHandler)_controller).handleBack() == BackHandler.BackResult.FullyHandled;
      }
      return false;
   }

   private void addToMenu(Menu platformMenu,
                          List<MenuItem> items) {
      for (MenuItem item : items) {
         if (item.isSubmenu()) {
            android.view.SubMenu submenu = platformMenu.addSubMenu(item.text());

            if (item.image() != MenuItem.NO_IMAGE) {
               submenu.setIcon(item.image());
            }

            addToMenu(submenu,
                      item.subItems());
         } else {
            android.view.MenuItem menuItem = platformMenu.add(item.text());

            if (item.image() != MenuItem.NO_IMAGE) {
               menuItem.setIcon(item.image());
            }

            if (item.checkable()) {
               menuItem.setCheckable(true);
               menuItem.setChecked(item.checked());
            }

            final Runnable action = item.action();

            if (action != null) {
               menuItem.setOnMenuItemClickListener(new android.view.MenuItem.OnMenuItemClickListener() {
                     @Override public boolean onMenuItemClick(android.view.MenuItem item) {
                        action.run();
                        return true;
                     }
                  } );
            }

         }
      }
   }

   public boolean onCreateOptionsMenu(Menu platformMenu) {
      return onPrepareOptionsMenu(platformMenu);
   }

   public boolean onPrepareOptionsMenu(Menu platformMenu) {

      platformMenu.clear();

      if (_controller instanceof MenuProvider) {

         final List<MenuItem> items = new ArrayList<MenuItem>();

         MenuProvider.Menu menu = new MenuProvider.Menu() {
               @Override public void add(MenuItem menuItem) {
                  items.add(menuItem);
               }
            };

         boolean showMenu = ((MenuProvider)_controller).initMenu(menu);

         if ( ! showMenu) return false;
         if (items.size() == 0) return false;

         addToMenu(platformMenu,
                   items);
         return true;
      } else {
         return false;
      }
   }

   public void handleOnSaveInstanceState(Activity activity,
                                         Bundle bundle) {
      handleOnStop();

      bundle.putByteArray(CONTROLLER_KEY,
                          SerializationUtils.serialize(_controller));
   }

   public ViewController controller() {
      return _controller;
   }

   private ViewController _controller;

   private Activity _activity;
   private Maybe<ActivityResult> _activityResult = Maybe.nothing();
}