package com.qulix.mdtlib.views.multiview;


import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.views.interfaces.AppView;

import android.view.View;

/**
 * @class MultiViewAppView
 */
public interface MultiViewAppView extends AppView {
	Operation switchTo(View appView);
}