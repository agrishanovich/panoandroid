package com.qulix.mdtlib.views.multiview.switchers;

import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.SimpleOperation;
import android.view.View;
import android.view.ViewGroup;

public final class DefaultViewSwitcher implements ViewSwitcher {

   private ViewGroup _navigationFrame;

   public DefaultViewSwitcher(ViewGroup mainNavFrame) {
      _navigationFrame = mainNavFrame;
   }

   public Operation doSwitching(View to) {
      // if no target - no switching
      if (to == null) throw new IllegalArgumentException("Target view is null!");

      _navigationFrame.removeAllViews();

      _navigationFrame.addView(to);

      return new SimpleOperation() {
            {
               // fire ended right in constructor to make it be completed.
               terminate();
            }
         };
   }
}