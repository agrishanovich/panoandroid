package com.qulix.mdtlib.views;

import android.app.Activity;
import com.qulix.mdtlib.app.ActivityResult;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.views.interfaces.AppView;
import com.qulix.mdtlib.views.interfaces.ViewController;
import com.qulix.mdtlib.views.traits.BackHandler;
import com.qulix.mdtlib.views.traits.MenuProvider;

@SuppressWarnings("serial")
public class ViewControllerWrapper implements ViewController,
                                              BackHandler,
                                              MenuProvider {
   public ViewControllerWrapper(ViewController controller) {
      _slave = controller;
   }

   @Override public BackResult handleBack() {
      if (_slave instanceof BackHandler) return ((BackHandler)_slave).handleBack();
      else return BackResult.ContinueHandling;
   }

   @Override public boolean initMenu(Menu menu) {
      if (_slave instanceof MenuProvider) {
         return ((MenuProvider)_slave).initMenu(menu);
      }

      return false;
   }

   /**
    * This method will be called before starting to showing this
    * view. This allows view to fully configure itself.
    * @param activityResult
    */
   @Override public void onActivate(Maybe<ActivityResult> activityResult) {
      _slave.onActivate(activityResult);
   }

   /**
    * This method will be called after view is gone. Use this method for
    * cleaning up resources etc. After calling this method object can be
    * freed at any time.
    */
   @Override public void onDeactivate() {
      _slave.onDeactivate();
   }

   /**
    * Get controller state (activated vs deactivated)
    *
    * @return whether the controller is active
    */
   @Override public boolean isActive() {
      return _slave.isActive();
   }

   /**
    * Get the view from the controller which will be displayed.
    *
    * @return
    */
   @Override public AppView view() {
      return _slave.view();
   }

   @Override public void setActivity(Activity activity) {
      _slave.setActivity(activity);
   }

   @Override public Activity activity() {
      return _slave.activity();
   }

   private ViewController _slave;
}