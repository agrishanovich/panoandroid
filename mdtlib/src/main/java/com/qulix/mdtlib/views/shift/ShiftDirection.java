package com.qulix.mdtlib.views.shift;

public enum ShiftDirection {
   Up,
   Down,
   Right,
   Left
}