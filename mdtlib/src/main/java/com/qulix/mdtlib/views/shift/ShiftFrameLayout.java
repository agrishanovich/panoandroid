package com.qulix.mdtlib.views.shift;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;

public class ShiftFrameLayout extends FrameLayout {

   public ShiftFrameLayout(Context context) {
      super(context);
   }

   public ShiftFrameLayout(Context context, AttributeSet attrs) {
      this(context, attrs, 0);
   }

   public ShiftFrameLayout(Context context, AttributeSet attrs, int defStyle) {
      super(context, attrs, defStyle);
   }

   public void shift(ShiftDirection direction, int shift, boolean withAnimation) {
      shift(direction, shift, withAnimation, ANIMATION_DURATION);
   }

   public void shift(ShiftDirection direction, int shift, boolean withAnimation, int duration) {
      _direction = direction;

      if (withAnimation) {
         startAnimation(createShiftAnimation(shift, duration));
      } else {
         updateView(shift);
      }
   }

   public void setShiftListener(ShiftListener shiftListener) {
      _shiftListener = shiftListener;
   }

   public void updateView(int shift) {
      _shift = shift;
      requestLayout();
   }

   private Animation createShiftAnimation(int shift, int duration) {
      ShiftAnimation shiftAnimation = new ShiftAnimation(this, _shift, shift, duration);

      shiftAnimation.setAnimationListener(new Animation.AnimationListener() {
         @Override
         public void onAnimationStart(Animation animation) {
         }

         @Override
         public void onAnimationEnd(Animation animation) {
            if (_shiftListener != null) {
               _shiftListener.onShiftEnded();
            }
         }

         @Override
         public void onAnimationRepeat(Animation animation) {
         }
      });

      return shiftAnimation;
   }

   @Override
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
      final int count = getChildCount();

      for (int i = 0; i < count; i++) {
         final View child = getChildAt(i);
         if (child.getVisibility() != GONE) {
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
         }
      }

      this.setMeasuredDimension(resolveSize(MeasureSpec.getSize(widthMeasureSpec), widthMeasureSpec),
              resolveSize(MeasureSpec.getSize(heightMeasureSpec), heightMeasureSpec));
   }


   @Override
   protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
      final int count = getChildCount();

      for (int i = 0; i < count; i++) {
         final View child = getChildAt(i);
         if (child.getVisibility() != GONE) {

            int horizontalShift = 0;
            int verticalShift = 0;

            if (_direction != null) {
               switch (_direction) {
                  case Up:
                     verticalShift = -_shift;
                     break;
                  case Down:
                     verticalShift = _shift;
                     break;
                  case Left:
                     horizontalShift = -_shift;
                     break;
                  case Right:
                     horizontalShift = _shift;
                     break;
                  default:
                     // Do nothing
               }
            }

            child.layout(left + horizontalShift,
                    top + verticalShift,
                    right + horizontalShift,
                    bottom + verticalShift);
         }
      }
   }

   private static final int ANIMATION_DURATION = 400;

   private ShiftDirection _direction;
   private int _shift;

   private ShiftListener _shiftListener;
}

