package com.qulix.mdtlib.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import com.qulix.mdtlib.functional.Factory;
import com.qulix.mdtlib.subscription.RunnableSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;
import com.qulix.mdtlib.views.interfaces.ViewController;

/**
 * To use this activity, please add this to the AndroidManifest.xml:
 <activity android:name="com.qulix.mdtlib.views.ViewControllerActivity"
 android:label="@string/app_name"/>
 * in the application section.
 */


public class ViewControllerActivity extends Activity {

   /** Called when the activity is first created. */

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      _controllerLifecycle
         = new ViewControllerLifecycle(this,
                                       viewControllerFactory(),
                                       getIntent(),
                                       savedInstanceState);
   }

   /**
    * Override this method if activity is intended to display some specific controller.
    */
   public Factory<ViewController> viewControllerFactory() {
      return null;
   }

   @Override public void onResume() {
      super.onResume();

      _controllerLifecycle.handleOnResume(this);
      _onResumeSubscription.run();
   }

   @Override
   protected void onPause() {
      super.onPause();

      _onPauseSubscription.run();
   }

   public Subscription<Runnable> onPauseSubscription() {
      return _onPauseSubscription;
   }

   public Subscription<Runnable> onResumeSubscription() {
      return _onResumeSubscription;
   }

   @Override public void onStop() {
      super.onStop();

      _controllerLifecycle.handleOnStop();
   }

   @Override public boolean onCreateOptionsMenu(Menu menu) {
      return _controllerLifecycle.onPrepareOptionsMenu(menu);
   }


   @Override public boolean onPrepareOptionsMenu(Menu menu) {
      return _controllerLifecycle.onPrepareOptionsMenu(menu);
   }

   @Override public void onSaveInstanceState(Bundle bundle) {
      super.onSaveInstanceState(bundle);

      _controllerLifecycle.handleOnSaveInstanceState(this,
                                                    bundle);
   }

   @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      _controllerLifecycle.onActivityResult(requestCode, resultCode, data);
   }

   @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
      if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
         event.startTracking();
         return true;
      }
      return super.onKeyDown(keyCode, event);
   }

   @Override
   public boolean onKeyUp(int keyCode, KeyEvent event) {
      if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking() && !event.isCanceled()) {
         if (_controllerLifecycle.onBackAction()) {
            return true;
         }
      }
      return super.onKeyUp(keyCode, event);
   }

   @Override public void onBackPressed() {
      /*
         If controller not handled back action, super.onBackPressed() must be called
         in order to perform default action (finish activity) for API levels >= 5
       */
      super.onBackPressed();
   }

   private ViewControllerLifecycle _controllerLifecycle;
   private RunnableSubscription _onPauseSubscription = new RunnableSubscription();
   private RunnableSubscription _onResumeSubscription = new RunnableSubscription();
}