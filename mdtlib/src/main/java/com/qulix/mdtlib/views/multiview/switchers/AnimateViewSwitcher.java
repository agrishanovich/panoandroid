package com.qulix.mdtlib.views.multiview.switchers;

import com.qulix.mdtlib.operation.FakeOperation;
import com.qulix.mdtlib.operation.SimpleOperation;
import com.qulix.mdtlib.operation.Operation;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.view.animation.LinearInterpolator;
import android.view.animation.Animation;
import android.view.animation.AlphaAnimation;
import android.widget.ViewSwitcher;

public class AnimateViewSwitcher
   implements com.qulix.mdtlib.views.multiview.switchers.ViewSwitcher {

   public static AnimateViewSwitcher fadeSwitcher(ViewSwitcher mainNavFrame) {
      return new AnimateViewSwitcher(mainNavFrame,
                                     new AlphaAnimation(0, 1),
                                     new AlphaAnimation(1, 0));
   }

   public AnimateViewSwitcher(ViewSwitcher mainNavFrame, Animation inAnim, Animation outAnim) {
      _navigationFrame = mainNavFrame;

      _fromFrame = new FrameLayout(mainNavFrame.getContext());
      _toFrame = new FrameLayout(mainNavFrame.getContext());

      _navigationFrame.addView(_toFrame);
      _navigationFrame.addView(_fromFrame);

      initAnimation(inAnim);
      initAnimation(outAnim);

      _inAnimation = inAnim;
      _outAnimation = outAnim;

      outAnim.setAnimationListener(new Animation.AnimationListener() {
         @Override
         public void onAnimationEnd(Animation animation) {
            if (_switchOperation != null) {
               _switchOperation.terminate();
            }

            // cleanup as fast as switching complete.
            _fromFrame.removeAllViews();
         }

         @Override
         public void onAnimationRepeat(Animation animation) {
               /* nothing to do */
         }

         @Override
         public void onAnimationStart(Animation animation) {
               /* nothing to do */
         }
      });
   }

   public Operation doSwitching(View to) {
      // if no target - no switching
      if (to == null) throw new IllegalArgumentException("No target to switching");

      if (_switchOperation != null) throw new IllegalStateException("Already in switching state!");

      switchFrames();

      _toFrame.removeAllViews();
      _toFrame.addView(to);

      _navigationFrame.showNext();

      if (_isFirstSwitching) {
         _isFirstSwitching = false;

         _navigationFrame.setInAnimation(_inAnimation);
         _navigationFrame.setOutAnimation(_outAnimation);

         _switchOperation = new FakeOperation(0, new Runnable() {
            @Override
            public void run() {
               _switchOperation = null;
            }
         });
      } else {
         _switchOperation = new SimpleOperation();
         _switchOperation.endedEvent().subscribe(new Runnable() {
            @Override
            public void run() {
               _switchOperation = null;
            }
         });
      }

      return _switchOperation;
   }

   private void switchFrames() {
      ViewGroup temp = _toFrame;
      _toFrame = _fromFrame;
      _fromFrame = temp;
   }

   private void initAnimation(Animation animation) {
      animation.setDuration(400);
      animation.setInterpolator(new LinearInterpolator());
      animation.setRepeatCount(0);
      animation.setRepeatMode(Animation.RESTART);
   }

   private boolean _isFirstSwitching = true;

   private ViewGroup _fromFrame;
   private ViewGroup _toFrame;

   private Animation _inAnimation;
   private Animation _outAnimation;

   private ViewSwitcher _navigationFrame;

   private SimpleOperation _switchOperation;
}