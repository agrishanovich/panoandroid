package com.qulix.mdtlib.views;

import android.app.Activity;
import com.qulix.mdtlib.app.ActivityResult;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.OperationTracker;
import com.qulix.mdtlib.operation.PendingOperationsSet;
import com.qulix.mdtlib.subscription.SubscriptionsSet;
import com.qulix.mdtlib.subscription.interfaces.Subscription;
import com.qulix.mdtlib.subscription.interfaces.UnsubscribeAction;
import com.qulix.mdtlib.views.interfaces.*;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class BasicViewController<ViewType extends AppView> implements ViewController,
                                                                               ActivityHolder {


   /**
    * Override this in child to spawn child on view open.
    *
    * Please, note that view not
    *
    * @return new AppView instance.
    */
   protected abstract ViewType spawnView();

   /**
    * This method will be called before starting to showing this
    * view. This allows view to fully configure itself.
    * @param activityResult
    */
   @SuppressWarnings("unchecked")
   @Override public void onActivate(Maybe<ActivityResult> activityResult) {

      if (_active) {
         throw new IllegalStateException("Activity was already set - activating already active controller.");
      }

      if (_activity == null) {
         throw new IllegalStateException("Activity not set - can not perform activate");
      }

      _active = true;

      _view = spawnView();

      if (_view instanceof StatefulView
          && _viewState != null) {


         ((StatefulView<Serializable>)_view).recallState(_viewState);
      }
   }

   /**
    * This method will be called after view is gone. Use this method for
    * cleaning up resources etc. After calling this method object can be
    * freed at any time.
    */
   @Override public void onDeactivate() {

      _active = false;

      if (_view instanceof StatefulView<?>) {
         _viewState = ((StatefulView<?>)_view).dumpState();
      }

      subscriptionsSet().unSubscribeAll();

      operationsInProgress().terminateAll();

      if (! operationsInProgress().isEmpty()) throw new RuntimeException("Operations in progress is not empty after cleanup "
                                                                        + "this is likely because termination handler of "
                                                                        + "some operation started new operation");

      _view = null;
   }

   @Override public final boolean isActive() {
      return _active;
   }

   /**
    * Get the view from the controller which will be displayed.
    *
    * @return
    */
   @Override public final ViewType view() {
      return _view;
   }

   @Override public final void setActivity(Activity activity) {
      _activity = activity;
   }

   @Override public final Activity activity() {
      return _activity;
   }

   /**
    * This method is needed to bind subscription lifecycle to the controller lifecycle
    */
   protected final <ListenerType> UnsubscribeAction keepSubscribedWhileActive(final Subscription<ListenerType> subscription,
                                                           final ListenerType listener) {

      if (!isActive()) throw new IllegalStateException("Don't start operation on inactive controller");

      return subscriptionsSet().subscribe(subscription, listener);
   }

   protected final Operation terminateOnDeactivate(final Operation operation) {

      if (!isActive()) throw new IllegalStateException("Don't start operation on inactive controller");

      return operationsInProgress().registerForTermination(operation);
   }

   protected final <T extends Operation> Maybe<T> terminateOnDeactivate(final Maybe<T> maybeOperation) {
      if (maybeOperation.isValue()) {
         terminateOnDeactivate(maybeOperation.value());
      }

      return maybeOperation;
   }

   protected final PendingOperationsSet terminateOnDeactivate(final PendingOperationsSet opSet) {
      if (!isActive()) throw new IllegalStateException("Don't start operation on inactive controller");

      return operationsInProgress().registerForTermination(opSet);
   }


   protected final OperationTracker operationTracker() {
      return operationsInProgress();
   }

   protected static boolean isSameAs(Object one, Object two) {
      if (one == two) return true;
      else if (one == null) return false;
      else return one.equals(two);
   }

   private ViewType _view;

   private SubscriptionsSet subscriptionsSet() {
      if (_subscriptionsSet == null) {
         _subscriptionsSet = new SubscriptionsSet();
      }
      return _subscriptionsSet;
   }

   transient private SubscriptionsSet _subscriptionsSet;

   private PendingOperationsSet operationsInProgress() {

      if (_operationsInProgress == null) {
         _operationsInProgress = new PendingOperationsSet();
      }

      return _operationsInProgress;
   }

   transient private PendingOperationsSet _operationsInProgress;

   transient private Activity _activity;

   private Serializable _viewState;

   private transient boolean _active;
}