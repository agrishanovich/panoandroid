package com.qulix.mdtlib.views.menu;
import java.util.List;


public class MenuItem {

   public static final int NO_IMAGE = 0;

   private enum Checked {
      Checked,
      Unchecked,
      NotCheckable
   }

   public MenuItem(String text, int image, Runnable action) {
      this(text, image, action, null);
   }

   public MenuItem(String text, int image, List<MenuItem> subItems) {
      this(text, image, null, subItems);
   }

   private MenuItem(String text,
                    int image,
                    Runnable action,
                    List<MenuItem> subItems) {
      _checked = Checked.NotCheckable;

      _image = image;
      _text = text;
      _action = action;
      _subItems = subItems;
   }

   public int image() {
      return _image;
   }

   public String text() {
      return _text;
   }

   public boolean isSubmenu(){
      return _subItems != null;
   }

   public Runnable action() {
      return _action;
   }

   public List<MenuItem> subItems() {
      return _subItems;
   }

   public MenuItem setChecked(boolean status) {
      if (status) _checked = Checked.Checked;
      else _checked = Checked.Unchecked;

      return this;
   }

   public boolean checkable() {
      return _checked != Checked.NotCheckable;
   }

   public boolean checked() {
      if (_checked == Checked.NotCheckable) throw new Error("Uncheckable item.");

      return _checked == Checked.Checked;
   }

   private Checked _checked;

   private int _image;
   private String _text;
   private Runnable _action;
   private List<MenuItem> _subItems;
}