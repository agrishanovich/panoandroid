package com.qulix.mdtlib.views.interfaces;

import android.app.Activity;

public interface ActivityHolder {
   Activity activity();
}