package com.qulix.mdtlib.views.interfaces;

import android.app.Activity;
import com.qulix.mdtlib.app.ActivityResult;
import com.qulix.mdtlib.functional.Maybe;

import java.io.Serializable;

/**
 * @class ViewController
 *
 * @brief View controller. This incapsulates "state" and "activities" of
 * view from point of view of view.
 *
 * Interaction is built next way:
 *
 * View controller is pull-data-provider for the view and notifies it when
 * view should be changed.
 *
 * The default pattern is to provide from requested view interface for
 * controller view expects and to implement this interface in the
 * controller.
 *
 * Between calls to onDeactivate and onActivate controller must be in
 * serialize-safe state.
 */
public interface ViewController extends Serializable, ActivityHolder {

   /**
    * This method will be called before starting to showing this
    * view. This allows view to fully configure itself.
    * @param activityResult
    */
   void onActivate(Maybe<ActivityResult> activityResult);

   /**
    * This method will be called after view is gone. Use this method for
    * cleaning up resources etc. After calling this method object can be
    * freed at any time.
    */
   void onDeactivate();

   /**
    * Get controller state (activated vs deactivated)
    *
    * @return whether the controller is active
    */
   boolean isActive();

   /**
    * Get the view from the controller which will be displayed.
    *
    * @return controller view
    */
   AppView view();

   void setActivity(Activity activity);
   Activity activity();
}
