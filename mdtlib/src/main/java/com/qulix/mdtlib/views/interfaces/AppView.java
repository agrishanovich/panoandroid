package com.qulix.mdtlib.views.interfaces;

import android.view.View;

public interface AppView {

   /**
    * This method needed to access "plain view" of view. This is view
    * which will be add to the view hierarchy when switching to this AppView.
    *
    * @return view object.
    */
   View view();
}