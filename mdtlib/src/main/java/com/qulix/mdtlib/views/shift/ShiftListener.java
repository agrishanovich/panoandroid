package com.qulix.mdtlib.views.shift;

public interface ShiftListener {

   void onShiftEnded();
}
