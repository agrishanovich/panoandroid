package com.qulix.mdtlib.views.multiview;

import com.qulix.mdtlib.app.ActivityResult;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.views.BasicViewController;
import com.qulix.mdtlib.views.interfaces.ViewController;

import java.io.Serializable;


/**
 * @class MultiViewContoller
 *
 * @brief This class is controller part of controller-view basic
 * implementations of the multiview views.
 *
 * This is base of mechanisms needed to implement multiview view, like view
 * with embedded tabs and so on. This mechanics operates on standard view
 * controllers, making it easy to embed implemented as pair controller-view
 * views into tabs.
 */

@SuppressWarnings("serial")
public abstract class MultiViewContoller<ViewType extends MultiViewAppView, ComparableViewKey extends Serializable > extends BasicViewController<ViewType> {

   public MultiViewContoller(ComparableViewKey initialTarget) {
	  _targetKey = initialTarget;
   }

   /**
    * This method must be overriden by the derived class to provide
    * controller by key
    */
   protected abstract ViewController viewController(ComparableViewKey key);

   private transient boolean _switchingNow;

   private final void performSwitch(final Runnable onSwitchCompleted, Maybe<ActivityResult> activityResult) {

	  if (_targetKey.equals(_currentKey)) {
		 _targetKey = null;
		 return;
	  }

	  if (_switchingNow) throw new IllegalStateException("This class now not supports switching while switching, this is to do.");
	  _switchingNow = true;


      ViewController target = viewController(_targetKey);
	  final ViewController current = current();

      if (target == null) throw new IllegalStateException("MultiViewContoller: target controller can not be null");



      target.setActivity(activity());
      target.onActivate(activityResult);

      // call switch method
      terminateOnDeactivate(view().switchTo(target.view().view()))
         .endedEvent().subscribe(new Runnable() {
               @Override public void run() {
				  _switchingNow = false;

				  if (current != null) {
					 current.onDeactivate();
				  }

				  _currentKey = _targetKey;
				  _targetKey = null;

				  if (onSwitchCompleted != null) {
					 onSwitchCompleted.run();
				  }
               }
            });
   }

   /**
    * This is implementation of method to prepare view controller to be
    * shown.
    * @param activityResult
    */
   @Override public void onActivate(Maybe<ActivityResult> activityResult) {
      // make BasicViewController create view.
      super.onActivate(activityResult);

	  if (_currentKey != null) {
		 _targetKey = _currentKey;
		 _currentKey = null;
	  }

      performSwitch(null, activityResult);
   }

   protected final void switchTo(ComparableViewKey targetKey) {
	  switchTo(targetKey, null);
   }

   protected final void switchTo(ComparableViewKey targetKey, Runnable onSwitchCompleted) {
	  _targetKey = targetKey;
	  performSwitch(onSwitchCompleted, Maybe.<ActivityResult>nothing());
   }

   /**
    * onDeactivate method will pass onDeactivate event to the current controller.
    */
   @Override public void onDeactivate() {
	  ViewController current = current();
      if (current == null) return ;

      current.onDeactivate();

      super.onDeactivate();
   }

   private ViewController current() {
	  if (_currentKey != null) {
		 return viewController(_currentKey);
	  } else {
		 return null;
	  }
   }

   /**
    * Currently selected controller
    */
   private ComparableViewKey _targetKey;
   private ComparableViewKey _currentKey;
}