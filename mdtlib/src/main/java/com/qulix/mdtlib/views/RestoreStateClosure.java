package com.qulix.mdtlib.views;
import java.io.Serializable;

public interface RestoreStateClosure <ViewType> extends Serializable {
   void recall(ViewType view);
}