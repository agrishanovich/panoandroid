package com.qulix.mdtlib.views.shift;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class ShiftAnimation extends Animation {

   public ShiftAnimation(ShiftFrameLayout view, int startShift, int targetShift, int duration) {
      _view = view;

      _startShift = startShift;
      _targetShift = targetShift;

      _isIncreasing = (targetShift - startShift) > 0;

      setDuration(duration);
   }

   @Override
   protected void applyTransformation(float interpolatedTime, Transformation transformation) {
      if (_isIncreasing) {
         _currentShift = _startShift + (int) ((_targetShift - _startShift) * interpolatedTime);
      } else {
         _currentShift = _targetShift + (int) ((_startShift - _targetShift) * (1 - interpolatedTime));
      }

      _view.updateView(_currentShift);
   }

   @Override
   public boolean willChangeBounds() {
      return true;
   }

   private ShiftFrameLayout _view;
   private int _currentShift;
   private int _startShift;
   private int _targetShift;
   private boolean _isIncreasing;
}


