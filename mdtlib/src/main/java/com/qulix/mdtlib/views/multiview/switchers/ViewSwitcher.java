package com.qulix.mdtlib.views.multiview.switchers;


import com.qulix.mdtlib.operation.Operation;
import android.view.View;

public interface ViewSwitcher {

   /**
    * This method is needed to start performing switch.
    *
    * @param to target view.
    * @param whenDone this runnable will be runned when switching
    * is done. Can be null.
    */
   Operation doSwitching(View to);
}