package com.qulix.mdtlib.views.interfaces;

import java.io.Serializable;

public interface StatefulView <StateType extends Serializable> {

   StateType dumpState();

   void recallState(StateType state);
}