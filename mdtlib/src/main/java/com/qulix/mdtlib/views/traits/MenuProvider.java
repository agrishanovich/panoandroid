package com.qulix.mdtlib.views.traits;

import com.qulix.mdtlib.views.menu.MenuItem;

public interface MenuProvider {

   interface Menu {
      void add(MenuItem item);
   }


   boolean initMenu(Menu menu);
}