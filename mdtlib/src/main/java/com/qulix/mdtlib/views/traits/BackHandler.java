package com.qulix.mdtlib.views.traits;

public interface BackHandler {
   public enum BackResult {
      FullyHandled,
      ContinueHandling
   }

   BackResult handleBack();
}