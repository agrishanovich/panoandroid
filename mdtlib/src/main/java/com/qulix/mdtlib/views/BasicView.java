package com.qulix.mdtlib.views;

import android.view.View;
import com.qulix.mdtlib.utils.LayoutUtils;
import com.qulix.mdtlib.views.interfaces.AppView;
import com.qulix.mdtlib.views.interfaces.ViewController;

public class BasicView <ControllerType extends ViewController> implements AppView {

   protected BasicView(ControllerType controller, int layoutId) {
      _controller = controller;

      _view = LayoutUtils.inflateLayout(_controller.activity(),
                                        layoutId);
   }

   protected final ControllerType controller() {
      return _controller;
   }

   @Override public View view() {
      return _view;
   }

   protected View viewById(final int id) {
      return view().findViewById(id);
   }

   private ControllerType _controller;

   private View _view;
}