package com.qulix.mdtlib.debug.logging;

import android.util.Log;


/**
 * @class LogLine
 *
 * @brief Log line is log line for some log information.
 *
 * This is convinience class to make it possibly to easy add log lines to
 * the Logging class.
 */
public class LogcatLogLine implements LogLine {

   /**
    * Tag.
    */
   private String _tag;

   /**
    * Log level.
    */
   private LogLevel _level;

   /**
    * Construct log line with tag and level of work. Depending on level of
    * loggin different method calls will be ignored.
    *
    * |---------+--------+---------+--------+---------+-------|
    * |         | SILENT | MINIMAL | NORMAL | VERBOSE | EXTRA |
    * |---------+--------+---------+--------+---------+-------|
    * | fail    | -      | +       | +      | +       | +     |
    * | info    | -      | -       | +      | +       | +     |
    * | details | -      | -       | -      | +       | +     |
    * | extra   | -      | -       | -      | -       | +     |
    * |---------+--------+---------+--------+---------+-------|
    *
    * @param tag tag to use with android logger
    * @param level log level to configure this object
    */
   public LogcatLogLine(String tag, LogLevel level) {
      _level = level;
      _tag = tag;
   }

   /**
    * Log fail message. Will log in case if level > SILENT
    *
    * @param objects variable parameters which will be logged.
    */
   public void fail(Object ... objects) {
      if (_level != LogLevel.SILENT) {
         Log.w(_tag, "Failing: " + logMessage(objects));
      }
   }


   /**
    * Log info message. Will log in case if level > MINIMAL
    *
    * @param objects variable parameters which will be logged.
    */
   public void info(Object ... objects) {
      if (_level.isGreaterThan(LogLevel.MINIMAL)) {
         Log.d(_tag, logMessage(objects));
      }
   }

   /**
    * Log details message. Will log in case if level > NORMAL
    *
    * @param objects variable parameters which will be logged.
    */
   public void details(Object ... objects) {
      if (_level.isGreaterThan(LogLevel.NORMAL)) {
         Log.i(_tag, logMessage(objects));
      }
   }

   /**
    * Log extra details message. Will log in case if level > VERBOSE
    *
    * @param objects variable parameters which will be logged.
    */
   public void extra(Object ... objects) {
      if (_level.isGreaterThan(LogLevel.VERBOSE)) {
         Log.i(_tag, logMessage(objects));
      }
   }

   /**
    * Internal method to construct message from the array of objects.
    *
    * @param objects objects to construct message
    *
    * @return string with concatenated objects.
    */
   private static String logMessage(Object ... objects) {
      StringBuilder builder = new StringBuilder();

      for (Object o : objects) {
         builder.append(o);
      }

      return builder.toString();
   }
}