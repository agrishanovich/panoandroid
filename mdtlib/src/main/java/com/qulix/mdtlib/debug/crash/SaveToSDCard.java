package com.qulix.mdtlib.debug.crash;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class SaveToSDCard implements CrashLogDump.OutputMethod {

   public SaveToSDCard(String prefix) {
      _fileNameFormat = prefix + NAME_FORMAT_POSTFIX;
   }

   private String getMemoryCardPath() {
      File externalStorageDirectory = Environment.getExternalStorageDirectory();
      if (externalStorageDirectory == null) {
         return "/";
      }
      return externalStorageDirectory.getAbsolutePath() + "/";
   }

   private String formatFileName() {
      Date currentDate = Calendar.getInstance().getTime();

      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);

      String date = dateFormat.format(currentDate);

      return String.format(_fileNameFormat, date);
   }

   @Override public void write(String data) {
      OutputStream out = null;

      try {
         if (_fileNameFormat == null) {
            return ;
         }

         String fullFilePath = getMemoryCardPath() + formatFileName();

         out = new FileOutputStream(fullFilePath);

         out.write(data.getBytes("UTF-8"));

      } catch(Throwable e) {
         // This is very bad!
      } finally {
         if (out != null) {
            try {
               out.close();
            } catch(IOException e) {
               // ignore
            }

         }
      }
   }

   private static final String DATE_FORMAT_STRING = "MM.dd.yyyy-k.m.s";
   private static final String NAME_FORMAT_POSTFIX = "-%s.crash.info";

   private String _fileNameFormat;

}
