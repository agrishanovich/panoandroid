package com.qulix.mdtlib.debug.crash;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public final class CrashLogDump {

   public interface OutputMethod {
      void write(String data);
   }

   public CrashLogDump(OutputMethod outputMethod) {
      _outputMethod = outputMethod;

      enableExceptionLogging();
   }

   public void enableExceptionLogging() {

      if (_defaultHandler == null) {

         _defaultHandler = Thread.getDefaultUncaughtExceptionHandler();

         Thread.UncaughtExceptionHandler crashLogHandler = createExceptionHandler(_defaultHandler);

         Thread.setDefaultUncaughtExceptionHandler(crashLogHandler);
      }
   }

   public void disableExceptionLogging() {
      if (_defaultHandler != null) {

         Thread.setDefaultUncaughtExceptionHandler(_defaultHandler);

         _defaultHandler = null;
      }
   }

   private Thread.UncaughtExceptionHandler createExceptionHandler(final Thread.UncaughtExceptionHandler slaveHandler) {
      return new Thread.UncaughtExceptionHandler(){
         public void uncaughtException(Thread t, Throwable e) {
            //do some action like writing to file or upload somewhere

            if (_outputMethod != null) {
               _outputMethod.write(formatCrashLogInfo(t, e));
            }

            //call original handler
            slaveHandler.uncaughtException(t, e);
         }
      };
   }


   private static String formatCrashLogInfo(Thread t, Throwable e) {
      final Writer result = new StringWriter();
      final PrintWriter printWriter = new PrintWriter(result);

      e.printStackTrace(printWriter);

      final String stacktrace = result.toString();

      printWriter.close();

      return stacktrace;
   }


   private OutputMethod _outputMethod = null;
   private Thread.UncaughtExceptionHandler _defaultHandler = null;
}
