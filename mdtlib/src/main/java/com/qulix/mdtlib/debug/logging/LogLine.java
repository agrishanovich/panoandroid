package com.qulix.mdtlib.debug.logging;

public interface LogLine {
   void fail(Object ... objects);
   void info(Object ... objects);
   void details(Object ... objects);
   void extra(Object ... objects);
}