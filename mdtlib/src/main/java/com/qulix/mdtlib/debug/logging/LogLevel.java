package com.qulix.mdtlib.debug.logging;

public enum LogLevel {

   /**
    * Silent log level. Configured to be silent, log line will not log.
    */
   SILENT,

   /**
    * Minimal - log only fails.
    */
   MINIMAL,

   /**
    * Normal - log fails and some information.
    */
   NORMAL,

   /**
    * Verbose - log additional details.
    */
   VERBOSE,

   /**
    * Extra verbose - log additional details for additional details.
    */
   EXTRA;

   public boolean isGreaterThan(LogLevel level) {
      return ordinal() > level.ordinal();
   }
}