package com.qulix.mdtlib.debug.logging;
import java.util.Arrays;
import java.util.List;

public class ComboLogLine implements LogLine {

   public ComboLogLine(LogLine ... loggers) {
      _loggers = Arrays.asList(loggers);
   }


   @Override public void fail(Object ... objects) {
      for (LogLine line : _loggers) {
         if (line != null) {
            line.fail(objects);
         }
      }
   }

   @Override public void info(Object ... objects) {
      for (LogLine line : _loggers) {
         if (line != null) {
            line.info(objects);
         }
      }
   }

   @Override public void details(Object ... objects) {
      for (LogLine line : _loggers) {
         if (line != null) {
            line.details(objects);
         }
      }
   }

   @Override public void extra(Object ... objects) {
      for (LogLine line : _loggers) {
         if (line != null) {
            line.extra(objects);
         }
      }
   }


   private List<LogLine> _loggers;
}