package com.qulix.mdtlib.debug.logging;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Environment;
import android.util.Log;


/**
 * @class LogLine
 *
 * @brief Log line is log line for some log information.
 *
 * This is convinience class to make it possibly to easy add log lines to
 * the Logging class.
 */
public class SDCardFileLogLine implements LogLine {

   private FileOutputStream _output;

   /**
    * Log level.
    */
   private LogLevel _level;

   /**
    * Construct log line with tag and level of work. Depending on level of
    * loggin different method calls will be ignored.
    *
    * |---------+--------+---------+--------+---------+-------|
    * |         | SILENT | MINIMAL | NORMAL | VERBOSE | EXTRA |
    * |---------+--------+---------+--------+---------+-------|
    * | fail    | -      | +       | +      | +       | +     |
    * | info    | -      | -       | +      | +       | +     |
    * | details | -      | -       | -      | +       | +     |
    * | extra   | -      | -       | -      | -       | +     |
    * |---------+--------+---------+--------+---------+-------|
    *
    * @param tag tag to use with android logger
    * @param level log level to configure this object
    */
   public SDCardFileLogLine(String filePrefix,
                            LogLevel level) {
      File externalStorageDirectory = Environment.getExternalStorageDirectory();
      if (externalStorageDirectory == null) {
         externalStorageDirectory = new File("/");
      }

      DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);

      String dateString = dateFormat.format(new Date());

      String filePath =
         externalStorageDirectory.getAbsolutePath()
         + "/"
         + String.format(filePrefix + NAME_FORMAT_POSTFIX, dateString);

      try {
         _output = new FileOutputStream(filePath);
      } catch(IOException e) {
         Log.d("SDCardFileLogLine", "Failed open file: " + filePath + " reason: " + e);
      }

      _level = level;
   }

   /**
    * Log fail message. Will log in case if level > SILENT
    *
    * @param objects variable parameters which will be logged.
    */
   public void fail(Object ... objects) {
      if (_level != LogLevel.SILENT) {
         writeLine("Failing: " + logMessage(objects));
      }
   }


   /**
    * Log info message. Will log in case if level > MINIMAL
    *
    * @param objects variable parameters which will be logged.
    */
   public void info(Object ... objects) {
      if (_level.isGreaterThan(LogLevel.MINIMAL)) {
         writeLine(logMessage(objects));
      }
   }

   /**
    * Log details message. Will log in case if level > NORMAL
    *
    * @param objects variable parameters which will be logged.
    */
   public void details(Object ... objects) {
      if (_level.isGreaterThan(LogLevel.NORMAL)) {
         writeLine(logMessage(objects));
      }
   }

   /**
    * Log extra details message. Will log in case if level > VERBOSE
    *
    * @param objects variable parameters which will be logged.
    */
   public void extra(Object ... objects) {
      if (_level.isGreaterThan(LogLevel.VERBOSE)) {
         writeLine(logMessage(objects));
      }
   }

   private synchronized void writeLine(String line) {
      try {
         if (_output == null) return ;

         DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);

         _output.write((dateFormat.format(new Date()) + ": " + line).getBytes("UTF-8"));
         _output.flush();
      } catch(Throwable e){
         // ignore
      }
   }

   /**
    * Internal method to construct message from the array of objects.
    *
    * @param objects objects to construct message
    *
    * @return string with concatenated objects.
    */
   private static String logMessage(Object ... objects) {
      StringBuilder builder = new StringBuilder();

      for (Object o : objects) {
         builder.append(o);
      }

      builder.append("\n");

      return builder.toString();
   }

   private static final String DATE_FORMAT_STRING = "MM.dd.yyyy-k.m.s";
   private static final String NAME_FORMAT_POSTFIX = "-%s.runlog";
}