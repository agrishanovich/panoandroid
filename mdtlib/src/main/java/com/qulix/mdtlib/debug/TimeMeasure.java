package com.qulix.mdtlib.debug;
import android.util.Log;

public class TimeMeasure {

   public interface Measure {
      void end(String message);
      void end();
   }

   public TimeMeasure(String name) {
      _name = name;
   }

   public Measure start() {
      return start(null);
   }

   public Measure start(String comment) {
      ++_counter;
      final long startTime = System.currentTimeMillis();
      final int index = _counter;

      final String messageCommon = "Action " + _name + " run " + index;

      if (comment != null) {
         comment = messageCommon + " (" + comment + ")";
      }

      Log.d("TimeMeasure",
            time() + ": " + comment +" started");

      return new Measure() {

         @Override public void end(String message) {
            long timeTook = System.currentTimeMillis() - startTime;

            Log.d("TimeMeasure",
                  time() + ": " + messageCommon + " (" + message + ") took " + timeTook + " ms");
         }

         @Override public void end() {
            long timeTook = System.currentTimeMillis() - startTime;

            Log.d("TimeMeasure",
                  time() + ": " + messageCommon +" took " + timeTook + " ms");
         }
      };
   }

   private String time() {
      return String.format("%8d",
                           System.currentTimeMillis());
   }

   private int _counter;
   private String _name;
}