package com.qulix.mdtlib.database;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.qulix.mdtlib.functional.Factory;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.lists.dataset.BasicDataSet;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.subscription.RunnableSubscription;


/**
 * @class QueryResult
 *
 * @brief Result group controller.
 *
 * DbResultSet is subscribed to the events of result (change of state -
 * valid/invalid data) to behave correctly in case of table changes.
 *
 * To maintain such subscription, provide state changing methods and
 * factory method to create the DbResultSet, QueryResult is
 * provided.
 */
public final class QueryResult {

   public enum ResultState {
      Invalid,
      Valid
   }

   public QueryResult(ResultState initialState) {
      this(null, initialState);
   }

   public QueryResult(AsyncDBDataExtractor extractor, ResultState initialState) {
      _asyncExtractor = extractor;
      _resultState = initialState;
   }

   /**
    * This method will invalidate result table and notify all active result
    * sets that no data available.
    *
    * This method will perform notification only if current state of table
    * is other than invalid.
    */
   public void reset() {
      if (_resultState != ResultState.Invalid) {
         _resultState = ResultState.Invalid;

         _stateChangedEvent.run();
      }
   }

   /**
    * This method will validate result table and notify all active result
    * sets that data available.
    *
    * This method will perform notification not depending of current state
    * of table.
    */
   public QueryResult revalidate() {
      _resultState = ResultState.Valid;

      _stateChangedEvent.run();

      return this;
   }

   /**
    * @param cursorFactory facility to create cursor when it needed
    * @param extractor extractor to extract instances.
    *
    * @return new DbResultSet subscribed to this table events.
    */
   public <DataType> DbResultSet<DataType> createResultSet(Factory<Cursor> cursorFactory,
                                                           Extractor<DataType> extractor) {
      return new ResultSet<DataType>(cursorFactory,
                                     extractor);
   }

   private boolean isValid() {
      return _resultState == ResultState.Valid;
   }

   /**
    * Lazy getter for AsyncDBDataExtractor. This makes that in case if it
    * not needed (theoretically this is most of cases) this not be created.
    *
    * @return lazy created instance of AsyncDBDataExtractor
    */
   private AsyncDBDataExtractor asyncExtractor() {
      if (_asyncExtractor == null) {
         _asyncExtractor = new AsyncDBDataExtractor();
      }

      return _asyncExtractor;
   }

   private final class ResultSet<DataType> extends BasicDataSet<DataType> implements DbResultSet<DataType> {

      private Runnable _forwardNotification = new Runnable() {
            @Override public void run() {
               onStateChanged();
            }
         };


      public ResultSet(Factory<Cursor> cursorFactory,
                       Extractor<DataType> extractor) {
         _cursorFactory = cursorFactory;
         _extractor = extractor;

         // when state changed, notify all subscribers
         _stateChangedEvent.subscribeWeak(_forwardNotification);
      }

      @Override public Operation get(final int start,
                                     final int count,
                                     final Receiver<List<DataType>> receiver) {

         final Cursor rangeCursor = rangeCursor();

         AsyncDBDataExtractor.Request request
            = asyncExtractor().request(new Factory<Cursor>() {
                  @Override public Cursor create() {

                     // we will move only in case if start > 0 because if
                     // start is 0, we likely have situation of initiating
                     // new request, so to make actual request be performed
                     // on the async extractor worker thread, we will allow
                     // it to move cursor to first element.
                     if (start > 0) {
                        rangeCursor.moveToPosition(start);
                     }

                     return new CursorWrapper(rangeCursor) {
                        @Override public void close() {
                           // ignore. this cursor is reusable, we will not
                           // allow async extractor close it.
                        }
                     };
                  }
               } ,
               _extractor,
               receiver);

         request.setLimit(count);

         return request;
      }

      @Override public DataType get(int index) {
         if (isValid()) return extractObjectForIndex(index);
         else throw new Error("Impossible to get object - no data!");
      }

      @Override public int count() {
         if (isValid()){

            if (_count == -1) {
               _count = cursor().getCount();
            }

            return _count;
         }
         else return 0;
      }

      private DataType extractObjectForIndex(int index) {
         Cursor cursor = cursor();
         cursor.moveToPosition(index);
         return _extractor.extract(cursor);
      }

      private void onStateChanged() {
         reset();
         _count = -1;

         notifyUpdated();
      }

      private void reset() {
         if (_currentCursor != null) {
            _currentCursor.close();
            _managedCursors.remove(_currentCursor);
            _currentCursor = null;
         }

         if (_rangeCursor != null) {
            _rangeCursor.close();
            _managedCursors.remove(_rangeCursor);
            _rangeCursor = null;
         }
      }

      /**
       * Lazy getter for cursor.
       */
      private Cursor cursor() {
         if (_currentCursor == null) {

            if (! isValid()) throw new Error("Trying to create cursor for table in invalid state!");

            _currentCursor = _cursorFactory.create();
            _managedCursors.add(_currentCursor);
         }

         return _currentCursor;
      }

      private Cursor rangeCursor() {
         if (_rangeCursor == null) {

            if (! isValid()) throw new Error("Trying to create cursor for table in invalid state!");

            _rangeCursor = _cursorFactory.create();
            _managedCursors.add(_rangeCursor);
         }

         return _rangeCursor;
      }

      @Override protected void finalize() throws Throwable {
         reset();
         super.finalize();
      }

      /**
       * As this QueryResult uses only one AsyncDBDataExtractor, it can
       * assume that only one async extracting can be performed at a
       * time. This allows us to keep opened cursor for background requests
       * instead of opening cursor every time, avoiding actually performing
       * rerequests for cases if several subsequent requests is needed.
       */
      private Cursor _rangeCursor;
      private Cursor _currentCursor;
      private int _count = -1;

      private final Extractor<DataType> _extractor;
      private final Factory<Cursor> _cursorFactory;
   }

   /**
    * As java not guarantees calling finalize of class before finalize
    * method of it's members, we can run (and we did) into situation when
    * cursor's finalize method called first, resulting in error message in
    * logcat.
    *
    * Simple overriding of ResultSet's finalize method and closing cursor
    * there is not enought, because cursor's finalize method called before
    * ResultSet's method.
    *
    * So only way to deny cursor from being finalized before ResultSet is
    * to keep reference of it in the list before we will finalize result
    * set actually.
    */

   private static final List<Cursor> _managedCursors = new ArrayList<Cursor>();

   final RunnableSubscription _stateChangedEvent = new RunnableSubscription();

   private ResultState _resultState = ResultState.Invalid;

   private AsyncDBDataExtractor _asyncExtractor;
}