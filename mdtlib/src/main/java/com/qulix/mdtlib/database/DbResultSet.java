package com.qulix.mdtlib.database;

import java.util.List;

import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.lists.dataset.DataSet;
import com.qulix.mdtlib.operation.Operation;

public interface DbResultSet<DataType> extends DataSet<DataType> {

   /**
    * This method allows to request the range of objects.
    */
   Operation get(int offset, int count, Receiver<List<DataType>> receiver);
}