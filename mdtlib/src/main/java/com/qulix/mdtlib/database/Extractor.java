package com.qulix.mdtlib.database;

import android.database.Cursor;

public interface Extractor<ResultType> {
   ResultType extract(Cursor cursor);
}