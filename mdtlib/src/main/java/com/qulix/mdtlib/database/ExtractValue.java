package com.qulix.mdtlib.database;

import android.database.Cursor;
import java.text.DateFormat;
import java.util.Date;
import android.util.Log;

/**
 * @class ExtractValue
 *
 * @brief Extractor for value from Cursor.
 *
 * It initialized with column name. Column index is then cached and used
 * for every subsequent calls so same extractor object can not be used for
 * different requests (returning different column set).
 */
public class ExtractValue {

   private static final int INVALID_INDEX = -1;

   String _columnName;
   int _columnIndex = INVALID_INDEX;

   /**
    * Create extractor for column
    *
    * @param columnName name of column, which will be extracted with this
    * extractor.
    */
   public ExtractValue(String columnName) {
      if (columnName == null) throw new IllegalArgumentException("Column name can not be null!");

      _columnName = columnName;
   }

   /**
    * Extract value from cursor, representing it as a string.
    *
    * @param c cursor to extract value from
    *
    * @return value in form of string
    */
   public String asString(Cursor c) {
      return c.getString(columnIndex(c));
   }

   /**
    * Extract value from cursor, representing it as a int.
    *
    * @param c cursor to extract value from
    *
    * @return value in form of int
    */
   public int asInt(Cursor c) {
      return c.getInt(columnIndex(c));
   }

   /**
    * Extract value from cursor, representing it as a long.
    *
    * @param c cursor to extract value from
    *
    * @return value in form of long
    */
   public long asLong(Cursor c) {
      return c.getLong(columnIndex(c));
   }

   /**
    * Extract value from cursor, representing it as a double.
    *
    * @param c cursor to extract value from
    *
    * @return value in form of double
    */
   public double asDouble(Cursor c) {
      return c.getDouble(columnIndex(c));
   }

   /**
    * Extract value from cursor, representing it as a boolean. Will return
    * true if column value is not null and can be interpreted as integer
    * with non zero value.
    *
    * @param c cursor to extract value from
    *
    * @return value in form of boolean
    */
   public boolean asBoolean(Cursor c) {
      int index = columnIndex(c);
      return ((! c.isNull(index))
              && (c.getInt(index) != 0));
   }

   public boolean isNull(Cursor c) {
      return c.isNull(columnIndex(c));
   }

   /**
    * Extract value from cursor, showing it as date, loading from string
    *
    * @param c cursor to extract value from
    * @param format format of date
    *
    * @return Date object
    */
   public Date asDate(Cursor c, DateFormat format) {
      String data = asString(c);
      try {
         return format.parse(data);
      } catch(Throwable e) {
         Log.e("ExtractValue", "Failed to extract date from: " + data + ": " + e.toString());
      }

      return null;
   }

   /**
    * Extract date from saved column of cursor, threating long value in
    * column as miliseconds value used to construct date. This is method
    * to superseed asDate with DateFormat as this is very ineffcient to
    * store date as a string.
    *
    * @param c cursor to extract date
    *
    * @return Date object
    */
   public Date asDate(Cursor c) {
      return new Date(asLong(c));
   }


   /**
    * Get (possibly cached) index of column. Argument used only for first
    * call - to actually get and cache value.
    *
    * @param c cursor to get index for
    *
    * @return column index
    */
   private int columnIndex(Cursor c) {
      if (_columnIndex == INVALID_INDEX) {
         _columnIndex = c.getColumnIndex(_columnName);
      }

      if (_columnIndex == INVALID_INDEX) {
         throw new IllegalStateException("Can't find column: " + _columnName);
      }
      return _columnIndex;
   }

}