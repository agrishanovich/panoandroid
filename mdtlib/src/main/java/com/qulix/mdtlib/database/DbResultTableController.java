package com.qulix.mdtlib.database;
import com.qulix.mdtlib.database.cursorfactory.AllFromTable;

import android.database.sqlite.SQLiteOpenHelper;

/**
 * @class DbResultTableController
 *
 * @brief Result table controller.
 *
 * There is abstraction of "result table" - table in database which created
 * to contain some kind of results, which will be provided to user.
 *
 * The use case of "result table" is the list with big amount of data,
 * which will not be changed frequently. In this case, for such list can be
 * created table containing result and by means of DbResultSet access to
 * this table can be provided.
 *
 * DbResultSet is subscribed to the events of table (change of state -
 * valid/invalid data) to behave correctly in case of table changes.
 *
 * To maintain such subscription, provide state changing methods and
 * factory method to create the DbResultSet, DbResultTableController is
 * provided.
 */
public final class DbResultTableController {

   public DbResultTableController(SQLiteOpenHelper db,
                                  String tableName) {
      _db = db;
      _tableName = tableName;

      _queryResult = new QueryResult(QueryResult.ResultState.Invalid);
   }


   /**
    * This method will invalidate result table and notify all active result
    * sets that no data available.
    *
    * This method will perform notification only if current state of table
    * is other than invalid.
    */
   public void reset() {
      _queryResult.reset();
   }

   /**
    * This method will validate result table and notify all active result
    * sets that data available.
    *
    * This method will perform notification not depending of current state
    * of table.
    */
   public void revalidate() {
      _queryResult.revalidate();
   }

   /**
    * This method is intended for creating result set based on this table.
    *
    * Result set will use specified extractor to extract instances of
    * result from cursor pointing to table controlling by this
    * DbResultTableController instance.
    *
    * @param extractor extractor to extract instances.
    *
    * @return new DbResultSet subscribed to this table events.
    */
   public <DataType> DbResultSet<DataType> createResultSet(Extractor<DataType> extractor) {
      return _queryResult.createResultSet(new AllFromTable(_db.getReadableDatabase(),
                                                           _tableName),
                                          extractor);
   }

   private final SQLiteOpenHelper _db;
   private final String _tableName;

   private final QueryResult _queryResult;
}