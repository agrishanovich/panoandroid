package com.qulix.mdtlib.database;

import com.qulix.mdtlib.debug.TimeMeasure;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import android.database.Cursor;

import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.concurrency.ThreadPool;
import com.qulix.mdtlib.concurrency.Worker;
import com.qulix.mdtlib.functional.Factory;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.SimpleOperation;


/**
 * @class AsyncDBDataExtractor
 *
 * @brief Asyncronous database data set extractor.
 */
public class AsyncDBDataExtractor {

   /// size of thread pool.
   private static final int THREAD_POOL_SIZE = 1;

   /// queue of requests for this extractor
   private final Queue<RequestInfo<?>> _reqQueue = new LinkedList<RequestInfo<?>>();

   /// flag, which indicates that some request already in progress, and so
   /// we need not issue new request in parallel, but instead just store
   /// request for further time.
   private boolean _requestInProgress;

   public interface Request extends Operation {
      void setLimit(int limit);
   }

   static TimeMeasure _tm = new TimeMeasure("extracting chunk");

   /**
    * @class RequestInfo
    *
    * @brief Internal class to store information about pending async
    * request to the database data.
    */
   private class RequestInfo<DataType> extends SimpleOperation implements Request {
      /// request's result
      private List<DataType> _result;

      /// preparation runnable - the object, which will be runned, when
      /// initializing of this request
      private Runnable _preparator;

      /// the handler of the resulting data.
      private Receiver<List<DataType>> _receiver;

      private final Factory<Cursor> _cursorFactory;

      /// object, capable to extract array of expected data objects.
      private final ExtractList<DataType> _extractor;

      private int _limit = ExtractList.ALL;

      /**
       * Constructor of request info object
       *
       * @param db database to work against
       * @param preparator preparator runnable
       * @param receiver data receiver object
       */
      RequestInfo(Factory<Cursor> cursorFactory, Runnable preparator, Receiver<List<DataType>> receiver, ExtractList<DataType> extractor) {
         _cursorFactory = cursorFactory;
         _preparator = preparator;
         _receiver = receiver;
         _extractor = extractor;
      }

      /**
       * Perform creation stage.
       */
      public void prepare() {
         if (_preparator != null) {
            _preparator.run();
         }
      }




      /**
       * Perform selection stage.
       */
      public void doSelect() {
         Cursor cursor = _cursorFactory.create();

         try {
            _result =_extractor.extract(cursor,
                                        _limit);
         } finally {
            cursor.close();
         }
      }

      /**
       * Perform passing data to the data receiver.
       */
      public void doNotify() {
         if (!isEnded()) {
            _receiver.receive(_result);
            terminate();
         }
      }

      @Override public void setLimit(int limit) {
         _limit = limit;
      }
   }

   /**
    * Constructor for the extractor object.
    *
    * @param tableName table name to select data from
    * @param extractor extractor object, which is capable to extract array
    * of data from temporary table tableName
    */
   public AsyncDBDataExtractor() {
      // ensure handler
      CTHandler.init();
   }

   /**
    * Queue request. In case if queue is empty and no request is performed
    * right now, execute.
    */
   public <DataType> Request request(Runnable prepareRunnable,
                                       Factory<Cursor> cursorFactory,
                                       Extractor<DataType> extractor,
                                       Receiver<List<DataType>> receiver) {

      return run(new RequestInfo<DataType>(cursorFactory, prepareRunnable, receiver,
                                           new ExtractList<DataType>(extractor)));
   }

   /**
    * Queue request. In case if queue is empty and no request is performed
    * right now, execute.
    */
   public <DataType> Request request(Runnable prepareRunnable,
                                       Factory<Cursor> cursorFactory,
                                       ExtractList<DataType> extractor,
                                       Receiver<List<DataType>> receiver) {

      return run(new RequestInfo<DataType>(cursorFactory, prepareRunnable, receiver, extractor));
   }

   public <DataType> Request request(Factory<Cursor> cursorFactory,
                                       ExtractList<DataType> extractor,
                                       Receiver<List<DataType>> receiver) {

      return request(null, cursorFactory, extractor, receiver);
   }

   public <DataType> Request request(Factory<Cursor> cursorFactory,
                                       Extractor<DataType> extractor,
                                       Receiver<List<DataType>> receiver) {

      return request(null, cursorFactory, extractor, receiver);
   }

   private <DataType> Request run(RequestInfo<DataType> request) {
      if (_requestInProgress) {
         saveRequest(request);
      } else {
         startRequest(request);
      }

      return request;
   }

   /**
    * Save request information for further performing.
    *
    * @param req information of request to save
    */
   private void saveRequest(RequestInfo<?> req) {
      _reqQueue.offer(req);
   }

   /**
    * Start executing request right now.
    *
    * @param req information of request to run
    */
   private void startRequest(final RequestInfo<?> req) {
      _requestInProgress = true;

      req.prepare();

      _worker.submit(new Runnable() {
            public void run() {

               req.doSelect();

               CTHandler.post(new Runnable(){
                     public void run() {
                        completeRequest(req);
                     }
                  } );
            }
         } );
   }

   /**
    * Perform request completion - pass data of current request to the
    * caller and start new request, if any.
    *
    * @param req information of request to complete
    */
   private void completeRequest(RequestInfo<?> req) {
      req.doNotify();

      RequestInfo<?> nextRequest = null;

      // find first not ended request
      while (((nextRequest = _reqQueue.poll()) != null)
             && (nextRequest.isEnded()));

      if (nextRequest != null) {
         startRequest(nextRequest);
      } else {
         _requestInProgress = false;
      }
   }

   private static Worker _worker = ThreadPool.newWithBackgroundPriority(THREAD_POOL_SIZE,
                                                                        "Database async extractor thread");
}


