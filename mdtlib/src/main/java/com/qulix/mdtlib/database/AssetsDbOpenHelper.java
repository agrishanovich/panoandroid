package com.qulix.mdtlib.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AssetsDbOpenHelper extends SQLiteOpenHelper {

   //The Android's default system path of your application database.
   private static File dbPath(Context context, String path) {
      return context.getDatabasePath(path);
   }

   private SQLiteDatabase _db;

   private final Context _context;
   private final String _name;

   /**
    * Constructor
    * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
    * @param context
    */
   public AssetsDbOpenHelper(Context context,
                             String dbName) throws IOException {

      super(context, dbName, null, 1);
      _name = dbName;
      _context = context;

      createDataBase();
   }

   public AssetsDbOpenHelper(Context context,
                             String dbName,
                             int version) throws IOException {

      super(context, dbName, null, version);
      _name = dbName;
      _context = context;

      createDataBase();
   }



   /**
    * Creates a empty database on the system and rewrites it with your own database.
    * */
   public void createDataBase() throws IOException {

      boolean dbExist = checkDataBase();

      if(dbExist){
         //do nothing - database already exist
      }else{

         //By calling this method and empty database will be created into the default system path
         //of your application so we are gonna be able to overwrite that database with our database.
         this.getReadableDatabase();
         this.close();

         try {

            copyDataBase();

         } catch (IOException e) {

            throw new Error("Error copying database: " + Log.getStackTraceString(e));

         }
      }

   }

   /**
    * Check if the database already exist to avoid re-copying the file each time you open the application.
    * @return true if it exists, false if it doesn't
    */
   private boolean checkDataBase(){

      File dbFile = dbPath(_context, _name);

      return dbFile.exists();
   }

   /**
    * Copies your database from your local assets-folder to the just created empty database in the
    * system folder, from where it can be accessed and handled.
    * This is done by transfering bytestream.
    * */
   private void copyDataBase() throws IOException{
      InputStream myInput = null;
      OutputStream myOutput = null;
      try {
         //Open your local db as the input stream
         myInput = _context.getAssets().open(_name);

         // Path to the just created empty db
         File outFile = dbPath(_context, _name);

         //Open the empty db as the output stream
         myOutput = new FileOutputStream(outFile);

         //transfer bytes from the inputfile to the outputfile
         byte[] buffer = new byte[1024];
         int length;
         while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
         }

      } finally {
         //Close the streams
         if (myOutput != null) {

            try {
               myOutput.flush();
            } catch (IOException ignored) {

            }

            try {
               myOutput.close();
            } catch (IOException ignored) {

            }
         }

         if (myInput != null) {
            try {
               myInput.close();
            } catch (IOException ignored) {

            }
         }
      }

   }

   public void openDataBase() throws SQLException{

      //Open the database
      String myPath = dbPath(_context, _name).toString();
      _db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

   }

   @Override
   public synchronized void close() {

      if(_db != null) {
         _db.close();
      }

      super.close();

   }

   @Override
   public void onCreate(SQLiteDatabase db) {

   }

   @Override
   public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
   }

   protected void replaceDb() {
      _context.deleteDatabase(_name);
      try {
         copyDataBase();
      } catch (IOException e) {
         throw new Error("Error copying database");
      }
   }

   // Add your public helper methods to access and get content from the database.
   // You could return cursors by doing "return _db.query(....)" so it'd be easy
   // to you to create adapters for your views.

}