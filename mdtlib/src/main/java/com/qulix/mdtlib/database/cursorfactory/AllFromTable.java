package com.qulix.mdtlib.database.cursorfactory;

import com.qulix.mdtlib.functional.Factory;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public final class AllFromTable implements Factory<Cursor> {
   public AllFromTable(SQLiteDatabase db,
                       String tableName) {
      _db = db;
      _tableName = tableName;
   }

   public Cursor create() {
      return _db.rawQuery("select * from " + _tableName,
                          null);
   }

   private final SQLiteDatabase _db;
   private final String _tableName;
}