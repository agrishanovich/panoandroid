package com.qulix.mdtlib.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;


/**
 * @class Request
 *
 * @brief This is SQL compiled statement wrapper.
 *
 *
 */
public class Request {

   String _sql;
   SQLiteStatement _compiled;

   /**
    * Create request from SQL request string
    *
    * @param sql sql request string to request from
    */
   public Request(String sql) {
      _sql = sql;
   }

   /**
    * Prepare compiled statement
    *
    * @param db database to compile statement against
    *
    * @return prepared request
    */
   public SQLiteStatement prepare(SQLiteDatabase db) {
      if (_compiled == null) {
         _compiled = db.compileStatement(_sql);
      } else {
         _compiled.clearBindings();
      }

      return _compiled;
   }

   /**
    * Use this method to execute request in case if it no need to bind
    * something.
    */
   public void exec(SQLiteDatabase db) throws SQLiteException {
      SQLiteStatement st = prepare(db);
      st.execute();
   }

   public long insert(SQLiteDatabase db) throws SQLiteException {
      SQLiteStatement st = prepare(db);
      return st.executeInsert();
   }
}