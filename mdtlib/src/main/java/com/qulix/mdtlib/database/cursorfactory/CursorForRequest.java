package com.qulix.mdtlib.database.cursorfactory;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.qulix.mdtlib.functional.Factory;

public class CursorForRequest implements Factory<Cursor> {
   public CursorForRequest(SQLiteDatabase db, String requestString, String... arguments) {
      _db = db;
      _requestString = requestString;
      _arguments = arguments;
   }

   public CursorForRequest(SQLiteDatabase db, String requestString, Object... arguments) {
      _db = db;
      _requestString = requestString;
      _arguments = new String[arguments.length];

      for (int a = 0; a < arguments.length; ++a) {
         _arguments[a] = arguments[a].toString();
      }
   }

   public Cursor create() {
      return _db.rawQuery(_requestString, _arguments);
   }

   private final SQLiteDatabase _db;
   private final String _requestString;
   private final String[] _arguments;
}