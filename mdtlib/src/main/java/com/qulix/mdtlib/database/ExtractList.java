package com.qulix.mdtlib.database;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;

public class ExtractList<DataType> {

   public static final int ALL = -1;

   public ExtractList(Extractor<DataType> typeExtractor) {
      _typeExtractor = typeExtractor;
   }

   public ExtractList() {
      this(null);
   }

   public final List<DataType> extract(Cursor cursor,
                                       int limit) {

      int count = cursor.getCount();

      if (count <= 0) {
         return new ArrayList<DataType>(0);
      }

      if (cursor.isBeforeFirst()) {
         cursor.moveToFirst();
      }

      List<DataType> data = new ArrayList<DataType>(count);

      if (limit == ALL) {
         limit = count;
      }

      while ((!cursor.isAfterLast()) && limit > 0) {

         data.add(create(cursor));

         --limit;
         cursor.moveToNext();
      }

      return data;
   }

   public DataType create(Cursor cursor) {
      if (_typeExtractor == null) throw new Error("You must provide non-null type extractor or override create method.");

      return _typeExtractor.extract(cursor);
   }

   private Extractor<DataType> _typeExtractor;
}