package com.qulix.mdtlib.database.cursorfactory;

import android.database.Cursor;

import com.qulix.mdtlib.functional.Factory;

public class CursorWithOffset implements Factory<Cursor> {
   public CursorWithOffset(int offset,
                           Factory<Cursor> slave) {
      _offset = offset;
      _slave = slave;
   }

   public Cursor create() {
      Cursor c = _slave.create();
      c.move(_offset);
      return c;
   }

   private Factory<Cursor> _slave;
   private int _offset;
}