package com.qulix.mdtlib.database;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class is simple wrapper for db transactions, allowing to avoid boilerplate code.
 */
public class DBTransactionExecutor {

   private static class DBAction {
      protected Cursor forClose(Cursor cursor) {
         if (forClose != null) throw new IllegalStateException("Trying to register second cursor for close");

         forClose = cursor;

         return cursor;
      }

      void closeCursor() {
         if (forClose != null) {
            forClose.close();
         }

         forClose = null;
      }

      private Cursor forClose;
   }

   /**
    * Interface for action intended to return value
    */
   public static abstract class Action<ResultType> extends DBAction {
      public abstract ResultType execute(SQLiteDatabase db) throws SQLiteException;
   }

   /**
    * Interface for action intended for side-effects only
    */
   public static abstract class VoidAction extends DBAction {
      public abstract void execute(SQLiteDatabase db) throws SQLiteException;
   }

   public interface DumpMethod {
      void dump(Throwable e);
   }

   public static final DumpMethod Dump_LogStacktrace = new DumpMethod() {
         @Override public void dump(Throwable e) {
            Log.e("DBTransactionExecutor", "Exception: " + Log.getStackTraceString(e));
         }
      };

   public DBTransactionExecutor(SQLiteOpenHelper dbOpenHelper,
                                DumpMethod dumpMethod) {
      this.dbOpenHelper = dbOpenHelper;
      this.dumpMethod = dumpMethod;
   }

   public <ResultType> ResultType executeWrite(Action<ResultType> action, ResultType defaultValue) {
      return execute(true, action, defaultValue);
   }

   public <ResultType> ResultType executeRead(Action<ResultType> action, ResultType defaultValue) {
      return execute(false, action, defaultValue);
   }

   private <ResultType> ResultType execute(boolean writeable,
                                           Action<ResultType> action,
                                           ResultType defaultValue) {
      ResultType value = defaultValue;

      SQLiteDatabase db = null;

      if (writeable) {
         db = dbOpenHelper.getWritableDatabase();
      } else {
         db = dbOpenHelper.getReadableDatabase();
      }

      db.beginTransaction();
      try {

         value = action.execute(db);

         db.setTransactionSuccessful();
      } catch (SQLiteException e) {
         dump(e);
         throw e;
      } finally {
         db.endTransaction();
      }

      action.closeCursor();

      return value;
   }

   private Action<Void> wrap(final VoidAction action) {
      return new Action<Void>() {
         @Override public Void execute(SQLiteDatabase db) {
            action.execute(db);
            return null;
         }
      } ;
   }

   public void executeWrite(VoidAction action) {
      execute(true, wrap(action), null);
   }

   public void executeRead(VoidAction action) {
      execute(false, wrap(action), null);
   }

   private void dump(Throwable e) {
      if (dumpMethod == null) return ;

      dumpMethod.dump(e);
   }

   private SQLiteOpenHelper dbOpenHelper;

   private DumpMethod dumpMethod;
}