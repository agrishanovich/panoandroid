package com.qulix.mdtlib.connection;

public interface StreamResolver <SourceType> {
   BackedStream resolveToStream(SourceType data);
}
