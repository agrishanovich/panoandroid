package com.qulix.mdtlib.connection;

import android.util.Log;
import com.qulix.mdtlib.concurrency.CTHandler;
import com.qulix.mdtlib.functional.Cancellable;
import com.qulix.mdtlib.functional.Receiver;

import java.io.IOException;
import java.io.InputStream;

/**
 * Backed stream is the abstraction of stream, which handles creating it's
 * slave stream by itself as fast as this needed.
 *
 * This abstraction is mainly thread safe (except that read methods can not
 * be called on Control Thread (CT)).
 *
 * Close method of this object can be called from any thread and in any
 * state, it will not throw any exception.
 *
 * After calling close method, all data access methods will throw
 * IOException("Stream is closed").
 *
 * The "external" working of this object is next:
 *
 * Creating a stream is fast - this is simply creating control object, no
 * any work done. When data access methods of this stream is called, stream
 * blocks, until background stream is created by factory, then stream
 * unblocks and executes data access operation on background stream. In
 * case if background stream fails to return data because of IOException,
 * BackedStream blocks execution of data access methods and requests
 * RecoveryStrategy object for the way of recovery. Because of async
 * interface of recovery strategy, BackedStream can be held in "recovering"
 * state as long as needed, allowing RecoveryStrategy to wait for better
 * connection, asking user or doing any other long-term work.
 *
 * All interactions with other objects (except of slave stream) this class
 * does on the CT, to hide some threading problems internally, this
 * includes - calling of any methods of StreamFactory and RecoveryStrategy
 * and calling cancel methods of cancellables, returned by StreamFactory
 * and RecoveryStrategy.
 *
 *
 * State diagram:
 *
 *     +--------------+                 +--------------------------+
 *     | Clean State  |  on data        | Waiting For Stream State |
 *     +--------------+  accesss        +--------------------------+        on close
 *     | State object |---------------->| Data methods blocked,    |----------------------------+
 *     |  created in  |                 | Waiting for stream from  |                            |
 *     +--------------+                 | factory                  |                            |
 *       |                              +--------------------------+                            |
 *       |                              /            ^            |                             |
 *       |               on fail to    /             |            |                             |
 *       |                 get stream /              |            | on stream                   |
 *       |                           /               |            | created                     |
 *       |                          /                |            |                             |
 *       | on                      L                 |            v                             |
 *       | close        +------------------------+   |         |-------------------------+      |
 *       |              | Error State            |   |         | Ready State             |      |
 *       |              +------------------------+   |         +-------------------------+      |
 *       |              | Data access methods    |   |         | Factory returned stream |      |
 *       |              | will throw IOException |   |         | Data access methods     |      |
 *       |              +------------------------+   |         | will return data        |      |
 *       |                  /        ^               |         +-------------------------+      |
 *       |                 /         |               |            /                |            |
 *       |         on     /       on |            on |           / on IOException  | on         |
 *       |         close /      fail |         retry |          /  in slave        | close      |
 *       |              /            |               |         /   stream          |            |
 *       v             /             |               |        L                    |            |
 *   +--------------+ L            +------------------------+                      |            |
 *   | Closed State |              | Recovering State       |                      |            |
 *   +--------------+   on close   +------------------------+                      |            |
 *   | Stream is    |<-------------| Data methods blocked,  |                      |            |
 *   |  Closed      |              | recovery way requested |                      |            |
 *   +--------------+              +------------------------+                      |            |
 *           ^                                                                     |            |
 *           |                                                                     |            |
 *           +---------------------------------------------------------------------+------------+
 *
 *
 */

public final class BackedStream extends InputStream {

   /**
    * For sake of simplicity all methods of this interface (and cancel of
    * Cancellable) is guaranteed to be called on CT.
    */
   public interface StreamFactory {
      void close();

      Cancellable requestStream(long offset,
                                Receiver<InputStream> streamReceiver,
                                Runnable errorReceiver);
   }

   /**
    * For sake of simplicity all methods of this interface must be called
    * on CT.
    */
   public interface RecoveryWay {
      void fail();
      void retry();
   }

   /**
    * For sake of simplicity all methods of this interface (and cancel of
    * Cancellable) is guaranteed to be called on CT.
    */
   public interface RecoveryStrategy {
      void close();

      Cancellable decideRecoveryWay(IOException exception,
                                    RecoveryWay wayToRecover);
   }


   /**
    * This object must be created on control thread, as he creates handler
    * to pass messages.
    */
   public BackedStream(StreamFactory streamFactory,
                       RecoveryStrategy recoveryStrategy) {
      _streamFactory = streamFactory;
      _recoveryStrategy = recoveryStrategy;
   }


   // ==========================================================================
   // Close method. It will be callable in every state and must not block.
   // ==========================================================================

   @Override public void close() throws IOException {
      synchronized(_stateChange) {
         if (! (_currentState instanceof StateClosed) ) {
            switchState(new StateClosed());
         }
      }
   }


   // ==========================================================================
   // This section of methods for marking, which is not implemented for
   // this stream
   // ==========================================================================

   @Override public void mark(int readlimit) {
      throw new RuntimeException("This stream does not supports marking.");
   }

   @Override public boolean markSupported() {
      return false;
   }

   @Override public synchronized void reset() throws IOException {
      throw new RuntimeException("This stream does not supports marking.");
   }

   // ==========================================================================
   // Stream accessing and process methods
   //
   // These methods can be called from any non-CT thread, as possibly can
   // block execution
   // ==========================================================================

   @Override public synchronized int available() throws IOException {
      return (int)doAction(new Action() {
            @Override public long doAction(InputStream stream) throws IOException {
               return stream.available();
            }
         } );
   }


   @Override public synchronized int read(final byte[] buffer) throws IOException {
      return (int)doAction(new Action() {
            @Override public long doAction(InputStream stream) throws IOException {
               long retval = stream.read(buffer);
               _streamOffset += retval;
               return retval;
            }
         } );
   }

   @Override public synchronized int read() throws IOException {
      return (int)doAction(new Action() {
            @Override public long doAction(InputStream stream) throws IOException {
               long retval = stream.read();
               _streamOffset += 1;
               return retval;
            }
         } );
   }

   @Override public int read(final byte[] buffer, final int offset, final int length) throws IOException {
      return (int)doAction(new Action() {
            @Override public long doAction(InputStream stream) throws IOException {
               long retval = stream.read(buffer, offset, length);
               _streamOffset += retval;
               return retval;
            }
         } );
   }

   @Override public long skip(final long byteCount) throws IOException {
      return doAction(new Action() {
            @Override public long doAction(InputStream stream) throws IOException {
               long retval = stream.skip(byteCount);
               _streamOffset += retval;
               return retval;
            }
         } );
   }

   // ==========================================================================
   // Actions mechanics to implement access methods
   // ==========================================================================

   private interface Action {
      long doAction(InputStream stream) throws IOException;
   }

   private long doAction(Action action) throws IOException {
      for(;;) {
         InputStream stream = null;

         synchronized(_stateChange) {
            // will lead to throwing closed IOException
            stream = _currentState.prepareStream();
         }

         try {
            // try to perform operation
            return action.doAction(stream);

         } catch (IOException e) {
            // if it fails, switch to recovering state
            switchState(new StateRecovering(e));
         }
      }
   }

   // ==========================================================================
   // Internal code of state handling
   // ==========================================================================

   private interface State {
      // this method must return stream or raise exception
      InputStream prepareStream() throws IOException;

      void openState();

      void closeState();
   }

   // ==========================================================================
   // Clean state. This state only needed to block and request stream.
   // ==========================================================================

   private final class StateClean implements State {

      @Override public InputStream prepareStream() throws IOException {
         switchState(new StateWaitingForStream());
         return blockUntilStream();
      }

      @Override public void openState() {
         throw new RuntimeException("Panic! openState must not be called in StateClean.");
      }

      @Override public void closeState() {
         // simply ignore this call
      }
   }

   // ==========================================================================
   // Waiting for stream state. This state will block any requests and
   // unblock as soon as stream arrives.
   // ==========================================================================

   private final class StateWaitingForStream implements State {
      @Override public InputStream prepareStream() throws IOException {
         // as we already waiting for stream, simply block until stream
         // arrives.
         return blockUntilStream();
      }

      @Override public void openState() {
         // post request stream
         CTHandler.post(new Runnable() {
               @Override public void run() {
                  requestStream();
               }
            });
      }

      // on CT
      private void requestStream() {
         synchronized (_stateChange) {
            if (_closed) return ;

            _streamRequest = _streamFactory.requestStream(_streamOffset,
                                                          new Receiver < InputStream >() {
                                                             @Override public void receive(InputStream readyStream) {
                                                                onStreamReady(readyStream);
                                                             }
                                                          },
                                                          new Runnable() {
                                                             @Override public void run() {
                                                                switchState(new StateError(new IOException("Failed to create stream.")));
                                                             }
                                                          });
         }
      }

      // on CT
      private void onStreamReady(InputStream readyStream) {
         synchronized(_stateChange) {
            _streamRequest = null;

            if (_closed) {
               try {
                  readyStream.close();
               } catch(IOException e) {
                  Log.e("BackedStream", "Failed to close stream: " + Log.getStackTraceString(e));
               }
            } else {
               switchState(new StateReady(readyStream));
            }
         }
      }


      @Override public void closeState() {
         synchronized(_stateChange) {
            _closed = true;

            if (_streamRequest != null) {
               CTHandler.post(new Runnable() {
                     @Override public void run() {
                        closeRequest();
                     }
                  });
            }
         }
      }

      // on CT
      private void closeRequest() {
         synchronized(_stateChange) {
            if (_streamRequest != null) {
               _streamRequest.cancel();
               _streamRequest = null;
            }
         }
      }


      private Cancellable _streamRequest;
      private boolean _closed;
   }

   // ==========================================================================
   // Ready state - in this state it is possible to do operations.
   // ==========================================================================

   private final class StateReady implements State {
      public StateReady(InputStream stream) {
         _streamToSet = stream;
      }

      @Override public InputStream prepareStream() throws IOException {
         return _stream;
      }

      @Override public void openState() {
         // notify potential waiter that stream is ready
         wakeUpWithStream(_streamToSet);
         _streamToSet = null;
      }

      @Override public void closeState() {
         try {
            _stream.close();
         } catch (IOException e) {
            Log.e("BackedStream", "Failed to close stream: " + Log.getStackTraceString(e));
         }
         _stream = null;
      }

      private InputStream _streamToSet;
   }

   // =========================================================================
   // Recovering state - the error occured and we need to request recovery
   // strategy to decide what to do
   // ==========================================================================

   private final class StateRecovering implements State {
      public StateRecovering(IOException error) {
         _error = error;
      }

      @Override public InputStream prepareStream() throws IOException {
         return blockUntilStream();
      }

      @Override public void openState() {
         synchronized (_stateChange) {
            if (_closed) return ;
            // post request for recovery strategy to decide what to do
            CTHandler.post(new Runnable() {
                  @Override public void run() {
                     synchronized (_stateChange) {
                        if (_closed) return ;

                        _request = _recoveryStrategy.decideRecoveryWay(_error,
                                                                       createRecoveryWay());
                     }
                  }
               });
         }
      }

      @Override public void closeState() {
         synchronized (_stateChange) {
            _closed = true;

            if (_request != null) {
               CTHandler.post(new Runnable() {
                     @Override public void run() {
                        synchronized (_stateChange) {
                           if (_request != null)  {
                              _request.cancel();
                              _request = null;
                           }
                        }

                     }
                  } );
            }
         }
      }

      private RecoveryWay createRecoveryWay() {
         return new RecoveryWay() {
            @Override public void fail() {
               synchronized (_stateChange) {
                  if (_closed) return ;
                  switchState(new StateError(_error));
               }
            }

            @Override public void retry() {
               synchronized (_stateChange) {
                  if (_closed) return ;
                  switchState(new StateWaitingForStream());
               }
            }
         } ;
      }

      private boolean _closed;
      private Cancellable _request;
      private IOException _error;
   }

   // ==========================================================================
   // Error state
   // ==========================================================================

   private final class StateError implements State {
      public StateError(IOException error) {
         _error = error;
      }

      @Override public InputStream prepareStream() throws IOException {
         throw _error;
      }

      @Override public void openState() {
         wakeUpWithError(_error);
      }

      @Override public void closeState() {
         _error = null;
      }

      private IOException _error;
   }

   // ==========================================================================
   // Closed state
   // ==========================================================================

   private final class StateClosed implements State {
      @Override public InputStream prepareStream() throws IOException {
         throw createError();
      }

      @Override public void openState() {
         wakeUpWithError(createError());

         CTHandler.post(new Runnable() {
               @Override public void run() {
                  _streamFactory.close();
                  _recoveryStrategy.close();
               }
            });
      }

      @Override public void closeState() {
         // do nothing
      }

      private IOException createError() {
         return new IOException("Stream is closed");
      }
   }

   // ==========================================================================
   // Blocking/waking up methods and code
   // ==========================================================================

   private InputStream blockUntilStream() throws IOException {
      synchronized (_stateChange){
         while (_stream == null
                && _error == null) {
            try {
               _stateChange.wait();
            } catch (InterruptedException e) {
               // restart wait
            }
         }

         if (_stream != null) {
            return _stream;
         }

         if (_error != null) {
            throw _error;
         }

         throw new RuntimeException("Panic: unblocked with no error or stream!");
      }
   }

   private InputStream _stream;
   private IOException _error;

   private void wakeUpWithError(IOException error) {
      synchronized (_stateChange){
         _error = error;
         _stateChange.notifyAll();
      }
   }

   private void wakeUpWithStream(InputStream stream) {
      synchronized (_stateChange){
         _stream = stream;
         _stateChange.notifyAll();
      }
   }


   // ==========================================================================
   // State changing
   // ==========================================================================

   private final Object _stateChange = new Object();

   private void switchState(final State nextState) {
      synchronized(_stateChange) {
         _currentState.closeState();

         _currentState = nextState;
         _currentState.openState();
      }
   }


   private State _currentState = new StateClean();


   private final StreamFactory _streamFactory;
   private final RecoveryStrategy _recoveryStrategy;

   private long _streamOffset;
}