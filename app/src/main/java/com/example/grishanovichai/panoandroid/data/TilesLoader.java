package com.example.grishanovichai.panoandroid.data;

import android.graphics.Bitmap;
import com.example.grishanovichai.panoandroid.utils.TileNameRule;
import com.example.grishanovichai.panoandroid.utils.TilesCombiner;
import com.panoramagl.enumerations.PLCubeFaceOrientation;
import com.qulix.mdtlib.concurrency.ThreadOperation;
import com.qulix.mdtlib.functional.Continuation;
import com.qulix.mdtlib.functional.Maybe;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.operation.CompositeOperation;
import com.qulix.mdtlib.subscription.ReceiverSubscription;
import com.qulix.mdtlib.subscription.interfaces.Subscription;

public class TilesLoader {

   public static TilesLoader getInstance() {
      if (_instance == null) {
         _instance = new TilesLoader();
      }
      return _instance;
   }

   public Subscription<Receiver<TileInfo>> onTileLoadedSubscription() {
      return _onTileLoadedSubscription;
   }

   public void startLoad() {
      reset();
      loadNextTiles();
   }

   private void loadNextTiles() {
      String[][] names = TileNameRule.getTileNames(_currentOrientation, _currentLevel);
      _loader = Maybe.value(new TileLoader(names, new Continuation<Bitmap>() {
         @Override
         public void catchException(Throwable exception) {
            throw new RuntimeException("error occured while tiles are loading!");
         }

         @Override
         public void receive(Bitmap tile) {
            _onTileLoadedSubscription.receive(new TileInfo(tile, _currentOrientation));
            if (_currentOrientation.ordinal() < PLCubeFaceOrientation.values().length - 1) {
               _currentOrientation = PLCubeFaceOrientation.values()[_currentOrientation.ordinal() + 1];
            } else {
               _currentLevel--;
               _currentOrientation = PLCubeFaceOrientation.values()[0];
            }
            if (_currentLevel >= 4) {
               loadNextTiles();
            }
         }
      }));
   }

   public void stopLoad() {
      reset();
   }

   private void reset() {
      _currentOrientation = PLCubeFaceOrientation.values()[0];
      _currentLevel = 5;
      if (_loader.isValue()) {
         _loader.value().terminate();
      }
   }

   private ReceiverSubscription<TileInfo> _onTileLoadedSubscription = new ReceiverSubscription<>();
   private PLCubeFaceOrientation _currentOrientation;
   private int _currentLevel;
   private Maybe<TileLoader> _loader = Maybe.nothing();

   private static TilesLoader _instance;

   private class TileLoader extends CompositeOperation {
      public TileLoader(final String[][] names, final Continuation<Bitmap> receiver) {
         _currentNames = names;
         _currentBitmaps = new Bitmap[_currentNames.length][_currentNames[0].length];
         _receiver = receiver;
         _currentRow = 0;
         _currentColumn = 0;
         nextStep();
      }

      private void nextStep() {
         setSlave(TilesSource.getInstance().getTile(_currentNames[_currentRow][_currentColumn], new Continuation<Bitmap>() {
            @Override
            public void catchException(Throwable exception) {
               _receiver.catchException(exception);
            }

            @Override
            public void receive(Bitmap bitmap) {
               _currentBitmaps[_currentRow][_currentColumn] = bitmap;
               _currentRow++;
               if (_currentRow == _currentNames.length && _currentColumn < _currentNames[0].length) {
                  _currentColumn++;
                  _currentRow = 0;
               }
               if (_currentRow <= _currentNames.length
                       && _currentColumn < _currentNames[0].length) {
                  nextStep();
               } else {
                  setSlave(new CombineBitmaps(_currentBitmaps, new Receiver<Bitmap>() {
                     @Override
                     public void receive(Bitmap tile) {
                        _receiver.receive(tile);
                     }
                  }));
               }
            }
         }));
      }

      private final Continuation<Bitmap> _receiver;
      private Bitmap[][] _currentBitmaps;
      private int _currentRow;
      private int _currentColumn;
      private String[][] _currentNames;
   }

   private class CombineBitmaps extends ThreadOperation {
      public CombineBitmaps(final Bitmap[][] bitmaps, final Receiver<Bitmap> receiver) {
         super();
         _bitmaps = bitmaps;
         _receiver = receiver;
         start();
      }

      @Override
      protected void runInThread() {
         final Bitmap tile = TilesCombiner.combine(_bitmaps);
         postResult(new Runnable() {
            @Override
            public void run() {
               _receiver.receive(tile);
            }
         });
      }
      private Bitmap[][] _bitmaps;
      private Receiver<Bitmap> _receiver;
   }
}
