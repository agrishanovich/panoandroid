package com.example.grishanovichai.panoandroid.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class TilesCombiner {

   public static Bitmap combine(final Bitmap[][] tiles) {
      Bitmap result = null;
      Bitmap tile = tiles[0][0];
      int width = tile.getWidth() * tiles[0].length;
      int height = tile.getHeight() * tiles.length;
      result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(result);
      width = 0;
      height = 0;
      for (int i = 0; i < tiles.length; i++) {
         width = 0;
         for (int j = 0; j < tiles[i].length; j++) {
            tile = tiles[i][j];
            canvas.drawBitmap(tile, width, height, null);
            width += tile.getWidth();
         }
         height += tile.getHeight();
      }
      return result;
   }
}
