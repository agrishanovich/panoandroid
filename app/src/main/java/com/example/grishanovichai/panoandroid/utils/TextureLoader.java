package com.example.grishanovichai.panoandroid.utils;

import android.content.Context;
import android.opengl.EGL14;
import com.panoramagl.PLTexture;
import com.qulix.mdtlib.concurrency.ThreadOperation;
import com.qulix.mdtlib.functional.Receiver;
import com.qulix.mdtlib.operation.Operation;

import javax.microedition.khronos.egl.*;
import javax.microedition.khronos.opengles.GL10;

public class TextureLoader {

   public TextureLoader(EGL10 egl,
                        EGLContext renderContext,
                        EGLDisplay display,
                        EGLConfig eglConfig) {
      this.egl = egl;
      this.display = display;
      this.eglConfig = eglConfig;
      textureContext = egl.eglCreateContext(display, eglConfig, renderContext, null);
   }

   public Operation loadTexture(final GL10 gl, final PLTexture texture, final Receiver<PLTexture> receiver) {
      return new ThreadOperation() {
         {start();}

         @Override
         protected void runInThread() {
            int pbufferAttribs[] = { EGL10.EGL_WIDTH, 1, EGL10.EGL_HEIGHT, 1, EGL14.EGL_TEXTURE_TARGET,
                    EGL14.EGL_NO_TEXTURE, EGL14.EGL_TEXTURE_FORMAT, EGL14.EGL_NO_TEXTURE,
                    EGL10.EGL_NONE };

            EGLSurface localSurface = egl.eglCreatePbufferSurface(display, eglConfig, pbufferAttribs);

            egl.eglMakeCurrent(display, localSurface, localSurface, textureContext);

            texture.getTextureId(gl);
            postResult(new Result() {
               @Override
               public void run() {
                  receiver.receive(texture);
               }

               @Override
               public void cleanupIfTerminated() {

               }
            });
         }
      };
   }

   private EGLContext textureContext;
   private EGL10 egl;
   private EGLConfig eglConfig;
   private EGLDisplay display;
}
