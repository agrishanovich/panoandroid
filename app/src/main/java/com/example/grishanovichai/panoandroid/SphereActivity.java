package com.example.grishanovichai.panoandroid;

import android.os.Bundle;
import android.widget.Toast;
import com.panoramagl.*;
import com.panoramagl.hotspots.PLHotspot;
import com.panoramagl.hotspots.PLIHotspot;
import com.panoramagl.ios.structs.CGPoint;
import com.panoramagl.structs.PLPosition;
import com.panoramagl.utils.PLUtils;

public class SphereActivity extends PanoramaActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      PLSpherical2Panorama panorama = new PLSpherical2Panorama();
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_sphere2), false));
      PLICamera camera = panorama.getCamera();
      camera.setZoomLevel(2);
      PLHotspot hotspot = new PLHotspot(1, new PLImage(PLUtils.getBitmap(this, R.drawable.hotspot), false), 0, 0);
      hotspot.setOnClick("Hello World!");
      panorama.addHotspot(hotspot);
      PLViewListener listener = new PLViewListener() {
         @Override
         public void onDidClickHotspot(PLIView view, PLIHotspot hotspot, CGPoint screenPoint, PLPosition scene3DPoint) {
            Toast.makeText(view.getActivity().getApplicationContext(),
                           hotspot.getOnClick(),
                           Toast.LENGTH_SHORT).show();
         }
      };
      this.setPanorama(panorama);
      this.setListener(listener);
   }


}
