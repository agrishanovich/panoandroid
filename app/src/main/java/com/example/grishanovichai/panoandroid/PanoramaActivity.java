package com.example.grishanovichai.panoandroid;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.panoramagl.PLView;
import com.panoramagl.ios.structs.CGPoint;

public class PanoramaActivity extends PLView {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
   }

   @Override
   protected View onContentViewCreated(View contentView) {
      ViewGroup mainView = (ViewGroup)getLayoutInflater().inflate(R.layout.activity_panorama, null);
      ViewGroup panoramaView = (ViewGroup)mainView.findViewById(R.id.content_view);
      panoramaView.addView(contentView);
      panoramaView.setOnTouchListener(new View.OnTouchListener() {
         @Override
         public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
               if (isRotating) {
                  stopRotation();
                  button.setText("Play");
               }
            }
            return false;
         }
      });
      button = (Button) mainView.findViewById(R.id.animate_button);
      button.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            if (isRotating) {
               stopRotation();
               button.setText("Play");
            } else {
               startRotation();
               button.setText("Stop");
            }
         }
      });
      stopAnimation();
      return super.onContentViewCreated(mainView);
   }

   protected void startRotation() {
      isRotating = true;
      startAnimation();
   }

   protected void stopRotation() {
      isRotating = false;
      stopAnimation();
      startAnimation();
   }
   @Override
   protected boolean drawView() {
      if (isRotating) {
         updateRotation();
      }
      return super.drawView();
   }

   private void updateRotation() {
      getPanorama().getCamera().rotate(CGPoint.CGPointMake(0,0), CGPoint.CGPointMake(50, 0));
   }

   protected boolean isRotating;
   private Button button;
}
