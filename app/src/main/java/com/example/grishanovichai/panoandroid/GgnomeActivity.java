package com.example.grishanovichai.panoandroid;

import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import com.example.grishanovichai.panoandroid.data.TileInfo;
import com.example.grishanovichai.panoandroid.data.TilesLoader;
import com.example.grishanovichai.panoandroid.utils.TextureLoader;
import com.panoramagl.*;
import com.qulix.mdtlib.functional.Receiver;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class GgnomeActivity extends PanoramaActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
   }

   @Override
   protected EGLContext createEglContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
      EGLContext context = super.createEglContext(egl, display, eglConfig);
      _textureLoader = new TextureLoader(egl, context, display, eglConfig);
      return context;
   }

   @Override
   protected void onResume() {
      super.onResume();
      PLCubicPanorama panorama = new PLCubicPanorama();
      setPanorama(panorama);
      TilesLoader.getInstance().onTileLoadedSubscription().subscribe(new Receiver<TileInfo>() {
         @Override
         public void receive(final TileInfo tileInfo) {
            Bitmap bitmap = tileInfo.bitmap();
            Bitmap scaledBitmap = null;
            final PLCubicPanorama panorama = (PLCubicPanorama) getPanorama();
            if (bitmap.getHeight() > 1024 || bitmap.getWidth() > 1024) {
               scaledBitmap = Bitmap.createScaledBitmap(bitmap, 1024, 1024, false);
            } else {
               scaledBitmap = bitmap;
            }
            _textureLoader.loadTexture(getGLContext(), new PLTexture(new PLImage(scaledBitmap)), new Receiver<PLTexture>() {
               @Override
               public void receive(PLTexture texture) {
                  setLocked(true);
                  panorama.setTexture(texture, tileInfo.orientation());
                  PLICamera camera = panorama.getCamera();
                  PLICamera cameraClone = camera.clone();
                  panorama.setCamera(cameraClone);
                  setPanorama(panorama);
                  setLocked(false);
               }
            });
         }
      });
      TilesLoader.getInstance().startLoad();
   }

   private TextureLoader _textureLoader;

}
