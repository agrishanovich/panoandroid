package com.example.grishanovichai.panoandroid.data;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GgnomeService {

   @GET("samples/pano2vr_4/synagoge/tiles/{tile}")
   Call<ResponseBody> tile(@Path("tile") String tileName);
}
