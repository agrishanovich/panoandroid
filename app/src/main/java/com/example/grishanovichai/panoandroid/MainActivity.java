package com.example.grishanovichai.panoandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final List<ItemData> data = Arrays.asList(new ItemData("Cube Panorama", CubicActivity.class),
                                                  new ItemData("Sphere Panorama", SphereActivity.class),
                                                  new ItemData("Ggnome Panorama", GgnomeActivity.class));
        RecyclerView list = (RecyclerView)findViewById(R.id.list);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        list.setLayoutManager(llm);
        list.setAdapter(new RecyclerView.Adapter<ItemHolder>() {
            @Override
            public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view, viewGroup, false);
                return new ItemHolder(view);
            }

            @Override
            public void onBindViewHolder(ItemHolder viewHolder, int i) {
                viewHolder.bind(data.get(i));
            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        });
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        public ItemHolder(View itemView) {
            super(itemView);
            _title = (TextView) itemView.findViewById(R.id.item_title);
        }

        public void bind(final ItemData data) {
            _title.setText(data._title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, data._activity);
                    MainActivity.this.startActivity(intent);
                }
            });
        }

        TextView _title;
    }

    private class ItemData implements Serializable {
        public final String _title;
        public final Class _activity;
        public ItemData(final String title, final Class activity) {
            _title = title;
            _activity = activity;
        }
    }
}



