package com.example.grishanovichai.panoandroid;

import android.os.Bundle;
import com.panoramagl.PLCubicPanorama;
import com.panoramagl.PLICamera;
import com.panoramagl.PLImage;
import com.panoramagl.enumerations.PLCubeFaceOrientation;
import com.panoramagl.utils.PLUtils;

public class CubicActivity extends PanoramaActivity {

   @Override
   public void onCreate(Bundle savedInstanceState)
   {
      super.onCreate(savedInstanceState);
      PLCubicPanorama panorama = new PLCubicPanorama();
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_cube_f), false), PLCubeFaceOrientation.PLCubeFaceOrientationFront);
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_cube_b), false), PLCubeFaceOrientation.PLCubeFaceOrientationBack);
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_cube_l), false), PLCubeFaceOrientation.PLCubeFaceOrientationLeft);
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_cube_r), false), PLCubeFaceOrientation.PLCubeFaceOrientationRight);
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_cube_d), false), PLCubeFaceOrientation.PLCubeFaceOrientationDown);
      panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.drawable.pano_cube_u), false), PLCubeFaceOrientation.PLCubeFaceOrientationUp);
      PLICamera camera = panorama.getCamera();
      camera.setZoomLevel(2);
      this.setPanorama(panorama);
   }
}
