package com.example.grishanovichai.panoandroid.data;

import android.graphics.Bitmap;
import com.panoramagl.enumerations.PLCubeFaceOrientation;

public class TileInfo {

   public TileInfo(final Bitmap bitmap, final PLCubeFaceOrientation orientation) {
      _bitmap = bitmap;
      _orientation = orientation;
   }

   public Bitmap bitmap() {
      return _bitmap;
   }

   public PLCubeFaceOrientation orientation() {
      return _orientation;
   }

   private final Bitmap _bitmap;
   private final PLCubeFaceOrientation _orientation;
}
