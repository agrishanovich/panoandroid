package com.example.grishanovichai.panoandroid.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qulix.mdtlib.functional.Continuation;
import com.qulix.mdtlib.operation.Operation;
import com.qulix.mdtlib.operation.SimpleOperation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TilesSource {

   public static TilesSource getInstance() {
      if (_instance == null) {
         _instance = new TilesSource();
      }
      return _instance;
   }

   private TilesSource() {
      Retrofit retrofit = new Retrofit.Builder().baseUrl("http://ggnome.com/").build();
      _service = retrofit.create(GgnomeService.class);
   }

   public Operation getTile(final String tileName, final Continuation<Bitmap> receiver) {
      return executeCall(_service.tile(tileName), new Callback<ResponseBody>() {
         @Override
         public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            receiver.receive(BitmapFactory.decodeStream(response.body().byteStream()));
         }

         @Override
         public void onFailure(Call<ResponseBody> call, Throwable t) {
            receiver.catchException(t);
         }
      });
   }

   private <T> Operation executeCall(final Call<T> call, final Callback<T> callback) {
      Operation operation = new SimpleOperation();
      call.enqueue(callback);
      operation.endedEvent().subscribe(new Runnable() {
         @Override
         public void run() {
            call.cancel();
         }
      });
      return operation;
   }

   private static TilesSource _instance;
   private GgnomeService _service;

}
