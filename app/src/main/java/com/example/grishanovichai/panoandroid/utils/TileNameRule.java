package com.example.grishanovichai.panoandroid.utils;

import com.panoramagl.enumerations.PLCubeFaceOrientation;
import com.qulix.mdtlib.functional.converting.Converter;

public class TileNameRule {

   public static String[][] getTileNames(final PLCubeFaceOrientation orientation, final int level) {
      int rows = rowsForLevel(level);
      int columns = columnsForLevel(level);
      String [][] result = new String[rows][columns];
      for (int i = 0; i < rowsForLevel(level); i++) {
         for (int j = 0; j < columnsForLevel(level); j++) {
            String name = ORIENTATION_TO_STRING_CONVERTER.convert(orientation) +
                    "_" + "l" + level +
                    "_" + i + "_" + j +
                    ".jpg";
            result[i][j] = name;
         }
      }
      return result;
   }

   private static int rowsForLevel(final int level) {
      if (level == 5) {
         return 1;
      }
      if (level == 4) {
         return 2;
      }
      throw new RuntimeException("do not support level");
   }

   private static int columnsForLevel(final int level) {
      if (level == 5) {
         return 1;
      }
      if (level == 4) {
         return 2;
      }
      throw new RuntimeException("do not support level");
   }

   private static Converter<String, PLCubeFaceOrientation> ORIENTATION_TO_STRING_CONVERTER = new Converter<String, PLCubeFaceOrientation>() {
      @Override
      public String convert(PLCubeFaceOrientation orientation) {
         switch (orientation) {
            case PLCubeFaceOrientationFront:
               return "c0";
            case PLCubeFaceOrientationBack:
               return "c2";
            case PLCubeFaceOrientationDown:
               return "c5";
            case PLCubeFaceOrientationLeft:
               return "c3";
            case PLCubeFaceOrientationRight:
               return "c1";
            case PLCubeFaceOrientationUp:
               return "c4";
            default:
               throw new RuntimeException("do not support orientation!");
         }
      }
   };
}
